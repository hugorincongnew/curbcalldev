class AddBrokerageIdToAgent < ActiveRecord::Migration
  def change
    add_column :agents, :brokerage_id, :integer
  end
end
