class CreateListings < ActiveRecord::Migration
  def change
    create_table :listings do |t|
      t.string :address
      t.string :mls_num
      t.string :city
      t.string :lat
      t.string :lon
      t.string :price
      t.string :baths
      t.string :beds
      t.string :office_num
      t.string :agent_num
      t.string :photo1
      t.string :photo2
      t.string :status

      t.timestamps
    end
  end
end
