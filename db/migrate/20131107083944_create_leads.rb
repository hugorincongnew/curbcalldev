class CreateLeads < ActiveRecord::Migration
  def change
    create_table :leads do |t|
      t.string :name
      t.string :lat
      t.string :lon
      t.string :address
      t.string :hood
      t.string :status
      t.string :agent_phone
      t.string :agent_company
      t.string :agent_email
      t.string :agent_image
      t.string :bio
      t.string :buyer_email
      t.string :buyer_phone
      t.string :buyer_price_range
      t.integer :buyer_num_showings

      t.timestamps
    end
  end
end
