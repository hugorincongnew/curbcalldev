class AddDevTokenToUsers < ActiveRecord::Migration
  def change
    add_column :users, :dev_token, :string
    add_column :agents, :dev_token, :string
  end
end
