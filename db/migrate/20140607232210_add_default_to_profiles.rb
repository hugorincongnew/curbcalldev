class AddDefaultToProfiles < ActiveRecord::Migration
  def change
  	change_column :profiles, :num_showings, :integer, default: 0
  end
end
