class CreateShowings < ActiveRecord::Migration
  def change
    create_table :showings do |t|
      t.integer :agent_id
      t.integer :lead_id
      t.string :address
      t.string :buyer_name
      t.string :agent_name
      t.string :buyer_id

      t.timestamps
    end
  end
end
