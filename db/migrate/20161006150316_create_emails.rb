class CreateEmails < ActiveRecord::Migration
  def change
    create_table :emails do |t|
      t.string :to
      t.string :from
      t.string :subject
      t.text :body
      t.boolean :parsed
      t.references :user, index: true
      t.string :last_name

      t.timestamps
    end

    add_index :emails, :from
  end
end
