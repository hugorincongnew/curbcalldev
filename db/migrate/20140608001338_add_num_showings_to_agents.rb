class AddNumShowingsToAgents < ActiveRecord::Migration
  def change
    add_column :agents, :num_showings, :integer, default: 0
  end
end
