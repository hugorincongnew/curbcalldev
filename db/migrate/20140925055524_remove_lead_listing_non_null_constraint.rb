class RemoveLeadListingNonNullConstraint < ActiveRecord::Migration
  def change
    change_column_null :leads, :listing_id, true
  end
end
