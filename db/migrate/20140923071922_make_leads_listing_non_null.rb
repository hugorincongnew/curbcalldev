class MakeLeadsListingNonNull < ActiveRecord::Migration
  def change
    
    #crate a placeholder listing
    fake_listing = Listing.create!
    #lookup listing by the address
    Lead.all.each do |lead|
      listing = Listing.find_by_address(lead.address)
      listing = fake_listing if listing.nil?
      
      lead.listing=listing
      lead.status=Lead::STATES[:unanswered]
      lead.save!
    end
    
    change_column_null :leads, :listing_id, false
  end
end
