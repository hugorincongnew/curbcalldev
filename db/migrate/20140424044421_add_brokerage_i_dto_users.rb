class AddBrokerageIDtoUsers < ActiveRecord::Migration
  def change
  	add_column :users, :name, :string
  	add_column :users, :brokerage_id, :integer
  	add_column :users, :bio, :text
  	add_column :users, :image, :string
  	add_column :users, :phone, :string
  	add_column :users, :friendly, :float
  	add_column :users, :local_expert, :float
  	add_column :users, :agent, :boolean
  end
end
