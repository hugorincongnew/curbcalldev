class RemoveBrokerageIdFromZips < ActiveRecord::Migration
  def change
  	remove_column :zips, :brokerage_id
  end
end
