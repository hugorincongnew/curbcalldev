class AddRatingInfoToProfile < ActiveRecord::Migration
  def change
    add_column :profiles, :showing_feedback_count, :int, :default => 0
    add_column :profiles, :nice_rating, :float
    add_column :profiles, :nice_rating_count, :int, :default => 0
  end
end
