class ChangeLatLonToFloatListings < ActiveRecord::Migration
  def change
  	remove_column :listings, :lat, :float
  	remove_column :listings, :lon, :float

  	add_column :listings, :lat, :float
  	add_column :listings, :lon, :float
  end
end
