class AddSandboxAccessTokenToApp < ActiveRecord::Migration
  def change
    add_column :apps, :sandbox_access_token, :string
  end
end
