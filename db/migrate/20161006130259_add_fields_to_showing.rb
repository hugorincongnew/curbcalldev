class AddFieldsToShowing < ActiveRecord::Migration
  def change
    add_column :showings, :called, :boolean, :default => false
    add_column :showings, :texted, :boolean, :default => false
  end
end
