class AddBtCustomerIdToRadiusUser < ActiveRecord::Migration
  def change
    add_column :radius_users, :bt_customer_id, :string
  end
end
