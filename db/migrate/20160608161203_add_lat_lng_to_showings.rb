class AddLatLngToShowings < ActiveRecord::Migration
  def change
    add_column :showings, :lat, :float
    add_column :showings, :lng, :float
  end
end
