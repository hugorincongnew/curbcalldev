class AddIndexesToListings < ActiveRecord::Migration
  def change
  	add_index :agents, :brokerage_id
  	add_index :brokerages, :mls_id
  	add_index :leads, :agent_id
  	add_index :leads, :buyer_id
  	add_index :leads, :status
  	add_index :listings, :mls_id
  	add_index :listings, :lat
  	add_index :listings, :lon
  	add_index :mls, :retsly
  	add_index :profiles, :user_id
  	add_index :showings, :buyer_id
  	add_index :users, :brokerage_id
  	add_index :zips, :code
  	add_index :zips, :mls_id
  	add_index :zips, :brokerage_id
  end
end
