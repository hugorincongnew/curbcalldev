class ChangeTypesInListings < ActiveRecord::Migration
  def change
  	remove_column :listings, :price
  	remove_column :listings, :baths
  	remove_column :listings, :beds

  	add_column :listings, :price, :integer
  	add_column :listings, :baths, :integer
  	add_column :listings, :beds, :integer
  end
end
