class CreateZips < ActiveRecord::Migration
  def change
    create_table :zips do |t|
      t.integer :code
      t.integer :brokerage_id
      t.integer :region_id

      t.timestamps
    end
  end
end
