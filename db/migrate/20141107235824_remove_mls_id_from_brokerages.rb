class RemoveMlsIdFromBrokerages < ActiveRecord::Migration
  def change
  	remove_column :brokerages, :mls_id
  end
end
