class AddProfileFieldsToAgent < ActiveRecord::Migration
  def change
    add_column :agents, :showing_rating, :float
    add_column :agents, :showing_feedback_count, :integer
    add_column :agents, :nice_rating, :float
    add_column :agents, :nice_rating_count, :integer
  end
end
