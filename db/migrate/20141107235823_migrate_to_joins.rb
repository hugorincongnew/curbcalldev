class MigrateToJoins < ActiveRecord::Migration
  def change
    
    Brokerage.all.each do |brokerage|
      MlsBrokerage.create(mls_id: brokerage.mls_id, brokerage_id: brokerage.id)
    end
    
    Zip.all.each do |zip|
      BrokerageZip.create(brokerage_id: zip.brokerage_id, zip_id: zip.id) unless zip.brokerage_id.nil?
      MlsZip.create(mls_id: zip.mls_id, zip_id: zip.id) unless zip.mls_id.nil?
    end

    
  end
end
