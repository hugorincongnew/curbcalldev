class AddStatusToShowing < ActiveRecord::Migration
  def change
    add_column :showings, :status, :string
    add_column :showings, :mls_num, :string
  end
end
