class ChangeCodeTypeOnZips < ActiveRecord::Migration
  def change
  	change_column :zips, :code, :string
  end
end
