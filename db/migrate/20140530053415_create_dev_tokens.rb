class CreateDevTokens < ActiveRecord::Migration
  def change
    create_table :dev_tokens do |t|
      t.string :dev_token
      t.integer :user_id

      t.timestamps
    end
  end
end
