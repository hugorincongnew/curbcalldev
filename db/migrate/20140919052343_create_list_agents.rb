class CreateListAgents < ActiveRecord::Migration
  def change
    create_table :list_agents do |t|
      t.integer :mls_num
      t.string :first_name
      t.string :last_name
      t.integer :office_code
      t.string :office_name

      t.timestamps
    end
  end
end
