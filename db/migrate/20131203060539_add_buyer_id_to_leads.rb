class AddBuyerIdToLeads < ActiveRecord::Migration
  def change
    add_column :leads, :buyer_id, :integer
  end
end
