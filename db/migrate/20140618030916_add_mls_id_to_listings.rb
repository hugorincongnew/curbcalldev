class AddMlsIdToListings < ActiveRecord::Migration
  def change
    add_column :listings, :mls_id, :integer
  end
end
