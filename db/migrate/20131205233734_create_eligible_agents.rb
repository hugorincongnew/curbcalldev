class CreateEligibleAgents < ActiveRecord::Migration
  def change
    create_table :eligible_agents do |t|
      t.integer :lead_id
      t.integer :agent_user_id

      t.timestamps
    end
  end
end
