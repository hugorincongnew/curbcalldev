class AddConfirmableToDeveloper < ActiveRecord::Migration

     def up
       add_column :developers, :confirmation_token, :string
       add_column :developers, :confirmed_at, :datetime
       add_column :developers, :confirmation_sent_at, :datetime
       add_column :developers, :unconfirmed_email, :string # Only if using reconfirmable
       add_index :developers, :confirmation_token, unique: true
       # User.reset_column_information # Need for some types of updates, but not for update_all.
       # To avoid a short time window between running the migration and updating all existing
       # developers as confirmed, do the following
       execute("UPDATE developers SET confirmed_at = NOW()")
       # All existing user accounts should be able to log in after this.
     end

     def down
       remove_columns :developers, :confirmation_token, :confirmed_at, :confirmation_sent_at, :unconfirmed_email
       
     end
end
