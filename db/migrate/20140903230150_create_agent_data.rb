class CreateAgentData < ActiveRecord::Migration
  def change
    create_table :agent_data do |t|
      t.integer :user_id
      t.integer :showings, :default => 0
      t.integer :requests, :default => 0
      t.integer :live, :default => 0

      t.timestamps
    end
  end
end
