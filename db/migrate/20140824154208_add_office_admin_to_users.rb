class AddOfficeAdminToUsers < ActiveRecord::Migration
  def change
    add_column :users, :office_admin, :boolean
  end
end
