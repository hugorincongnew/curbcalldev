class AddFieldsToAgent < ActiveRecord::Migration
  def change
  	add_column 	:agents, :user_id, :integer
  	add_column 	:agents, :email, :string
  	add_column 	:agents, :image, :string
  	add_column 	:agents, :bio, :text
  end
end
