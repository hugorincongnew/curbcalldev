class CreateMls < ActiveRecord::Migration
  def change
    create_table :mls do |t|
      t.string :name
      t.string :username
      t.string :password
      t.string :login_link

      t.timestamps
    end
  end
end
