class CreateHistoryAgent < ActiveRecord::Migration
  def change
    create_table :history_agents do |t|
      t.timestamp :start_time
      t.timestamp :end_time
      t.integer :num_request, default: 0
      t.integer :user_id
    end
  end
end
