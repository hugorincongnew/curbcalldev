class CreateApps < ActiveRecord::Migration
  def change
    create_table :apps do |t|
      t.string :name
      t.string :description
      t.string :website
      t.string :organization
      t.string :organization
      t.string :website
      t.string :client_id
      t.string :client_secret
      t.string :access_token
      t.references :developer

      t.timestamps
    end
  end
end
