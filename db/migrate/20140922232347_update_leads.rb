class UpdateLeads < ActiveRecord::Migration
  def change
    
    #add a listing to a lead
    add_column :leads, :listing_id, :integer
    add_index :leads, :listing_id
  end
end
