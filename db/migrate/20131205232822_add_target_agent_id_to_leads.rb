class AddTargetAgentIdToLeads < ActiveRecord::Migration
  def change
    add_column :leads, :target_agent, :integer
  end
end
