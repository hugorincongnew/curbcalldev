class AddHoodToShowing < ActiveRecord::Migration
  def change
    add_column :showings, :hood, :string
  end
end
