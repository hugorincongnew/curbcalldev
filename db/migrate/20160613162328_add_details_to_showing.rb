class AddDetailsToShowing < ActiveRecord::Migration
  def change
    add_column :showings, :photo_url, :string
    add_column :showings, :address_two, :string
    add_column :showings, :list_price, :string
    add_column :showings, :notes, :string
  end
end
