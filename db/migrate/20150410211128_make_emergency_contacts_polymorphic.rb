class MakeEmergencyContactsPolymorphic < ActiveRecord::Migration
  def change
    add_reference :emergency_contacts, :contactable, polymorphic: true, index: true
    
    #now take every user_id and put it in contactable_id and "User" into contactable_type
    update = "UPDATE emergency_contacts 
    SET contactable_id = emergency_contacts.user_id,
    contactable_type = 'User';"
    execute(update)
    
    #now delete user_id
    remove_column :emergency_contacts, :user_id
  end
end
