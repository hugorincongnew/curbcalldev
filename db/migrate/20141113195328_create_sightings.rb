class CreateSightings < ActiveRecord::Migration
  def change
    create_table :sightings do |t|
      t.references :showing, index: true, null: false
      t.decimal :lat, precision: 9, scale: 6, null: false
      t.decimal :lon, precision: 9, scale: 6, null: false

      t.timestamps
    end
  end
end
