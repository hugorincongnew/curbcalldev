class CreateAgents < ActiveRecord::Migration
  def change
    create_table :agents do |t|
      t.string :name
      t.float :lat
      t.float :lon
      t.string :phone
      t.string :company

      t.timestamps
    end
  end
end
