class AddBuyerPhoneToShowing < ActiveRecord::Migration
  def change
    add_column :showings, :buyer_phone, :string
  end
end
