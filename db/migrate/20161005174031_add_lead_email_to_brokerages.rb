class AddLeadEmailToBrokerages < ActiveRecord::Migration
  def change
    add_column :brokerages, :lead_email, :string
  end
end
