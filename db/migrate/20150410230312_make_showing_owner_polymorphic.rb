class MakeShowingOwnerPolymorphic < ActiveRecord::Migration
  def change
    add_reference :showings, :showing_owner, polymorphic: true, index: true
    
    #now take every user_id and put it in contactable_id and "User" into contactable_type
    update = "UPDATE showings 
    SET showing_owner_id = showings.agent_id,
    showing_owner_type = 'User';"
    execute(update)
    
    #now delete user_id
    remove_column :showings, :agent_id
  end
end
