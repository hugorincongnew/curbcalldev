class AddAccessTokenToBrokerage < ActiveRecord::Migration
  def change
    add_column :brokerages, :access_token, :string
  end
end
