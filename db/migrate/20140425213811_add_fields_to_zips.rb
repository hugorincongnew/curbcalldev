class AddFieldsToZips < ActiveRecord::Migration
  def change
    add_column :zips, :city, :string
    add_column :zips, :state, :string
    add_column :zips, :county, :string
    add_column :zips, :lat, :float
    add_column :zips, :lon, :float
  end
end
