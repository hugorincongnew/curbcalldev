class AddLegalToMls < ActiveRecord::Migration
  def change
    add_column :mls, :legal, :text
    add_column :mls, :logo, :string
  end
end
