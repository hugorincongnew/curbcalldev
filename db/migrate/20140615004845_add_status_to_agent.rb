class AddStatusToAgent < ActiveRecord::Migration
  def change
    add_column :agents, :status, :string
  end
end
