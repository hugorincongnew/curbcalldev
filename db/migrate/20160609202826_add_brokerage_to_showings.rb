class AddBrokerageToShowings < ActiveRecord::Migration
  def change
    add_reference :showings, :brokerage, index: true
  end
end
