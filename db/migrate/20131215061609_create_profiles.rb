class CreateProfiles < ActiveRecord::Migration
  def change
    create_table :profiles do |t|
      t.text :bio
      t.string :name
      t.string :company
      t.float :showing_rating
      t.string :image
      t.string :phone
      t.string :email
      t.string :price_range
      t.integer :num_showings
      t.integer :user_id

      t.timestamps
    end
  end
end
