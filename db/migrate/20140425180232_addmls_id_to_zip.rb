class AddmlsIdToZip < ActiveRecord::Migration
  def change
  	add_column :zips, :mls_id, :integer
  	add_column :regions, :mls_id, :integer
  	add_column :brokerages, :mls_id, :integer
  end
end
