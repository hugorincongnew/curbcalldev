class AddBuyerEmailToShowing < ActiveRecord::Migration
  def change
    add_column :showings, :buyer_email, :string
  end
end
