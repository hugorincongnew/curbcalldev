class AddSomeFieldsToListings < ActiveRecord::Migration
  def change
    add_column :listings, :remarks, :text
    add_column :listings, :subdivision, :string
    add_column :listings, :list_office, :string
    add_column :listings, :list_agent, :string
  end
end
