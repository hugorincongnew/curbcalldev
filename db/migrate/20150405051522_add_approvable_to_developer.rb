class AddApprovableToDeveloper < ActiveRecord::Migration
    # Note: You can't use change, as User.update_all will fail in the down migration
     def up
       add_column :developers, :approval_token, :string
       add_column :developers, :approved_at, :datetime
       add_column :developers, :approval_sent_at, :datetime
       # add_column :developers, :unconfirmed_email, :string # Only if using reconfirmable
       add_index :developers, :approval_token, unique: true
     end

     def down
      # remove_columns :developers, :approval_token, :approved_at, :approval_sent_at
     end
end
