class RemoveMlsIdFromZips < ActiveRecord::Migration
  def change
  	remove_column :zips, :mls_id, :integer
  end
end
