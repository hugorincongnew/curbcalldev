class AddAgentNameToLeads < ActiveRecord::Migration
  def change
  	add_column :leads, :agent_name, :string
  	add_column :leads, :agent_id, :integer
  end
end
