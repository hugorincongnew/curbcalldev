class AddAgentStatusToShowings < ActiveRecord::Migration
  def change
    add_column :showings, :agent_state, :string
  end
end
