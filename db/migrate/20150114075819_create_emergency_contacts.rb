class CreateEmergencyContacts < ActiveRecord::Migration
  def change
    create_table :emergency_contacts do |t|
      t.string :name
      t.string :phone
      t.string :email
      t.references :user, index: true

      t.timestamps
    end
  end
end
