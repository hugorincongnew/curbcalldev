# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161006224124) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_admin_comments", force: :cascade do |t|
    t.string   "namespace"
    t.text     "body"
    t.string   "resource_id",   null: false
    t.string   "resource_type", null: false
    t.integer  "author_id"
    t.string   "author_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree

  create_table "agent_data", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "showings",   default: 0
    t.integer  "requests",   default: 0
    t.integer  "live",       default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "agents", force: :cascade do |t|
    t.string   "name"
    t.float    "lat"
    t.float    "lon"
    t.string   "phone"
    t.string   "company"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id"
    t.string   "email"
    t.string   "image"
    t.text     "bio"
    t.float    "showing_rating"
    t.integer  "showing_feedback_count"
    t.float    "nice_rating"
    t.integer  "nice_rating_count"
    t.integer  "brokerage_id"
    t.string   "dev_token"
    t.integer  "num_showings",           default: 0
    t.string   "status"
    t.string   "mls"
  end

  add_index "agents", ["brokerage_id"], name: "index_agents_on_brokerage_id", using: :btree

  create_table "api_keys", force: :cascade do |t|
    t.string   "access_token"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "apps", force: :cascade do |t|
    t.string   "name"
    t.string   "description"
    t.string   "website"
    t.string   "organization"
    t.string   "client_id"
    t.string   "client_secret"
    t.string   "access_token"
    t.integer  "developer_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "sandbox_access_token"
  end

  create_table "brokerage_zips", id: false, force: :cascade do |t|
    t.integer "brokerage_id"
    t.integer "zip_id"
  end

  create_table "brokerages", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "access_token"
    t.string   "lead_email"
  end

  create_table "dev_tokens", force: :cascade do |t|
    t.string   "dev_token"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "developers", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.string   "approval_token"
    t.datetime "approved_at"
    t.datetime "approval_sent_at"
  end

  add_index "developers", ["approval_token"], name: "index_developers_on_approval_token", unique: true, using: :btree
  add_index "developers", ["confirmation_token"], name: "index_developers_on_confirmation_token", unique: true, using: :btree
  add_index "developers", ["email"], name: "index_developers_on_email", unique: true, using: :btree
  add_index "developers", ["reset_password_token"], name: "index_developers_on_reset_password_token", unique: true, using: :btree

  create_table "eligible_agents", force: :cascade do |t|
    t.integer  "lead_id"
    t.integer  "agent_user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "emails", force: :cascade do |t|
    t.string   "to"
    t.string   "from"
    t.string   "subject"
    t.text     "body"
    t.boolean  "parsed"
    t.integer  "user_id"
    t.string   "last_name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "emails", ["from"], name: "index_emails_on_from", using: :btree
  add_index "emails", ["user_id"], name: "index_emails_on_user_id", using: :btree

  create_table "emergency_contacts", force: :cascade do |t|
    t.string   "name"
    t.string   "phone"
    t.string   "email"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "contactable_id"
    t.string   "contactable_type"
  end

  add_index "emergency_contacts", ["contactable_type", "contactable_id"], name: "index_emergency_contacts_on_contactable_type_and_contactable_id", using: :btree

  create_table "history_agents", force: :cascade do |t|
    t.datetime "start_time"
    t.datetime "end_time"
    t.integer  "num_request", default: 0
    t.integer  "user_id"
  end

  create_table "identities", force: :cascade do |t|
    t.integer "user_id"
    t.string  "provider"
    t.string  "uid"
  end

  add_index "identities", ["user_id"], name: "index_identities_on_user_id", using: :btree

  create_table "leads", force: :cascade do |t|
    t.string   "name"
    t.string   "lat"
    t.string   "lon"
    t.string   "address"
    t.string   "hood"
    t.string   "status"
    t.string   "agent_phone"
    t.string   "agent_company"
    t.string   "agent_email"
    t.string   "agent_image"
    t.string   "bio"
    t.string   "buyer_email"
    t.string   "buyer_phone"
    t.string   "buyer_price_range"
    t.integer  "buyer_num_showings"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "buyer_id"
    t.integer  "target_agent"
    t.string   "agent_name"
    t.integer  "agent_id"
    t.integer  "listing_id"
  end

  add_index "leads", ["agent_id"], name: "index_leads_on_agent_id", using: :btree
  add_index "leads", ["buyer_id"], name: "index_leads_on_buyer_id", using: :btree
  add_index "leads", ["listing_id"], name: "index_leads_on_listing_id", using: :btree
  add_index "leads", ["status"], name: "index_leads_on_status", using: :btree

  create_table "list_agents", force: :cascade do |t|
    t.integer  "mls_num"
    t.string   "first_name"
    t.string   "last_name"
    t.integer  "office_code"
    t.string   "office_name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "listings", force: :cascade do |t|
    t.string   "address"
    t.string   "mls_num"
    t.string   "city"
    t.string   "office_num"
    t.string   "agent_num"
    t.string   "photo1"
    t.string   "photo2"
    t.string   "status"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "remarks"
    t.string   "subdivision"
    t.string   "list_office"
    t.string   "list_agent"
    t.integer  "price"
    t.integer  "baths"
    t.integer  "beds"
    t.float    "lat"
    t.float    "lon"
    t.integer  "mls_id"
  end

  add_index "listings", ["lat"], name: "index_listings_on_lat", using: :btree
  add_index "listings", ["lon"], name: "index_listings_on_lon", using: :btree
  add_index "listings", ["mls_id"], name: "index_listings_on_mls_id", using: :btree

  create_table "mls", force: :cascade do |t|
    t.string   "name"
    t.string   "username"
    t.string   "password"
    t.string   "login_link"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "retsly"
    t.text     "legal"
    t.string   "logo"
    t.datetime "update_time"
  end

  add_index "mls", ["retsly"], name: "index_mls_on_retsly", using: :btree

  create_table "mls_brokerages", id: false, force: :cascade do |t|
    t.integer "brokerage_id"
    t.integer "mls_id"
  end

  create_table "mls_zips", id: false, force: :cascade do |t|
    t.integer "mls_id"
    t.integer "zip_id"
  end

  create_table "profiles", force: :cascade do |t|
    t.text     "bio"
    t.string   "name"
    t.string   "company"
    t.float    "showing_rating"
    t.string   "image"
    t.string   "phone"
    t.string   "email"
    t.string   "price_range"
    t.integer  "num_showings",           default: 0
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "showing_feedback_count", default: 0
    t.float    "nice_rating"
    t.integer  "nice_rating_count",      default: 0
    t.string   "profile_pic"
  end

  add_index "profiles", ["user_id"], name: "index_profiles_on_user_id", using: :btree

  create_table "radius_users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name"
    t.string   "bt_customer_id"
  end

  add_index "radius_users", ["email"], name: "index_radius_users_on_email", unique: true, using: :btree
  add_index "radius_users", ["reset_password_token"], name: "index_radius_users_on_reset_password_token", unique: true, using: :btree

  create_table "regions", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "mls_id"
  end

  create_table "showings", force: :cascade do |t|
    t.integer  "lead_id"
    t.string   "address"
    t.string   "buyer_name"
    t.string   "agent_name"
    t.string   "buyer_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "status"
    t.string   "mls_num"
    t.string   "agent_state"
    t.integer  "showing_owner_id"
    t.string   "showing_owner_type"
    t.float    "lat"
    t.float    "lng"
    t.integer  "brokerage_id"
    t.string   "hood"
    t.string   "photo_url"
    t.string   "address_two"
    t.string   "list_price"
    t.string   "notes"
    t.string   "buyer_email"
    t.string   "buyer_phone"
    t.boolean  "called",             default: false
    t.boolean  "texted",             default: false
  end

  add_index "showings", ["brokerage_id"], name: "index_showings_on_brokerage_id", using: :btree
  add_index "showings", ["buyer_id"], name: "index_showings_on_buyer_id", using: :btree
  add_index "showings", ["showing_owner_type", "showing_owner_id"], name: "index_showings_on_showing_owner_type_and_showing_owner_id", using: :btree

  create_table "sightings", force: :cascade do |t|
    t.integer  "showing_id",                         null: false
    t.decimal  "lat",        precision: 9, scale: 6, null: false
    t.decimal  "lon",        precision: 9, scale: 6, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "sightings", ["showing_id"], name: "index_sightings_on_showing_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.string   "authentication_token"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name"
    t.integer  "brokerage_id"
    t.text     "bio"
    t.string   "image"
    t.string   "phone"
    t.float    "friendly"
    t.float    "local_expert"
    t.boolean  "agent"
    t.string   "dev_token"
    t.boolean  "admin"
    t.boolean  "office_admin"
    t.string   "player_id"
  end

  add_index "users", ["authentication_token"], name: "index_users_on_authentication_token", unique: true, using: :btree
  add_index "users", ["brokerage_id"], name: "index_users_on_brokerage_id", using: :btree
  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "zips", force: :cascade do |t|
    t.string   "code"
    t.integer  "region_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "city"
    t.string   "state"
    t.string   "county"
    t.float    "lat"
    t.float    "lon"
  end

  add_index "zips", ["code"], name: "index_zips_on_code", using: :btree

end
