class AgentMailer < ActionMailer::Base
  default from: "do-not-reply@curbcall.com"

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.agent_mailer.lead_info.subject
  #
  def lead_info(lead)
    @lead = lead
    mail to: lead.agent.email
  end
  
  def welcome(user)
    @user=user
    mail to: @user.email, subject: "Welcome to Curb Call! (Login details inside)"
  end
  
  # AgentMailer.emergency(to, from).deliver
  def emergency(to, from)
    
    @message = "Curb Call Safety Service"
    
    subject="Curb Call Agent Notification"
    mail to: to, from:from, subject: subject
  end
  
end
