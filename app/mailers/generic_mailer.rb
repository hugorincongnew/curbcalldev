class GenericMailer < ActionMailer::Base

  def template(to, from, subject, body)
    @body = body

    mail to: to, from: from, subject: subject
  end
end
