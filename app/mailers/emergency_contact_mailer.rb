class EmergencyContactMailer < ActionMailer::Base
  default from: "from@example.com"

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.emergency_contact_mailer.started.subject
  #
  def started(to, from, opts)
    @bundle=opts
    mail to:  to, from: from
  end

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.emergency_contact_mailer.completed.subject
  #
  def completed(to, from, opts)
    @bundle=opts
    mail to:  to, from: from
  end

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.emergency_contact_mailer.danger.subject
  #
  def danger(to, from, opts)
    @bundle=opts
    mail to:  to, from: from
  end
end
