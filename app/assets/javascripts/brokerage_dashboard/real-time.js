$(function()
{
  
  function initialize_map() {
    

    if (showings.length<1)
    {
      return; //show no showings yet
    }

    
    var mapOptions = {
      center: { lat: 32.7451599752403, lng: -117.137919347596},
      zoom: 11
    };
    map = new google.maps.Map(document.getElementById('rt-map-canvas'),
        mapOptions);
    
    
    var bounds = new google.maps.LatLngBounds();
    
    for(i=0; i<showings.length; i++)
    {
      var showing = showings[i]
      var last_sighting = showing.sightings[showing.sightings.length-1]
      
      var icon = 'https://maps.google.com/intl/en_us/mapfiles/ms/micons/red.png'      
      if(showing.agent_state=="safe")
      {  
        icon = 'https://maps.google.com/intl/en_us/mapfiles/ms/micons/green.png'
      }
      
      bounds.extend(new google.maps.LatLng(last_sighting.lat, last_sighting.lon));
      
      var marker = new google.maps.Marker({
          position: new google.maps.LatLng(last_sighting.lat, last_sighting.lon), 
          map: map, 
          icon: icon,
          animation: google.maps.Animation.DROP});
          
          build_info_window(marker, showing); //for JS closure
    }// end loop
    
    
    map.fitBounds(bounds);

  }
  
  build_info_window = function(marker, showing)
  {
    var template = $('#info-tmpl').html();
    var last_sighting = showing.sightings[showing.sightings.length-1];
    var show_warning="hidden"
    if(showing.agent_state!="safe")
    {  
      show_warning=""
    }
    
    var last_sighting_time = moment(last_sighting.created_at);
    var last_sighting_elapsed = last_sighting_time.fromNow();
    var profile_img_url = showing.showing_owner.profile.profile_pic
    var data ={
      "profile_img_url":profile_img_url, 
      "last_sighting_elapsed": last_sighting_elapsed,
      "show_warning": show_warning,
      "name": showing.agent_name,
      "latlng": last_sighting.lat+", "+last_sighting.lon,
      "address": showing.address,
      "elapsed": showing.time_length,
      "buyer_name": showing.buyer_name,
      "details_link": "/brokerage_dashboard/showings/"+showing.id
    }
    var output = $("#info-tmpl").html()
    var rendered = Mustache.render(template, data);
    var infowindow = new google.maps.InfoWindow({content: rendered});
    
    google.maps.event.addListener(marker, 'click', function() {
      console.log(marker)
      infowindow.open(map, marker);
    });
  }
  
  start_real_time=function()
  {
    console.log("starting real-time map")
    initialize_map();
  }
  

  if(typeof page_id === "string" && (page_id=="index" || page_id=="showings#real_time"))
  {
   
    start_real_time();
  }
 

})