$(function(){

  var start_showing_show=function(){
    console.log("running showing.js");
    initialize_map();
    start_listener();
  }

  var initialize_map = function(){
	if (showing.sightings.length<1)
	{
		return; //show no showings yet
	}
	// new up complex objects before passing them around
	window.directionsService = new window.google.maps.DirectionsService();
	window.map = new window.google.maps.Map(document.getElementById("map-canvas"));
    Tour_startUp(showing.sightings);
    window.tour.loadMap(window.map);
    if (showing.sightings.length > 1){
      window.tour.calcRoute(window.directionsService);
    }
	else
	{
		window.tour.createUniqueMarker(window.map);
	}
  };

  function Tour_startUp(stops) {
  	if (!window.tour) 
	  window.tour = {
		map: null,
		marker: null,
		arrayDirectionsDisplay: [],
		updateStops: function (newStops) {
		  if(stops)
		    stops = stops.concat(newStops);
		  else
			stops = newStops;
		},
		// map: google map object
		// directionsDisplay: google directionsDisplay object (comes in empty)
		loadMap: function (map) {
		  var myOptions = {
		    zoom: 11,
			center: { lat: 32.7451599752403, lng: -117.137919347596},
			mapTypeId: window.google.maps.MapTypeId.ROADMAP
		  };
		  map.setOptions(myOptions);
		  this.map = map;
		},
		prepareRoutes: function(stops){
			var routesReturn = [];
			var excludedTemp = null;
			for (var i = 0; i < stops.length; i++) {
				console.log(i);
				if(i!=0){
					var size = routesReturn.length;
					var p1 = new window.google.maps.LatLng(routesReturn[size-1].lat, routesReturn[size-1].lon);
					var p2 = new window.google.maps.LatLng(stops[i].lat, stops[i].lon);
					var distance = google.maps.geometry.spherical.computeDistanceBetween(p1, p2);
					if(distance > 100){
						if(excludedTemp == null)
							excludedTemp = stops[i];
						else {
							routesReturn.push(excludedTemp);
							routesReturn.push(stops[i]);
							excludedTemp = null;
						}
					}
					else {
						if(excludedTemp != null)
							excludedTemp = null;
						routesReturn.push(stops[i]);
					}
				}
				else
					routesReturn.push(stops[i]);
			}
			return routesReturn;
		},
		calcRoute: function (directionsService) {
		  var me = this;
		  var batches = [];
		  var itemsPerBatch = 10; // google API max = 10 - 1 start, 1 stop, and 8 waypoints
		  var itemsCounter = 0;
		  var wayptsExist = stops.length > 0;
		  var subitemsCounter = 0;
		  var subBatch = [];
		  
		  stops = me.prepareRoutes(stops);
		  
		  for (var j = itemsCounter; j < stops.length; j++) {
		    subitemsCounter++;
			subBatch.push({
			  location: new window.google.maps.LatLng(stops[j].lat, stops[j].lon),
			  stopover: true
			});
			if (subitemsCounter == itemsPerBatch || subitemsCounter == stops.length || j+1 == stops.length ){
			  batches.push(subBatch);
			  subBatch = [];
			  subitemsCounter = 0;
			}
		  }

		  // now we should have a 2 dimensional array with a list of a list of waypoints
		  var combinedResults;
		  
		  var directionsResultsReturned = 0;
		  
		  me.clearMaps();
		  
		  for (var k = 0; k < batches.length; k++) {
			var unsortedResults = [{}]; // to hold the counter and the results themselves as they come back, to later sort
			directionsDisplay = me.createMap();
			
		    var lastIndex = batches[k].length - 1;
			var start = batches[k][0].location;
			var end = batches[k][lastIndex].location;

			// trim first and last entry from array
			var waypts = [];
			waypts = batches[k];
			waypts.splice(0, 1);
			waypts.splice(waypts.length - 1, 1);

			var request = {
			  origin: start,
			  destination: end,
			  waypoints: waypts,
			  travelMode: window.google.maps.TravelMode.WALKING
			};
					
			(function (kk) {
			  directionsService.route(request, function (result, status) {
			    if (status == window.google.maps.DirectionsStatus.OK) {

			      var unsortedResult = { order: kk, result: result };
				  unsortedResults.push(unsortedResult);

				  directionsResultsReturned++;
				  
				  // sort the returned values into their correct order
				  unsortedResults.sort(function (a, b) { return parseFloat(a.order) - parseFloat(b.order); });
				  var count = 0;
				  for (var key in unsortedResults) {
				    if (unsortedResults[key].result != null) {
				      if (unsortedResults.hasOwnProperty(key)) {
					    if (count == 0) // first results. new up the combinedResults object
						  combinedResults = unsortedResults[key].result;
						else {
						  // only building up legs, overview_path, and bounds in my consolidated object. This is not a complete
						  // directionResults object, but enough to draw a path on the map, which is all I need
						  combinedResults.routes[0].legs = combinedResults.routes[0].legs.concat(unsortedResults[key].result.routes[0].legs);
						  combinedResults.routes[0].overview_path = combinedResults.routes[0].overview_path.concat(unsortedResults[key].result.routes[0].overview_path);
					      combinedResults.routes[0].bounds = combinedResults.routes[0].bounds.extend(unsortedResults[key].result.routes[0].bounds.getNorthEast());
						  combinedResults.routes[0].bounds = combinedResults.routes[0].bounds.extend(unsortedResults[key].result.routes[0].bounds.getSouthWest());
						}
						count++;
					  }
					}
				  }
				  directionsDisplay.setDirections(combinedResults);
				  var legs = combinedResults.routes[0].legs;
				  me.marker =  me.createMarker(directionsDisplay.getMap(),legs[legs.length-1].end_location, me.marker);
			    }
			  });
			})(k);
		  }
		},
		clearMaps: function(){
		  for(var i = 0; i < this.arrayDirectionsDisplay.length; i++){
			this.arrayDirectionsDisplay[i].setMap(null);
		  }
		  this.arrayDirectionsDisplay = [];
		},
		createMap: function(){
		  var directionsDisplay = new window.google.maps.DirectionsRenderer({suppressMarkers: true});
		  this.arrayDirectionsDisplay.push(directionsDisplay);	
		  directionsDisplay.setMap(this.map);
		  return directionsDisplay;
		},
		createMarker: function(map, latlng, last_marker) {
		  if(last_marker){
		    last_marker.setMap(null); //remove old
		  }
		  var marker = new google.maps.Marker({
		    position: latlng,
			map: map
		  });
		  return marker;
		},
		createUniqueMarker: function(map){
			var coord = new window.google.maps.LatLng(stops[0].lat, stops[0].lon);
			this.marker = this.createMarker(map, coord, null);
			var bounds = new google.maps.LatLngBounds();
			bounds.extend(coord);
			window.map.fitBounds(bounds);
		}
	  };
	}

	var start_listener = function(){
	  showing_id = showing.id;
	  channel = "showings/"+showing_id;
	  MessageBus.subscribe(channel, function(msg) {
	    sighting = JSON.parse(msg['sighting']);
		add_sighting(sighting);
		pulseBeacon();
	  }, -1); 
	}

	if(typeof page_id === "string" && page_id=="showing#show"){
	  console.log("LOAD MAP");
	  start_showing_show();
	}

});

var pulseBeacon = function(){
  $beacon = $('.beacon')
  $beacon.find('.dot').addClass('pulse').delay(1000).queue(function(){
    $(this).removeClass('pulse');
    $(this).dequeue();
  });
  $beacon.find('.ring').addClass('pulse').delay(1000).queue(function(){
    $(this).removeClass('pulse');
    $(this).dequeue();
  });
}

var add_sighting = function(element){
  window.tour.loadMap(window.map);
  window.tour.updateStops([{ lat: element.lat, lon: element.lon}]);
  window.tour.calcRoute(window.directionsService);
}
