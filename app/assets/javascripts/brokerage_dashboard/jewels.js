$(function() {

  
  var node = $("#navbar-jsx")[0]
  var className = node.getAttribute("data-react-class");

  // Assume className is simple and can be found at top-level (window).
  // Fallback to eval to handle cases like 'My.React.ComponentName'.
  var constructor = window[className] || eval.call(window, className);
  var propsJson = node.getAttribute("data-react-props");
  var props = propsJson && JSON.parse(propsJson);

  navbar_react = ReactDOM.render(React.createElement(constructor, props), node);

  var channel = brokerage_dashboard.mb_notifications_channel;
  console.log(channel)
  
  MessageBus.subscribe(channel, function(msg) {
    console.log("name: " + msg.name + " said: " + msg.data)
     items = navbar_react.state.items.concat([msg.data]);
     navbar_react.setState({ items: items });
  }, -1); 
  
  
});
