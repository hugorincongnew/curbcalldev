$(function()
{
  
  function initialize_map() {
    if (points.length<1)
    {
      return; //show no showings yet
    }
    
    var mapOptions = {
      center: { lat: 33.3999999752403, lng: -117.137919347596},
      zoom: 8
    };
    var map = new google.maps.Map(document.getElementById('map-canvas'),
        mapOptions);
    
    var bounds = new google.maps.LatLngBounds();
    
    for(i=0;i<points.length;i++)
    {
      var showing = points[i]
      console.log(showing.sightings[0])
      if(showing.sightings.length>0)
      {
        var marker = new google.maps.Marker({position: new google.maps.LatLng(showing.sightings[0].lat, showing.sightings[0].lon), map: map})
        bounds.extend(new google.maps.LatLng(showing.sightings[0].lat, showing.sightings[0].lon));
        build_info_window(map,marker, showing); //for JS closure
      }
      
      
    }
    
      map.fitBounds(bounds);

  }
  
  get_agent_profile_pic = function(showing){
	  var profile_img_url = showing.photo_url;
	  if(showing.agent && showing.agent.profile)
		  profile_img_url = showing.agent.profile.profile_pic;
	  else if(showing.showing_owner && showing.showing_owner.profile)
		  profile_img_url = showing.showing_owner.profile.image;
	  return profile_img_url;
  }
  
  build_info_window = function(map, marker, showing)
  {
    var template = $('#today-showings-info-tmpl').html();
    var last_sighting = showing.sightings[showing.sightings.length-1];
    var show_warning="hidden"
    if(showing.agent_state!="safe")
    {  
      show_warning=""
    }
    
    var last_sighting_time = moment(last_sighting.created_at);
    var last_sighting_elapsed = last_sighting_time.fromNow();
    var profile_img_url = get_agent_profile_pic(showing);
    var data ={
      "profile_img_url":profile_img_url, 
      "last_sighting_elapsed": last_sighting_elapsed,
      "show_warning": show_warning,
      "name": showing.agent_name,
      "latlng": last_sighting.lat+", "+last_sighting.lon,
      "address": showing.address,
      "elapsed": showing.time_length,
      "buyer_name": showing.buyer_name,
      "details_link": "brokerage_dashboard/showings/"+showing.id
    }
    var output = $("#info-tmpl").html()
    var rendered = Mustache.render(template, data);
    var infowindow = new google.maps.InfoWindow({content: rendered});
    
    google.maps.event.addListener(marker, 'click', function() {
      console.log(marker)
      infowindow.open(map, marker);
    });
  }
  
  start_index=function()
  {
    initialize_map();
  }
  
  if(window.location.pathname==="/brokerage_dashboard")
  {
    start_index();
  }

})