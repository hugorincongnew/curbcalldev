var MessageList = React.createClass({
  render: function() {    
    var createItem = function(itemText) {
      return <a className="item" href="#"><i className="icon-signin"></i>{itemText}<span className="time"><i className="icon-time"></i>13 min.</span></a>
    };
    return (
      <div className="notifications">
        <h3>You have {this.props.items.length} new notifications</h3>{this.props.items.map(createItem)}
        <div className="footer">
          <a className="logout" href="#">
            View all notifications
          </a>
        </div>
      </div>
      );
  }
});


var NotificationsJewel = React.createClass({
  render: function() {
    return (
      <li className="notification-dropdown hidden-xs hidden-sm">
        <a href="#" className="trigger">
          <i className="icon-warning-sign" />
          <span className="count">{this.props.items.length} </span>
        </a>
        <div className="pop-dialog">
          <div className="pointer right">
            <div className="arrow" />
            <div className="arrow_border" />
          </div>
          <div className="body">
            <a href="#" className="close-icon"><i className="icon-remove-sign" /></a>
            <MessageList items={this.props.items} />
          </div>
        </div>
      </li>
    );
  }
});

var NavBar = React.createClass({
  getInitialState: function() {
    return {items: []};
  },
  render: function() {
    return (

      <ul className="nav navbar-nav pull-right hidden-xs">
        <NotificationsJewel items={this.state.items} />
        <li className="dropdown">
          <a href="#" className="dropdown-toggle hidden-xs hidden-sm" data-toggle="dropdown">
            Your account
            <b className="caret" />
          </a>
          <ul className="dropdown-menu">
            <li><a href="#">Account settings</a></li>
            <li><a data-method="delete" href="/users/sign_out" rel="nofollow">Log out</a></li>
          </ul>
        </li>
      </ul>
    );
  }
});