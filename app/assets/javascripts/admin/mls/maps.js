$(function()
{
  var markers = [];
  var image = 'https://lh3.ggpht.com/hx6IeSRualApBd7KZB9s2N7bcHZIjtgr9VEuOxHzpd05_CZ6RxZwehpXCRN-1ps3HuL0g8Wi=w9-h9';
  var map;
  
  var initialize = function(){
    
    var mapOptions = {
      center: { lat: 32.7451599752403, lng: -117.137919347596},
      zoom: 14
    };
    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    
    google.maps.event.addListener(map, 'idle', function(){
      deleteMarkers();
      var lat_lng_bounds = map.getBounds();
      fetchListings(lat_lng_bounds.toUrlValue());
     });
     
     
     // binds a map marker and infoWindow together on click
     bindInfoWindow = function(marker, map, infowindow, html) {
         google.maps.event.addListener(marker, 'click', function() {
             infowindow.setContent(html);
             infowindow.open(map, marker);
         });
     }
    
  };
  
  // Sets the map on all markers in the array.
  setAllMap = function (map) {
    for (var i = 0; i < markers.length; i++) {
      markers[i].setMap(map);
    }
  }
  
  addMarker = function (location) {    
		var marker = new google.maps.Marker({
		    map: map,
		    position: location,
        icon: image
		});
    
    markers.push(marker);
    return marker;
  }
  
  deleteMarkers = function () {
    setAllMap(null);
    markers = [];
  }
  
  
  var fetchListings = function(query_str) {
    var infowindow =  new google.maps.InfoWindow({
    		    content: ''
    		});
    
    jQuery.ajax({
    			url : 'listings?bounds='+query_str,
    			dataType : 'json',
    			success : function(response) {
            
    					listings = response.listings;
              
    					for (l in listings) {

    						tmpLatLng = new google.maps.LatLng( listings[l].lat, listings[l].lon);
                
                var marker = addMarker(tmpLatLng)

                bindInfoWindow(marker, map, infowindow, '<b>'+listings[l].address);
    				}
    			}
    		});
        
  };
  
  
  if(typeof page_id === "string" && page_id=="mls#map")
  {
    initialize();
  }
});
