class GlvarUpsertWorker
  include Sidekiq::Worker

  def perform(options)
    options.recursive_symbolize_keys!

  
    SlackWorker.perform_async("Running GlvarUpsertWorker")
    puts 'GlvarUpsertWorker running...'
    
    Tools::Import::Import.use :glvar
    Tools::Import::Import.import(options) 
    
    puts 'GlvarUpsertWorker complete...'
    SlackWorker.perform_async("Finished GlvarUpsertWorker")
  end
end