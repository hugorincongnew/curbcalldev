# app/workers/push_workers.rb
class PushWorker
  include Sidekiq::Worker

  def perform(user_id, message, opts={})
    data = { alert: message, sound: "sosumi.aiff"}
    data.merge!(opts)
    push = Parse::Push.new(data)
    query = Parse::Query.new(Parse::Protocol::CLASS_INSTALLATION).eq('user_id', user_id)
    push.where = query.where
    push.save
  end
end