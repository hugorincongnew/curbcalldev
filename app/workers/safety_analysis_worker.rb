# app/workers/push_workers.rb
class SafetyAnalysisWorker
  include Sidekiq::Worker

  def perform(sighting_id)
    sighting = Sighting.find(sighting_id)
    showing = sighting.showing
    showing.perform_safety_analysis
  end
end