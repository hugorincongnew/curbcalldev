class SlackWorker
  include Sidekiq::Worker

  def perform(msg)
    return unless Rails.env=="production"
    puts 'SlackWorker running...'
    
    notifier = Slack::Notifier.new("https://hooks.slack.com/services/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")
    notifier.ping msg
    
    puts 'SlackWorker complete...'
  end
end