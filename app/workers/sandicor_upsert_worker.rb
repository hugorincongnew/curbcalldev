class SandicorUpsertWorker
  include Sidekiq::Worker

  def perform(options)
    options.recursive_symbolize_keys!

  
    SlackWorker.perform_async("Running SandicorUpsertWorker")
    puts 'SandicorUpsertWorker running...'
    
    Tools::Import::Import.use :sandicor
    Tools::Import::Import.import(options) #rename import to upsert/merge
    
    puts 'SandicorUpsertWorker complete...'
    SlackWorker.perform_async("Finished SandicorUpsertWorker")
  end
end