class DispatchTimeoutWorker
   include Sidekiq::Worker
   
   def perform(showing_id)
     showing = Showing.find(showing_id)
     showing.timeout_search
   end
end
