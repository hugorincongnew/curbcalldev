class MessageWorker
  include Sidekiq::Worker
  sidekiq_options :retry => false

  def perform(klass_name, id, showing_id, args={})
    message  = CurbCall::Messages::Types.const_get(klass_name).new(args)
    message.deliver_all(id, showing_id)
  end
end