class UpdateCrmlsWorker
  include Sidekiq::Worker

  def perform(options)
    options.recursive_symbolize_keys!

  
    SlackWorker.perform_async("Running UpdateCrmlsWorker")
    puts 'SlackWorker running...'
    
    Tools::Import::Import.use :crmls
    Tools::Import::Import.import(options) #rename import to upsert/merge
    
    puts 'SlackWorker complete...'
    SlackWorker.perform_async("Finished UpdateCrmlsWorker")
  end
end