# app/workers/hard_worker.rb
class HardWorker
  include Sidekiq::Worker

  def perform(time)
    sleep(time)
    puts 'Doing hard work'
  end
end