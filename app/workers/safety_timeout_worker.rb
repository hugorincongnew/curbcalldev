class SafetyTimeoutWorker
   include Sidekiq::Worker
   
   def perform(showing_id)
     showing = Showing.find(showing_id)
     showing.check_ack_timeout
   end
end
