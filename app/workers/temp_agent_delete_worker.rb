class TempAgentDeleteWorker
  include Sidekiq::Worker

  def perform(agent_id)
    agent = Agent.find(agent_id)
    agent.destroy!
  end
end