class ArmlsUpsertWorker
  include Sidekiq::Worker

  def perform(options)
    options.recursive_symbolize_keys!

  
    SlackWorker.perform_async("Running ArmlsUpsertWorker")
    puts 'ArmlsUpsertWorker running...'
    
    Tools::Import::Import.use :armls
    Tools::Import::Import.import(options) 
    
    puts 'ArmlsUpsertWorker complete...'
    SlackWorker.perform_async("Finished ArmlsUpsertWorker")
  end
end