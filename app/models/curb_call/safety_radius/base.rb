require "rubygems"
require "bundler/setup"


module CurbCall
  module SafetyRadius
    class Base
      attr_accessor :points, :radius, :origin, :threshold
      STATES={safe: "safe", warning: "warning", unknown: "unknown", low_data: "low_data"}
      SETTINGS={safety_radius: "safety_radius", safety_threshold: "safety_threshold", safety_sync_message: "safety_sync_message", 
    timeout_message:"timeout_message", warning_timeout: "warning_timeout", push_panic_message: "push_panic_message"}
      
      def initialize(points, radius, threshold)
        @points = points
        @radius = radius
        @origin = set_origin()
        @threshold = threshold
      end
      
      def self.factory(radius, points, threshold)
        if points.empty?
          raise "0 data points specified. Cannot run analysis on 0 data points"
        end
        radius = radius
        points = points
        Base.new(points, radius, threshold)
      end
      
      def run_analysis
        
        result = Result.new
        
        #not enough points to take a sample yet
        if @points.length < @threshold
          result.state = STATES[:low_data]
          return result
        end
        
        #grab the last x (thres) points
        # TODO rename threshold to window_length
        sample = @points[-@threshold..-1]
        
        #I got a little too excited w/ this one-liner
        # take the lsat 5 points (the sample) and run each through the is_inside_radius? routine
        # then I run uniq on that. 
        # outputs: 
        # true false true false ture false -> uniq -> true false, default to safe
        # [] -> uniq -> [], default to unknown
        # true true true true -> uniq -> true, safe, inside radius
        # false false false   -> uniq -> false, warning, outside radius for whole sample window
        
        results = sample.collect{|point| is_inside_radius?(@origin, @radius, point)}.uniq

        if(results.length > 1)
          result.state = STATES[:safe]
        elsif(results.length < 1)
          result.state = STATES[:unknown]
        elsif(results.first==true)#anythying here is an array of size 1, so either TRUE or FALSE
          result.state = STATES[:safe]
        elsif(results.first==false)
          result.state = STATES[:warning]
        else
          result.state = STATES[:unknown]
        end
        
        result
        
      end
      

      def is_inside_radius?(origin, radius, point)
        
        # x >= centerX - radius && x <= centerX + radius &&
        #             y >= centerY - radius && y <= centerY + radius;
        
        lat_mag = (origin.lat - point.lat).abs
        lng_mag = (origin.lng - point.lng).abs
        
        lat_valid = lat_mag < radius
        lng_valid = lng_mag < radius
        
        lat_valid && lng_valid
        
      end
      
      private       
      def set_origin
        #get first 5 sightings and set average as origin
        if(@points.length<5)
          @origin = @points.first
        else
          @origin = average_sightings(@points[0..4])
        end
      end
      
      
      def average_sightings(sightings)
        lat = sightings.collect(&:lat).reduce(:+) / sightings.length
        lng = sightings.collect(&:lng).reduce(:+) / sightings.length
        
        avg = CurbCall::SafetyRadius::LatLng.new(lat,lng)
      end
      
    
      
    end
  end
end