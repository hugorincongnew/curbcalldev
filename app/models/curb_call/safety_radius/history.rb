require "rubygems"
require "bundler/setup"


module CurbCall
  module SafetyRadius
    class History
      
      EVENTS = {start: "start", end: "end", warning: "warning", danger: "danger", syn_ack: "syn_ack", safe:"safe", panic:"panic", 
        completed: "completed"}
      MESSAGES = 
      {
        start: "The showing began.",
        end: "The showing was completed.",
        warning: "The agent left the safety-radious, we contacted them...",
        danger: "The agent has left the safety radius for a significant amount of time and has not responsded to safety messages",
        syn_ack: "The user acknowledged the safety message",
        panic: "The user pressed the panic button",
        completed: "The showing successfully ended."
      }
      
      def initialize(showing_id, redis)
        @showing_id = showing_id
        @redis = redis
      end
      
      def key
        key = "showings:#{@showing_id}:events"
      end
      
      # add event
      def add(event)
        
        
        event = {ts: Time.zone.now.to_i, event: event}
        val = JSON.dump(event)

        Sidekiq.redis do |redis|
          redis.rpush key, val #add it to end of list (not front)
        end
        
      end
      
      #get all the history
      def all
        
        items = []
        Sidekiq.redis do |redis|
          items = redis.lrange key, 0, -1
        end
        
        #deseralize
        events =[]
        items.each do |item|
          events << parse(item)
        end
        
        events
      end
      
      private
      
      def parse(event_hash)
        event = JSON.load(event_hash).symbolize_keys
        date = Time.zone.at(event[:ts])
        event[:ts]=date
        event
        
      end
      
    end
  end
end