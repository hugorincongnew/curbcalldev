require "rubygems"
require "bundler/setup"


module CurbCall
  module SafetyRadius
    class Timer
      
      TIMEOUT = 300 #seconds - 5 min
      
      def self.timeout?(start, now=Time.zone.now)
        elapsed = now - start #seconds diff
        
        timeout = elapsed > TIMEOUT
      end
    end
  end
end