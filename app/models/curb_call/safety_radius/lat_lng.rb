require "rubygems"
require "bundler/setup"

module CurbCall
  module SafetyRadius

    class LatLng
      attr_accessor :lat, :lng
      def initialize(lat, lng)
        @lat = lat
        @lng = lng
      end
      
      # makes collection of Sighting into LatLng
      def self.collection_factory(sightings)
        latlngs=[]
        sightings.each do |sighting|
          latlng = LatLng.new(sighting.lat, sighting.lon)
          latlngs << latlng
        end
        latlngs
      end
     
      
    end
  end
end