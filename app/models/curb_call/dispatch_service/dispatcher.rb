module CurbCall
  module DispatchService
    class Dispatcher
      SETTINGS={delete_timeout: "delete_timeout"}

      attr_accessor :errors


      def initialize(showing=nil)
        @showing = showing
        @brokerage = !showing.nil? ? showing.brokerage : nil
        @errors=[]
      end

      def errors
        if @errors.any?
          return {errors: @errors}
        else
          return @errors
        end
      end

      def notify_agents(showing, agents)
        #agents.each do |agent|
        #  agent.notify(:lead)
        #end

        #start timer
        #jid = DispatchTimeoutWorker.perform_at(2.minutes.from_now, @showing.id)
        #log_notify_job(jid)
        CurbCall::Messages::Types::NotifyAgent.enqueue(showing, agents)
      end


      def log_notify_job(jid)
        Sidekiq.redis do |redis|
          redis.set("showings:#{@showing.id}:dispatch_timeout_jid", jid)
        end
      end

      def lookup_notify_job
        jid=nil
        Sidekiq.redis do |redis|
          jid = redis.get("showings:#{@showing.id}:dispatch_timeout_jid")
        end
        jid
      end

      def find_agents(opts={})

        p "SHOWING REQUEST FIND AGENT OPTS ARE: #{opts}"

        if opts[:agent_email] != nil # looking for an agent via email
          user = User.where('lower(email) = ?', opts[:agent_email].downcase).last
          if user.nil?
            agent = Agent.where(id:0) #return no agents
          else
            agent = Agent.by_brokerage(@brokerage).where("created_at > ? AND user_id = ?",Date.yesterday, user.id)
          end
          agents = agent
        elsif opts[:agent_id] != nil # looking for a particular agent id
          agent = Agent.by_brokerage(@brokerage).where(id:opts[:agent_id])
          agents = agent
        else
          #scan the Agents table for GPS coords close to user
          agents = []
          radius = opts[:radius] || 10
          lat = opts[:lat] || 0
          lng = opts[:lng] || 0
          origin = Geokit::LatLng.new(lat, lng)
          agents = Agent.by_brokerage(@brokerage).within(radius, origin: origin)#sort this
        end

        #sandbox override
        if opts[:sandbox]
          agent = create_temp_agent(lat: opts[:lat], lon: opts[:lng], name: "Sandbox Agent", phone:"8182539344", company:"EggMos")
          agents = [agent]
        end


        if agents.any?
        else
          @errors << "Could not find any agents"
        end

        p "AGENTS ARE: #{agents}"
        return agents

      end

      private

      def create_temp_agent(params={})
        agent = Agent.create! params
        #queue for deletion
        TempAgentDeleteWorker.perform_at(1.minutes.from_now, agent.id)
        agent

      end

    end
  end
end
