# To be included into a Showing model
module CurbCall
  module StateMachineExt
    def transition!(state)
      case(state)
      when Showing::STATES[:warning] then self.syn_agent
      when Showing::STATES[:danger]
        if(self.agent_state == Showing::STATES.fetch(:safe))
          self.panic
        else
          self.ack_timeout
        end
      when Showing::STATES[:safe] then self.syn_ack
      end
    end
  end
end