require "open-uri"


module CurbCall
  class Oauth
    
    def self.fb_api_get(access_token)
      uri = URI.parse("https://graph.facebook.com/me?access_token=#{access_token}")
      body = uri.read
    end
    
    def self.get(access_token)
      #now make FB api request to /me w/ token
      begin
        body = CurbCall::Oauth.fb_api_get(access_token)
        mjson = JSON.parse(body)
        return mjson
      rescue
        return nil
      end
    end
  end
end