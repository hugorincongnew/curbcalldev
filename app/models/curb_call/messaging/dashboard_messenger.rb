require "rubygems"
require "bundler/setup"


module CurbCall
  module Messaging
    class DashboardMessenger
      def self.channel(brokerage)
        "#{ENV.fetch('MB_DASH_CHANNEL')}/#{brokerage.id}"
      end
      
      def self.publish(brokerage, data, opts = nil)
        MessageBus.publish(DashboardMessenger.channel(brokerage), data, opts)
      end
    end
  end
end
