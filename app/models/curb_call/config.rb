require 'json'

module CurbCall
  module Config
    
    def self.get(key)
      value =  CurbCall::Config[key]
      if value.nil?
        raise "The settign: '#{key}', did not return a value (was nil). You must set it in the curb call admin panel!"
      end
      value
    end
    
    def self.p_get(key)
      val = CurbCall::Config.get(key)
      JSON.load(val)
    end
    
    def self.[](key)
      Sidekiq.redis do |redis|
        redis.get("cc:cfg:#{key}")
      end
    end
 
    def self.[]=(key, value)
      Sidekiq.redis do |redis|
        redis.set("cc:cfg:#{key}", value)
      end
    end
  end
end