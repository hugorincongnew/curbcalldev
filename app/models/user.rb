class User < ActiveRecord::Base
  has_one :profile
 
  has_many :identities, :dependent => :destroy   #oauth FB, et. al.
  has_many :emergency_contacts, as: :contactable
  accepts_nested_attributes_for :identities
  accepts_nested_attributes_for :emergency_contacts, allow_destroy: true
  acts_as_token_authenticatable
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :confirmable,
         :recoverable, :rememberable, :trackable, :validatable#, :token_authenticatable
         
  after_create :agent_welcome_email, :if => :agent?

  # Setup accessible (or protected) attributes for your model
  #attr_accessible :name, :email, :password, :password_confirmation, :remember_me, :mls, :organization
  # attr_accessible :title, :body
  
  before_save :ensure_authentication_token
  after_create :default_profile
  
  belongs_to :brokerage
  has_many :agents
  has_many :dev_tokens
  has_many :agent_datas
  has_many :showings, as: :showing_owner
  has_many :history_agents

  attr_accessor :temporary_password
  
  scope :for_brokerage, ->(brokerage){where(brokerage: brokerage)}
#LS
  scope :for_range, ->(start_date,end_date) { where('users.created_at BETWEEN ? AND ?', start_date, end_date)}

  scope :agent_hist, ->(start_date,end_date) {
        joins{history_agents.user}
        .joins{showings}
        .select{[avg(showings.updated_at-history_agents.start_time).as(avg_rsp)]}
        .where{ (history_agents.start_time >= start_date) & (history_agents.end_time <= end_date) & (history_agents.start_time <= showings.created_at) & (history_agents.end_time >= showings.created_at) }
}
 scope :notfollow_con, ->(start_date,end_date) {
        joins{history_agents.user}
        .joins{showings}
        .select{[showings.id,email,(history_agents.end_time-showings.created_at).as(time)]}
        .where{(history_agents.start_time <= showings.created_at) & (history_agents.end_time >=  showings.created_at) & (history_agents.start_time >= start_date) & (history_agents.end_time <= end_date) & (showings.status != 'completed' )}
}

 



#LS    
  def skip_confirmation!
    self.confirmed_at = Time.now
  end
  
  def reset_authentication_token!
    super
    history = HistoryAgent.not_finish(self.user).first
    history.finish if !history.nil?
  end
  
  def confirmation_required?
  	confirmed_at.nil?
  end
  
  #quick idea I came up w/ to build the profile image -- egg
  def image_url
    "https://s3.amazonaws.com/curbcallImages/#{self.id}_profile_pic.jpg"
  end
  
  def agent_welcome_email
      # TODO add flag if email already sent
      AgentMailer.welcome(self).deliver
  end
  
  # find or create user from oauth
  def self.from_oauth(auth)
    user = joins(:identities).where(identities:{uid: auth['id']}).first || new_from_oauth(auth).save!
    # user.oauth_token = auth["credentials"]["token"]
    # user.oauth_secret = auth["credentials"]["secret"]
    # user.save!
    user
  end
  
  # create a new user from oauth OR form hash
  def self.factory_with_oauth(signup_params)
    access_token = signup_params[:access_token]
    fb_user = CurbCall::Oauth.get(access_token)
    
    if(fb_user)
      user = User.new_from_oauth(fb_user)
    else
      user = User.new(signup_params)
    end

    user
  end
  
  # build a new user object from fb oauth
  # populate user fields w/ FB graph data
  def self.new_from_oauth(fb_user)
    email = fb_user.fetch("email")
    password = Devise.friendly_token[0,20]
    name = "#{fb_user["first_name"]} #{fb_user["last_name"]}"
    fid = fb_user["id"]
    fb_profile_pic_url = "https://graph.facebook.com/#{fid}/picture?type=large&width=640"
    
    user = User.new do |u|
      u.name = name
      u.email = email
      u.password = password
      u.password_confirmation = password
    end
  
    user.identities.build provider: "facebook", uid: fid
    user.build_profile email:email, name: name, profile_pic: fb_profile_pic_url
    user
  end
  

  
  def notify_contacts(event, opts={})
    contacts = self.emergency_contacts
    contacts.each do |contact|
      contact.notify(event, opts)
    end
  end
  
  def internal_code
    Digest::SHA256.hexdigest(self.id.to_s)[0..6].upcase
  end
  
  
  def signin_hash
    
    response_ds = { :success => true,
                      :info => "Logged in",
                      :data => { :auth_token => authentication_token,
                                 :user_id => id,
                                 :name => name,
                                 :email => email,
                                 :organization => brokerage_id,
                                 :bio => bio,
                                 :image => self.try(:profile).try(:profile_pic),
                                 :phone => phone,
                                 :user_agent =>agent, #verify agent,
                                 :dev_token => dev_token } }
  end
  
  private
  
  def default_profile
    Profile.create(user: self, email: self.email)
  end
end
