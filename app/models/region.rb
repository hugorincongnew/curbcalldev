class Region < ActiveRecord::Base
	has_many :zips
	belongs_to :mls
end
