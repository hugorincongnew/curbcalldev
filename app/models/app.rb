class App < ActiveRecord::Base
  belongs_to :developer
  
   after_validation :gen_token, on: [ :create ]
   
   
   private
   
   def gen_token
       self.client_id = SecureRandom.hex(20)
       self.client_secret = SecureRandom.hex(20)
       self.access_token = SecureRandom.hex(20)
       self.sandbox_access_token = SecureRandom.hex(20)
   end
end
