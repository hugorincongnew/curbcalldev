class Sighting < ActiveRecord::Base
  validates :lat, :lon, presence: true
  belongs_to :showing
  
  scope :close_to, -> (latitude, longitude, distance_in_meters = 2000) {
      where(%{
        ST_DWithin(
          ST_GeographyFromText(
            'SRID=4326;POINT(' || sightings.lon || ' ' || sightings.lat || ')'
          ),
          ST_GeographyFromText('SRID=4326;POINT(%f %f)'),
          %d
        )
      } % [longitude, latitude, distance_in_meters])
    }
    
    
    def self.factory(sighting_params)
      sighting = Sighting.new(sighting_params)
      success = sighting.save
      SafetyAnalysisWorker.perform_async(sighting.id) if success
      
      return sighting
    end

end
