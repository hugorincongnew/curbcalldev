
class Setting

  include ActiveModel::Validations
  include ActiveModel::Conversion
  extend ActiveModel::Naming

  attr_accessor :key, :val

  validates :key, :presence => true
  validates :val, :presence => true

  def initialize(attributes = {})
    attributes.each do |name, value|
      send("#{name}=", value)
    end
  end
  
  def id
    self.key
  end
  
  def update(attribs)
    val = attribs[:val]
    CurbCall::Config[self.key]=val
  end

  def persisted?
    true
  end
  
  def self.find(key)
    val = CurbCall::Config[key]
    Setting.new key: key, val:val
  end

end