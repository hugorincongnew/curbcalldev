class Lead < ActiveRecord::Base
	has_many :eligible_agents
  has_many :showings
  belongs_to :buyer, :class_name => 'User', :foreign_key => "buyer_id"
  belongs_to :agent
  belongs_to :listing
  
  # validates :listing, presence: true
  # validates :agent, :buyer, presence: true
  
  
  STATES={claimed: 'claimed', unanswered: 'unanswered'}
  
  state_machine :status, initial: :unanswered do
    event :claim do
      transition unanswered: :claimed
    end
    
    after_transition unanswered: :claimed do |lead, transition|
      AgentMailer.lead_info(lead).deliver
    end

    event :decline do
      transition unanswered: :declined
    end
  end
  
end
