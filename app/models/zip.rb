class Zip < ActiveRecord::Base
	
	belongs_to :region
	
	has_and_belongs_to_many :brokerages, join_table: :brokerage_zips
	has_and_belongs_to_many :mls, join_table: :mls_zips
end

# IMPORT CSV OF ZIPS
# require 'csv'
# CSV.foreach('us_postal_codes.csv', headers: true) do |row|
# 	@a = row.to_hash
# 	zip = Zip.create!(code: @a["Postal Code"], city: @a["Place Name"], state: @a["State Abbreviation"], county: @a["County"], lat: @a["Latitude"], lon: @a["Longitude"])
# end