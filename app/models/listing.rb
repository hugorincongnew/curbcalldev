class Listing < ActiveRecord::Base
	include ImageQuery
	acts_as_mappable :default_units => :miles,
                   :default_formula => :flat,
                   :distance_field_name => :distance,
                   :lat_column_name => :lat,
                   :lng_column_name => :lon

    belongs_to :mls
    
    STATES = {active: 'active', pending:'pending', sold: 'sold'}
    #add state machine gem
end
