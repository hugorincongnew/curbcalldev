class Email < ActiveRecord::Base

  belongs_to :user

  after_create { ParseEmail.parse(self) }

  default_scope { order(created_at: :desc) }
  scope :parsed,   -> { where(parsed: true) }
  scope :unparsed, -> { where(parsed: false) }
  scope :by_domain, ->(domain) { where(from: domain) }
  scope :parsed_by_domain,  ->(domain) { parsed.by_domain(domain) }
  scope :unparsed_by_domain,->(domain) { unparsed.by_domain(domain) }

  scope :emails_parsed_since, ->(date) { parsed.where('created_at > ?', date) }
  scope :parsed_in_30_days,   -> { emails_parsed_since(Time.zone.now - 30.days) }
  scope :parsed_in_7_days,    -> { emails_parsed_since(Time.zone.now - 7.days) }
  scope :parsed_in_1_day,     -> { emails_parsed_since(Time.zone.now - 1.day) }

  def find_email_templates(from)
    user.templates.where(from: from)
  end

end
