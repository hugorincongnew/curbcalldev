class Brokerage < ActiveRecord::Base

	has_many :users, dependent: :destroy
	has_many :agents, dependent: :destroy
	has_many :showings, through: :users
	has_many :showings_request, class_name: "Showing"

	has_and_belongs_to_many :mls, join_table: :mls_brokerages, dependent: :destroy
	has_and_belongs_to_many :zips, join_table: :brokerage_zips, dependent: :destroy

	accepts_nested_attributes_for :users

	before_create :gen_token
	after_create :generate_lead_email


	private

	def gen_token
		self.access_token = SecureRandom.hex(20)
	end

	def generate_lead_email
		random_token = generate_random_token
		if email_token_is_used?(random_token)
			while email_token_is_used?(random_token)
			 random_token = generate_random_token
			end
		else
			self.update(lead_email: random_token+"@curbcallcollect.com")
		end
	end

	def generate_random_token
		SecureRandom.urlsafe_base64(nil, false).downcase
	end

	def email_token_is_used?(random_token)
		Brokerage.where(lead_email: random_token).count >= 1
	end
end
