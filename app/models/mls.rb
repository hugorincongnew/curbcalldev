class Mls < ActiveRecord::Base

	has_many :regions
  # has_many :zips
	#has_many :brokerages
	has_many :listings
	has_and_belongs_to_many :brokerages, join_table: :mls_brokerages
	has_and_belongs_to_many :zips, join_table: :mls_zips
end
