class Agent < ActiveRecord::Base
	acts_as_mappable :default_units => :miles,
                   :default_formula => :flat,
                   :distance_field_name => :distance,
                   :lat_column_name => :lat,
                   :lng_column_name => :lon

    belongs_to :user
    belongs_to :brokerage
    
    before_save :set_brokerage
    after_save :write_log
    
    before_destroy :write_finish_log
    
    scope :by_brokerage, ->(brokerage){ joins(:brokerage).where(brokerage: brokerage) }
    
    
    #notify of new lead
    def notify(opts={})
      # TODO coordinate w/ seth to ensure this will cause the agent-client to respond
      # may need to use pusher for legacy support
      PushWorker.perform_async(self.user_id, ::EligibleAgent::WAKE_UP_MESSAGE)
    end
    
    def new_request
      history = write_log
      history.num_request += history.num_request + 1
      history.save
    end
    
    private
    
    def set_brokerage
    	self.brokerage = self.user.brokerage
    end
    
    def write_log
      history = HistoryAgent.not_finish(self.user).first
      if history.nil?
        history = HistoryAgent.create(user: self.user, start_time: Time.now)
      end
      history
    end
    
    def write_finish_log
      history = HistoryAgent.not_finish(self.user).first
      history.finish if !history.nil?
    end
end
