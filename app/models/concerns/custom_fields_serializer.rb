module CustomFieldsSerializer
  extend ActiveSupport::Concern
  
  included do
    def self.custom_serializer(fields=[], objects=[])
      objects.map do |obj|
        hash = OpenStruct.new
        fields.each do |field|
          hash.send(:"#{field}=", obj.send(field))
        end
        hash.to_h
      end
    end
  
  end
end