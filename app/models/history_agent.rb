class HistoryAgent < ActiveRecord::Base
  belongs_to :user
  belongs_to :showing
  
  scope :not_finish, -> (user){ where(user: user, end_time: nil) }
  scope :length_of_time, ->(brokerage_find, start_time_find, end_time_find){ 
    joins{user.brokerage}.group{user.id}
      .select { [user.id.as(user_id), user.email.as(user_email), sum(end_time - start_time).as(length)] }
      .where{ (user.brokerage == brokerage_find) & (start_time >= start_time_find) & (end_time <= end_time_find) }      
  }
  scope :agent_notified, ->(brokerage_find, start_time_find, end_time_find){ 
    joins{user.brokerage}.group{user.id}
      .select { [user.id.as(user_id), user.email.as(user_email), sum(num_request).as(total_request)] }
      .where{ (user.brokerage == brokerage_find) & (start_time >= start_time_find) & ((end_time <= end_time_find) | (end_time == nil)) }      
  }

  scope :ignored_con, ->(brokerage_find, start_time_find, end_time_find){ 
    joins{user.brokerage}.group{user.id}
     .select {[user.id.as(user_id), user.email.as(user_email), count(start_time).as(time)]}
     .where {(user.brokerage == brokerage_find) & (start_time >= start_time_find) & (start_time <= end_time_find) & (end_time == nil) }
  }
  
  
  def finish
    self.end_time = Time.now
    self.save
  end
end
