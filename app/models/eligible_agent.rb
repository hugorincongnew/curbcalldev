class EligibleAgent < ActiveRecord::Base
	belongs_to :lead
  attr_reader :sidekiq_id  #for test's to pick if if job was queued in SK
  
  after_create :wake_up_client
  
  #this is a routine to wake up the iOS client
  #hopefully this APNS push will cause the user to bring
  #the app into the foreground where the app can use
  #pusher for communication
  WAKE_UP_MESSAGE="You've Been Requested!"
  def wake_up_client
     agent = Agent.find(self.agent_user_id) #seth's cluster :)
     @sidekiq_id=PushWorker.perform_async(agent.user_id, WAKE_UP_MESSAGE)
  end
end
