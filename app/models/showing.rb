class Showing < ActiveRecord::Base
  acts_as_mappable :default_units => :miles,
                   :default_formula => :flat,
                   :distance_field_name => :distance,
                   :lat_column_name => :lat,
                   :lng_column_name => :lng

  belongs_to :showing_owner, polymorphic: true
  belongs_to :brokerage
  belongs_to :lead
  belongs_to :user
  has_many :sightings
  has_many :history_agent
#ls
  
  scope :take_called, -> {select {[ avg(updated_at - created_at).as(prom) ]}   }
  scope :called, ->{ where(called: [true]) }
  
#ls
  scope :for_range, ->(start_date,end_date) { where('showings.created_at BETWEEN ? AND ?', start_date, end_date)}
  scope :active, ->{where(status: ["waiting", "transit", "started"])}
  scope :waiting, ->{where(status: ["waiting"])}
  scope :for_brokerage, ->(brokerage){ where(brokerage: brokerage) }
  scope :claimed, ->{ where(status: ["transit", "completed", "started"]) }
  scope :expired, ->{ where(status: ["expired"]) }
  scope :completed, ->{ where(status: ["completed"]) }
  scope :to_expired, ->{ where{(showing_owner_id == nil) & (created_at <= (Time.now - 2.minutes).utc) } }
  scope :search_param, ->(search, status_state_search=''){ 
    id_search = search
    search = "%#{search}%"
    status_state_search = "%#{status_state_search}%"
    includes{[showing_owner(User), sightings]}
      .joins{[showing_owner(User).outer]}
      .where{ ((id == id_search) | (showing_owner(User).email.like search) | (sift  :agent_name_or_buyer_name_or_buyer_email_or_address_contains, search) | (sift :lat_or_lng_equals, id_search)) & (status.like status_state_search) }
  }
  
  sifter :agent_name_or_buyer_name_or_buyer_email_or_address_contains do |string|
    agent_name.matches(string) | buyer_email.matches(string) | buyer_name.matches(string) | address.matches(string)
  end
  
  sifter :lat_or_lng_equals do |string|
    (lat == string) | (lng == string)
  end
  
  STATES = { safe: "safe", warning: "warning", danger: "danger"}
  SHOWING_STATES={waiting: "waiting", transit: "transit", started:"started", completed:"completed", pending: "pending", timeout: "timeout", expired:"expired"}

  include CurbCall::Messages
  include CurbCall::StateMachineExt
  # include CurbCall::DispatchService

  state_machine :status, initial: SHOWING_STATES.fetch(:waiting) do
    after_transition to: SHOWING_STATES[:started], :do => :on_start
    after_transition to: SHOWING_STATES[:completed], :do => :on_complete


    event :attach_agent do
      transition SHOWING_STATES.fetch(:waiting) => SHOWING_STATES.fetch(:transit)
    end

    event :timeout_search do
      transition SHOWING_STATES.fetch(:waiting) => SHOWING_STATES.fetch(:timeout)
    end

    event :start do
      transition SHOWING_STATES.fetch(:transit) => SHOWING_STATES[:started]
    end

    event :complete do
      transition SHOWING_STATES[:started] => SHOWING_STATES[:completed]
    end

    event :wait do
      transition SHOWING_STATES[:started]=> SHOWING_STATES[:pending]
    end
    
    event :expired do
      transition all => SHOWING_STATES[:expired]
    end

  end

  state_machine :agent_state, initial: STATES[:safe] do

    after_transition STATES[:safe] => STATES[:warning], do: :sync_agent
    after_transition STATES[:warning] => STATES[:danger], do: :on_exit_radius
    after_transition STATES[:warning] => STATES[:safe], do: :on_syn_ack
    after_transition STATES[:safe] => STATES[:danger], do: :on_panic

    event :syn_agent do
      transition STATES[:safe] => STATES[:warning]
    end

    event :syn_ack do
      transition [STATES[:warning], STATES[:danger]] => STATES[:safe]
    end

    event :ack_timeout do
      transition STATES[:warning] => STATES[:danger]
    end

    event :panic do
      transition all => STATES[:danger]
    end


  end

  #called by redis job
  def check_ack_timeout(now=Time.zone.now)
     start = self.history.all.last[:ts]
     expired = CurbCall::SafetyRadius::Timer.timeout?(start, now)

     if(expired)
       self.ack_timeout
     end

     expired
  end

  def history
    CurbCall::SafetyRadius::History.new(self.id, nil)
  end



  def on_complete
    p "on_complete"
    #showing has ended safely
    unless self.showing_owner_id.nil?
      message = CurbCall::Messages::Types::ShowingCompletedMessage.new(self)
      messaging_service = CurbCall::Messages::MessagingService.new(message, User.find(self.showing_owner_id).emergency_contacts)
      messaging_service.deliver()
    end

    self.history.add(CurbCall::SafetyRadius::History::EVENTS[:completed])
  end

  def on_start
    p "on_start"
    #showing has begun
    CurbCall::Messages::Types::ShowingStartedMessage.enqueue(User.find(self.showing_owner_id).emergency_contacts, self) #if self.showing_owner
    p "E CONTACTS: #{User.find(self.showing_owner_id).emergency_contacts}"
    self.history.add(CurbCall::SafetyRadius::History::EVENTS.fetch(:start))

  end

  def on_self_start
    p "on_self_start"
    #showing has begun
    CurbCall::Messages::Types::ShowingStartedMessage.enqueue(User.find(self.showing_owner_id).emergency_contacts, self) #if self.showing_owner
    p "E CONTACTS: #{User.find(self.showing_owner_id).emergency_contacts}"
    self.history.add(CurbCall::SafetyRadius::History::EVENTS.fetch(:start))

  end

  def send_adhoc_stuff(showing_id)
    showing = Showing.find(showing_id)
    em_contacts = User.find(showing.showing_owner_id).emergency_contacts

    CurbCall::Messages::Types::ShowingStartedMessage.enqueue(em_contacts, showing) #if self.showing_owner
    p "E CONTACTS: #{User.find(showing.showing_owner_id).emergency_contacts}"
    showing.history.add(CurbCall::SafetyRadius::History::EVENTS.fetch(:start))
  end

  def send_adhoc_end(showing_id)
    #showing has ended safely
    #unless self.showing_owner_id.nil?
    showing = Showing.find(showing_id)
      message = CurbCall::Messages::Types::ShowingCompletedMessage.new(showing)
      messaging_service = CurbCall::Messages::MessagingService.new(message, User.find(showing.showing_owner_id).emergency_contacts)
      messaging_service.deliver()
    #end

    showing.history.add(CurbCall::SafetyRadius::History::EVENTS[:completed])
  end

  #try to contact agent and see if safe
  def sync_agent
    p "sync_agent"
    sync_message =  CurbCall::Config.get(CurbCall::SafetyRadius::Base::SETTINGS[:safety_sync_message])
    logger.info "sending push"
    PushWorker.perform_async(self.showing_owner_id, sync_message)
    CurbCall::Messaging::DashboardMessenger.publish(self.showing_owner.brokerage, {:data=>"#{self.showing_owner.name} exited safety-radius!"})
    self.history.add(CurbCall::SafetyRadius::History::EVENTS[:warning])

    #start sidekiq timer
    at = CurbCall::Config.p_get(CurbCall::SafetyRadius::Base::SETTINGS.fetch(:warning_timeout))
    SafetyTimeoutWorker.perform_at(at.minutes.from_now, self.id)


  end



  def on_panic
    p "on_panic"

    self.wait #SM

    CurbCall::Messaging::DashboardMessenger.publish(self.showing_owner.brokerage, { :data=>"#{self.showing_owner.name} has pressed the panic button!"}) if showing_owner_type=="User"
    self.history.add(CurbCall::SafetyRadius::History::EVENTS.fetch(:panic))

    panic_message =  CurbCall::Config.get(CurbCall::SafetyRadius::Base::SETTINGS.fetch(:push_panic_message))
    PushWorker.perform_async(self.showing_owner_id, panic_message, {cc_event: CurbCall::SafetyRadius::History::EVENTS[:panic]})

    CurbCall::Messages::Types::AgentPanicMessage.enqueue(self.showing_owner.emergency_contacts, self)
  end


  def on_exit_radius
    logger.info "on_exit_radius"
    CurbCall::Messaging::DashboardMessenger.publish(self.showing_owner.brokerage, { :data=>"#{self.showing_owner.name} has left the safety radius for long time!"})
    self.history.add(CurbCall::SafetyRadius::History::EVENTS.fetch(:danger))

    timeout_message =  CurbCall::Config.get(CurbCall::SafetyRadius::Base::SETTINGS[:timeout_message])
    PushWorker.perform_async(self.showing_owner_id, timeout_message, {cc_event: CurbCall::SafetyRadius::History::EVENTS[:danger]})

    CurbCall::Messages::Types::AgentDangerMessage.enqueue(self.showing_owner.emergency_contacts, self)

  end


  #callback for transition back to safe
  def on_syn_ack
    logger.info "on_syn_ack"
    self.history.add(CurbCall::SafetyRadius::History::EVENTS.fetch(:syn_ack))
  end



  def perform_safety_analysis
    radius = CurbCall::Config.get(CurbCall::SafetyRadius::Base::SETTINGS[:safety_radius])
    radius = JSON.load(radius) #parse float

    threshold = CurbCall::Config[CurbCall::SafetyRadius::Base::SETTINGS[:safety_threshold]]
    threshold = JSON.load(threshold)

    points = CurbCall::SafetyRadius::LatLng.collection_factory(self.sightings)
    safety_service = CurbCall::SafetyRadius::Base.factory( radius, points, threshold)
    result = safety_service.run_analysis()

    logger.info "safety analysis result=#{result.inspect}"

    if result.state == CurbCall::SafetyRadius::Base::STATES[:safe]
      #don't transition back to safe if the state is pending -- this means we panic'd
      self.syn_ack if(self.status != SHOWING_STATES.fetch(:pending))
    elsif result.state == CurbCall::SafetyRadius::Base::STATES[:warning]
      self.syn_agent #state machine
    elsif result.state == CurbCall::SafetyRadius::Base::STATES[:low_data]
      self.syn_ack
    else
      raise "How did I get here?"
    end

    result
  end

  def time_length
    timestamps = self.sightings.collect(&:created_at)
    start = timestamps.first || 0
    stop = timestamps.last || 0
    elapsed=(stop-start)/60
    elapsed.to_i.abs
  end
  
  def build_request_sightings
    sightings.build(lat: self.lat, lon: self.lng)
  end

  def self.collect_last_points(showings)
    sightings = []

    showings.each do |showing|
      unless showing.sightings.empty?
        sightings.push showing.sightings.last
      end
    end

    sightings
  end

  def dispatcher
    CurbCall::DispatchService::Dispatcher.new(self)
  end
  
  def agent_name_completed
    (!showing_owner.nil?) ? showing_owner.name : agent_name
  end
  
  def agent_name_completed_with_email
    (!showing_owner.nil?) ? showing_owner.email : agent_name
  end

 def owner_id
    agent_id
 end

end
