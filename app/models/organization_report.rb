class OrganizationReport
  
  attr_accessor :brokerage
  
  def showings_report(brokerage, start_date, end_date)
    @brokerage = brokerage
    @history_agents = HistoryAgent.length_of_time(@brokerage, start_date, end_date)
    @total_showings = Showing.for_brokerage(@brokerage).for_range(start_date, end_date).count
    @total_showings_expired = Showing.for_brokerage(@brokerage).for_range(start_date, end_date).expired.count
    @total_claimed_showings = Showing.for_brokerage(@brokerage).for_range(start_date, end_date).claimed.count
    @agents_notified = HistoryAgent.agent_notified(@brokerage, start_date, end_date)

    @connection_attempts = Showing.for_brokerage(@brokerage).for_range(start_date, end_date)
    @connection_successful = Showing.for_brokerage(@brokerage).for_range(start_date, end_date).completed
    @downloaded_app = User.for_brokerage(@brokerage).for_range(start_date,end_date).count
    @time_agent_declined = HistoryAgent.length_of_time(@brokerage, start_date, end_date)
    @avg_take_called = Showing.for_brokerage(@brokerage).for_range(start_date, end_date).called.take_called
    @avg_time_agentcustomer_response = User.for_brokerage(@brokerage).agent_hist(start_date, end_date)
    @time_notfollow_conn = User.for_brokerage(@brokerage).notfollow_con(start_date, end_date)
    @time_ignored_attempt = HistoryAgent.ignored_con(@brokerage, start_date, end_date)

    return {
      request: request(start_date, end_date),
      agents_length_time: @history_agents,
      total_showings: @total_showings,
      total_showings_expired: @total_showings_expired,
      total_claimed_showings: @total_claimed_showings,
      agents_notified: @agents_notified,    
      connection_attempts: @connection_attempts,
      connection_successful: @connection_successful,
      downloaded_app: @downloaded_app,
      time_agent_declined: @time_agent_declined,
      avg_take_called: @avg_take_called,
      avg_time_agentcustomer_response: @avg_time_agentcustomer_response,
      time_notfollow_conn: @time_notfollow_conn,
      time_ignored_attempt: @time_ignored_attempt
    }
  end
  
  private
  
  def request(start_date, end_date)
    {
      name: @brokerage.name,
      start_date: start_date,
      end_date: end_date
    }
  end
  
  def agents_length_time
    @history_agents.map do |history|
      {
        agent: history.user_email,
        length: "#{Time.parse(history.length).min} minutes"
      }
    end
  end
end

