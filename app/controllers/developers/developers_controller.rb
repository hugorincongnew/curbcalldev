class Developers::DevelopersController < ActionController::Base
    before_action :authenticate_developers_developer!
    layout 'developers'
end
