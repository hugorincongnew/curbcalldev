class Developers::ApiController < Developers::DevelopersController
  def index
  end
  
  def authentication
  end

  def agents
  end

  def showings
  end
end
