class Developers::AppsController < Developers::DevelopersController

  
  def index

    @apps = current_developers_developer.apps
  end
  
  def show
    @app = App.find(params[:id])
  end
  
  def new
    @app  = App.new
  end
  
  def create
    @app = App.new(create_app_params)


    respond_to do |format|
      if @app.save
        #possibly add mailer here for client key
        format.html { redirect_to developers_apps_path, notice: 'App was successfully created.' }
        format.json { render :show, status: :created, location: @app }
      else
        format.html { render :new }
        format.json { render json: @app.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def edit
    @app = App.find(params[:id])
  end

  private 
  
  def create_app_params
    p = params.require(:app).permit(:name, :description, :website, :organization)
    p["developer_id"] = current_developers_developer.id #set developer id for this resource
    p
  end
    
  def app_params
    params.require(:app).permit(:name, :description, :website, :organization)
  end
end
