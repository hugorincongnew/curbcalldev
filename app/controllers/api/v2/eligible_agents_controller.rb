class Api::V2::EligibleAgentsController < ApplicationController
  #before_filter :restrict_access
  # GET /eligible_agents
  # GET /eligible_agents.json
  
  def index
    @eligible = EligibleAgent.where(agent_user_id: params[:id]).last
    @eligible_agent = Array.new
    @eligible_agent << @eligible
    render json: @eligible_agent
  end

  # GET /eligible_agents/1
  # GET /eligible_agents/1.json
  def show
    #@eligible_agent = EligibleAgent.find(params[:id])
    @eligible_agent = EligibleAgent.where(agent_user_id: params[:id]).last
    if @eligible_agent.any?
      render json: @eligible_agent
    else
      render json: @eligible_agent, status: :none
    end
  end

  # POST /eligible_agents
  # POST /eligible_agents.json
  def create
    @eligible_agent = EligibleAgent.new(ei_params)

    if @eligible_agent.save
      @lead = Lead.find(@eligible_agent.lead_id)
      
      #push to pusher (on agent device)
      Pusher.trigger("agent_#{@eligible_agent.agent_user_id}", 'status', {:message => 'request'})
      
      #send push notification 
      # token = Agent.where(id: @eligible_agent.agent_user_id).last.dev_token
      # notification = Houston::Notification.new(device: token)
      # notification.alert = "You've Been Requested!"
      # notification.badge = 57
      # # notification.sound = "sosumi.aiff"
      # notification.sound = "Turn.aiff"
      # notification.content_available = true
      # notification.custom_data = {foo: "bar"}
      # APN.push(notification)
      # PPN.push(notification)


      render json: @eligible_agent, status: :created
    else
      render json: @eligible_agent.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /eligible_agents/1
  # PATCH/PUT /eligible_agents/1.json
  def update
    @eligible_agent = EligibleAgent.find(params[:id])

    if @eligible_agent.update(ei_params)
      head :no_content
    else
      render json: @eligible_agent.errors, status: :unprocessable_entity
    end
  end

  # DELETE /eligible_agents/1
  # DELETE /eligible_agents/1.json
  def destroy
    @eligible_agent = EligibleAgent.find(params[:id])
    @eligible_agent.destroy

    head :no_content
  end

  private
  def ei_params
    params.permit(:lead_id, :agent_user_id)
  end 

  def restrict_access
    authenticate_or_request_with_http_token do |token, options|
      ApiKey.exists?(access_token: token)
    end
  end
end
