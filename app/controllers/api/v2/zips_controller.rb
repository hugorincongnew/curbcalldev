class Api::V2::ZipsController < ApplicationController
      #before_filter :restrict_access


      # Actions omitted


  # GET /zips
  # GET /zips.json
  def index
    @zips = Zip.all

    render json: @zips
  end

  # GET /zips/1
  # GET /zips/1.json
  def show
    @zip = Zip.find(params[:id])

    render json: @zip
  end

  # POST /zips
  # POST /zips.json
  def create
    @zip = Zip.new(params[:zip])

    if @zip.save
      render json: @zip, status: :created
    else
      render json: @zip.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /zips/1
  # PATCH/PUT /zips/1.json
  def update
    @zip = Zip.find(params[:id])

    if @zip.update(params[:zip])
      head :no_content
    else
      render json: @zip.errors, status: :unprocessable_entity
    end
  end

  # DELETE /zips/1
  # DELETE /zips/1.json
  def destroy
    @zip = Zip.find(params[:id])
    @zip.destroy

    head :no_content
  end
  

  private
  def restrict_access
    authenticate_or_request_with_http_token do |token, options|
      ApiKey.exists?(access_token: token)
    end
  end
end
