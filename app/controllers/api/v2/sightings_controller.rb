class Api::V2::SightingsController < ApplicationController
  
  #post
  def heartbeat
    @sighting = Sighting.factory(sighting_params)
    showing = @sighting.showing
    
    #update real-time maps
    MessageBus.publish("showings/#{showing.id}", {sighting:@sighting.to_json}, {})

    if @sighting.persisted?
      render json: @sighting, status: :created
    else
      render json: @sighting.errors, status: :unprocessable_entity
    end
  end
  
  private
  def sighting_params 
    params.permit( :showing_id, :lat, :lon )
  end
  
  
end
