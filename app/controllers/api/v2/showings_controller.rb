class Api::V2::ShowingsController < ApplicationController
  #before_filter :restrict_access
  # GET /showings
  # GET /showings.json
  include CurbCall::Messages

  def index
    if params.has_key?(:buyer_id)
      @showings = Showing.where(buyer_id: "#{params[:buyer_id]}").last
    else
      @showings = Showing.all
    end
    render json: @showings
  end

  # GET /showings/1
  # GET /showings/1.json
  def show
    @showing = Showing.find(params[:id])

    render json: @showing
  end

  # POST /showings
  # POST /showings.json
  def create
    @showing = Showing.new(showing_params)

    if @showing.save
      @showing.send_adhoc_stuff(@showing.id)

      #update num of showings for this agent
      user = User.find(@showing.showing_owner_id)#type, should be user_id
      profile = Profile.where(user_id: user.id).last
      num_showings = profile.num_showings
      new_num_showings = num_showings + 1
      profile.update(num_showings: new_num_showings)

      unless @showing.buyer_id.nil?
        agent_id = Agent.where(user_id: @showing.showing_owner_id).last.id

        #push to pusher (on buyer device)
        Pusher.trigger("agent_#{agent_id}", 'showing_status', {:message => 'start_showing'})
      end


      @showing.start

      render json: @showing, status: :created
    else
      render json: @showing.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /showings/1
  # PATCH/PUT /showings/1.json
  def update
    @showing = Showing.find(params[:id])

    #trigger transittion
    #showing.fire_state_event(:shift_up)
    case(showing_params[:agent_state])
    when Showing::STATES[:warning] then @showing.syn_agent
    when Showing::STATES[:danger]
      if(@showing.agent_state == Showing::STATES.fetch(:safe))
        @showing.panic
      else
        @showing.ack_timeout
      end
    when Showing::STATES[:safe] then @showing.syn_ack
    end

    case(showing_params[:status])
    when Showing::SHOWING_STATES[:completed] then @showing.complete
    end


    if @showing.update(showing_params)
      if @showing.status == "completed" && !@showing.buyer_id.nil?
        #push to pusher (on buyer device)
        Agent.where(user_id: @showing.showing_owner).each do |agent|
          Pusher.trigger("agent_#{agent.id}", 'showing_status', {:message => 'end_showing'})
        end

        @showing.send_adhoc_end(@showing.id)
      end

       render json: @showing, status: :ok
    else
       render json: @showing.errors, status: :unprocessable_entity
    end
  end

  # DELETE /showings/1
  # DELETE /showings/1.json
  def destroy
    @showing = Showing.find(params[:id])
    @showing.destroy

    head :no_content
  end

  private
  def showing_params
    params.permit(:showing_owner_id, :showing_owner_type, :lead_id, :address, :buyer_name, :agent_name, :buyer_id, :status, :mls_num, :agent_state, :id, :called, :texted)
  end

  def restrict_access
    authenticate_or_request_with_http_token do |token, options|
      ApiKey.exists?(access_token: token)
    end
  end
end
