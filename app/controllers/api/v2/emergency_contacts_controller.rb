class Api::V2::EmergencyContactsController < ApplicationController

  before_filter :load_user, except: [:update, :destroy]
  before_filter :load_emergency_contact, only: [:update, :destroy]

  def index
  	@emergency_contacts = @user.emergency_contacts
  	render :json => @emergency_contacts, root: false
  end
  
  def create
  	@emergency_contact = @user.emergency_contacts.build(emergency_contact_params)

    respond_to do |format|
      if @emergency_contact.save
        format.json { render json: @emergency_contact, status: :accepted}
      else
        format.json { render json: @emergency_contact.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @emergency_contact.update(emergency_contact_params)
        format.json { render json: @emergency_contact, status: :created}
      else
        format.json { render json: @emergency_contact.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @emergency_contact.destroy

    head :no_content
  end


  private
  def emergency_contact_params
    params.permit(:name, :email, :phone, :contactable_id)
  end
  
  def load_user
    @user = User.find(params[:user_id])
    params.merge!(contactable_id: @user.id)
  end
  
  def load_emergency_contact
    @emergency_contact = EmergencyContact.find(params[:id])
  end

  def restrict_access
    authenticate_or_request_with_http_token do |token, options|
      ApiKey.exists?(access_token: token)
    end
  end
end
