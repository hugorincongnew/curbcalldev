class Api::V2::BrokeragesController < ApplicationController
  #before_filter :restrict_access
  # GET /brokerages
  # GET /brokerages.json
  def index
    @brokerages = Brokerage.all

    render json: @brokerages
  end

  # GET /brokerages/1
  # GET /brokerages/1.json
  def show
    @brokerage = Brokerage.find(params[:id])

    render json: @brokerage
  end

  # POST /brokerages
  # POST /brokerages.json
  def create
    @brokerage = Brokerage.new(params[:brokerage])

    if @brokerage.save
      render json: @brokerage, status: :created
    else
      render json: @brokerage.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /brokerages/1
  # PATCH/PUT /brokerages/1.json
  def update
    @brokerage = Brokerage.find(params[:id])

    if @brokerage.update(params[:brokerage])
      head :no_content
    else
      render json: @brokerage.errors, status: :unprocessable_entity
    end
  end

  # DELETE /brokerages/1
  # DELETE /brokerages/1.json
  def destroy
    @brokerage = Brokerage.find(params[:id])
    @brokerage.destroy

    head :no_content
  end

  private
  def restrict_access
    authenticate_or_request_with_http_token do |token, options|
      ApiKey.exists?(access_token: token)
    end
  end
end
