class Api::V2::GetListingsController < ApplicationController
  #before_filter :restrict_access
	include HTTParty

  def index
    if params.has_key?(:zip)
      zip = Zip.find_by(code: params[:zip])
      if zip.nil? || zip.brokerages.size == 0
        none = Brokerage.find_by(name: "NONE")
        @brokerage = Array.new
        @mls = Array.new
        @mls << Mls.find_by(name: "NONE")
      else
        @brokerage = zip.brokerages
        @mls = zip.mls
      end

      if @mls.size == 0
        # TODO: Send a message about no Listing source here
        p "No MLS"
        @addys = Array.new
        render json: @addys
      elsif @brokerage.size == 0
        p "No Brokerage"
        # TODO: Send a message about no coverage here yet
        @addys = Array.new
        render json: @addys
      else
        if @mls.first.retsly?      
          p "retsly"
          response = HTTParty.get("https://rets.io/api/v1/listing/#{@mls.name}.json?near=#{params[:lat].to_f},#{params[:lon].to_f}&radius=0.1&access_token=Cbaab9vEe0XKCBLfHl5TEeAtd9XCF6B4")
          string = response.body.to_str
          hash = JSON.parse string
          parsed = hash['bundle']
          @addys = Array.new
          parsed.each do |x| 
            listing = Array.new
            listing << x['address']
            listing << x['address']
            listing << x['price']
            if x['photos'].any?
              #listing << x['photos'][0]['location']
            end
            listing << "http://210.mlsimages.movoto.com/082/140016082_0.jpg"
            listing << x['publicRemarks']
            listing << x['bedrooms']
            listing << x['baths']
            #listing.h_baths << 1 #x['NumberOfHalfBaths']
            listing << x['subdivision']

            listing << x['status']
            listing << x['mlsNumber']
            listing << x['agent']['officeName']
            listing << @mls.name
            listing << @mls.legal
            listing << @mls.logo
            unless @brokerage.size == 0
            	listing << @brokerage.first.name
            	listing << @brokerage.first.id
            end
            listing << x['agent']['fullName']
            @addys << listing
          end
        	p @addys
        	render json: @addys
        else
        	p "Not Retsly"
          @addys = Array.new
        	@location = []
  		    @location << params[:lat].to_f
  		    @location << params[:lon].to_f
  		    @nearby_listings = Listing.within(0.1, :origin => @location)

          @nearby_listings.each do |x|
            listing = Array.new
            listing << x['address']
            listing << x['address']
            listing << x['price']
            listing << x['photo1']
            listing << x['remarks']
            listing << x['beds']
            listing << x['baths']
            listing << x['subdivision']
            listing << x['status']
            listing << x['mls_num']
            listing << x['list_office']
            listing << x.mls.name
            listing << x.mls.legal
            listing << x.mls.logo
            # unless @brokerage.size == 0
              listing << @brokerage.first.name
              listing << @brokerage.first.id
            # end
            listing << x['list_agent']
            listing << x['lat']
            listing << x['lon']
            @addys << listing
            
          
          end
          render json: @addys
          #render json: @nearby_listings
        end # if mls.retsly? 
      end # if mls.nil?
    end # If has_key(:zip)
  end #index

  private
  def restrict_access
    authenticate_or_request_with_http_token do |token, options|
      ApiKey.exists?(access_token: token)
    end
  end

  def lead_params
    params.permit(:zip, :lat, :lon)
  end  
end