class Api::V2::MlsController < ApplicationController
	#before_filter :restrict_access
	
  # GET /mls
  # GET /mls.json
  def index
    @mls = Mls.all

    render json: @mls
  end

  # GET /mls/1
  # GET /mls/1.json
  def show
    @mls = Mls.find(params[:id])

    render json: @mls
  end

  # POST /mls
  # POST /mls.json
  def create
    @mls = Mls.new(mls_params)

    if @mls.save
      render json: @mls, status: :created, location: @mls
    else
      render json: @mls.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /mls/1
  # PATCH/PUT /mls/1.json
  def update
    @mls = Mls.find(params[:id])

    if @mls.update(mls_params)
      head :no_content
    else
      render json: @mls.errors, status: :unprocessable_entity
    end
  end

  # DELETE /mls/1
  # DELETE /mls/1.json
  def destroy
    @mls = Mls.find(params[:id])
    @mls.destroy

    head :no_content
  end

  private
  def restrict_access
    authenticate_or_request_with_http_token do |token, options|
      ApiKey.exists?(access_token: token)
    end
  end

  def mls_params
    params.permit(:name, :username, :password, :login_link, :retsly, :legal, :logo)
  end
end
