class Api::V2::LeadsController < ApplicationController
  #before_filter :restrict_access
  # GET /leads
  # GET /leads.json
  def index
    @leads = Lead.where(:status => nil)

    render json: @leads
  end

  # GET /leads/1
  # GET /leads/1.json
  def show
    @lead = Lead.find(params[:id])

    render json: @lead
  end

  # POST /leads
  # POST /leads.json
  def create
    @lead = Lead.new(lead_params)

    if @lead.save
      render json: @lead, status: :created
    else
      render json: @lead.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /leads/1
  # PATCH/PUT /leads/1.json
  def update
    @lead = Lead.find(params[:id])

    if @lead.update(lead_params)
      if @lead.status == "claimed"
        @eis = EligibleAgent.where(lead_id: @lead.id)
        if @eis.any?
          @eis.each do |x|
            x.destroy
          end
        end
      end
      render :nothing => true, :status => 200
    else
      render json: @lead.errors, status: :unprocessable_entity
    end
  end

  # DELETE /leads/1
  # DELETE /leads/1.json
  def destroy
    @lead = Lead.find(params[:id])
    @eis = EligibleAgent.where(lead_id: @lead.id)
    if @eis.any?
      @eis.each do |x|
        x.destroy
      end
    end
    @lead.destroy

    head :no_content
  end

  private
  def lead_params
    params.permit(:name, :lat, :lon, :address, :hood, :status, :agent_phone, :agent_company, :agent_email, :agent_image, :bio, :buyer_email, :buyer_phone, :buyer_price_range, :buyer_num_showings, :buyer_id, :target_agent, :agent_name, :agent_id)
  end  

  def restrict_access
    authenticate_or_request_with_http_token do |token, options|
      ApiKey.exists?(access_token: token)
    end
  end
end
