class Api::V1::GetListingsController < ApplicationController
  #before_filter :restrict_access
	include HTTParty

  def index
    if params.has_key?(:zip)
      zip = Zip.where(code: params[:zip]).last
      if zip.brokerage.nil?
        @brokerage = Brokerage.where(name: "NONE").last
        @mls = Mls.where(name: "NONE").last
      else
        @brokerage = zip.brokerage
        @mls = zip.mls
      end

      if @mls.retsly?
        p "retsly"
        response = HTTParty.get("https://rets.io/api/v1/listing/#{@mls.name}.json?near=#{params[:lat].to_f},#{params[:lon].to_f}&radius=0.1&access_token=Cbaab9vEe0XKCBLfHl5TEeAtd9XCF6B4")
        string = response.body.to_str
        hash = JSON.parse string
        parsed = hash['bundle']
        @addys = Array.new
        parsed.each do |x| 
          listing = Array.new
          listing << x['address']
          listing << x['address']
          listing << x['price']
          if x['photos'].any?
            #listing << x['photos'][0]['location']
          end
          listing << "http://210.mlsimages.movoto.com/082/140016082_0.jpg"
          listing << x['publicRemarks']
          listing << x['bedrooms']
          listing << x['baths']
          #listing.h_baths << 1 #x['NumberOfHalfBaths']
          listing << x['subdivision']

          listing << x['status']
          listing << x['mlsNumber']
          listing << x['agent']['officeName']
          listing << @mls.name
          listing << @mls.legal
          listing << @mls.logo
          unless @brokerage.nil?
          	listing << @brokerage.name
          	listing << @brokerage.id
          end
          listing << x['agent']['fullName']
          @addys << listing
        end
      	p @addys
      	render json: @addys
      else
      	p "Not Retsly"
        @addys = Array.new
      	@location = []
		    @location << params[:lat].to_f
		    @location << params[:lon].to_f
		    @nearby_listings = Listing.within(10, :origin => @location)

        @nearby_listings.each do |x|
          listing = Array.new
          listing << x['address']
          listing << x['address']
          listing << x['price']
          listing << x['photo1']
          listing << x['remarks']
          listing << x['beds']
          listing << x['baths']
          listing << x['subdivision']
          listing << x['status']
          listing << x['mls_num']
          listing << x['list_office']
          listing << @mls.name
          listing << @mls.legal
          listing << @mls.logo
          unless @brokerage.nil?
            listing << @brokerage.name
            listing << @brokerage.id
          end
          listing << x['list_agent']
          listing << x['lat']
          listing << x['lon']
          @addys << listing
          
        
        end
        render json: @addys
        #render json: @nearby_listings
      end # if mls.retsly? 
    end # If has_key(:zip)
  end #index

  private
  def restrict_access
    authenticate_or_request_with_http_token do |token, options|
      ApiKey.exists?(access_token: token)
    end
  end

  def lead_params
    params.permit(:zip, :lat, :lon)
  end  
end