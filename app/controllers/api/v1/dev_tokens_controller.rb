class Api::V1::DevTokensController < ApplicationController
  #before_filter :restrict_access
  # GET /eligible_agents
  # GET /eligible_agents.json
  
  def index
    @dev_tokens = DevToken.all

    render json: @dev_tokens
  end

  # GET /eligible_agents/1
  # GET /eligible_agents/1.json
  def show
    #@eligible_agent = DevToken.find(params[:id])
    @dev_token = DevToken.where(agent_user_id: params[:id])
    if @dev_token.any?
      render json: @dev_token
    else
      render json: @dev_token, status: :none
    end
  end

  # POST /dev_tokens
  # POST /dev_tokens.json
  def create
    @dev_token = DevToken.new(token_params)

    if @dev_token.save
      render json: @dev_token, status: :created
    else
      render json: @dev_token.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /dev_tokens/1
  # PATCH/PUT /dev_tokens/1.json
  def update
    @dev_token = DevToken.find(params[:id])

    if @dev_token.update(token_params)
      head :no_content
    else
      render json: @dev_token.errors, status: :unprocessable_entity
    end
  end

  # DELETE /dev_tokens/1
  # DELETE /dev_tokens/1.json
  def destroy
    @dev_token = DevToken.find(params[:id])
    @dev_token.destroy

    head :no_content
  end

  private
  def token_params
    params.permit(:dev_token, :user_id)
  end 

  def restrict_access
    authenticate_or_request_with_http_token do |token, options|
      ApiKey.exists?(access_token: token)
    end
  end
end