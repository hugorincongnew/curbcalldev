class Api::V1::ListingsController < ApplicationController
  #before_filter :restrict_access
  include HTTParty
  # GET /listings
  # GET /listings.json
  def index
    # if params.has_key?(:zip)
    #   zip = Zip.find_by(code: params[:zip])
    #   @brokerage = zip.brokerage
    #   @mls = zip.mls

    #   if @mls.retsly?
    #     response = HTTParty.get("https://rets.io/api/v1/listing/#{@mls.name}.json?near=#{params[:lat].to_f},#{params[:lon].to_f}&radius=0.1&access_token=Cbaab9vEe0XKCBLfHl5TEeAtd9XCF6B4")
    #     string = response.body.to_str
    #     hash = JSON.parse string
    #     parsed = hash['bundle']
    #     @addys = Array.new
    #     parsed.each do |x| 
    #       listing = Array.new
    #       listing << x['address']
    #       listing << x['address']
    #       listing << x['price']
    #       if x['photos'].any?
    #         listing << x['photos'][0]['location']
    #       end
    #       listing << x['publicRemarks']
    #       listing << x['bedrooms']
    #       listing << x['baths']
    #       #listing.h_baths << 1 #x['NumberOfHalfBaths']
    #       listing << x['subdivision']
    #       @addys << listing
    #     end
    #   p @addys
    # end



    @listings = Listing.all

    render json: @listings
  end

  # GET /listings/1
  # GET /listings/1.json
  def show
    @listing = Listing.find(params[:id])

    render json: @listing
  end

  # POST /listings
  # POST /listings.json
  def create
    @listing = Listing.new(params[:listing])

    if @listing.save
      render json: @listing, status: :created, location: @listing
    else
      render json: @listing.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /listings/1
  # PATCH/PUT /listings/1.json
  def update
    @listing = Listing.find(params[:id])

    if @listing.update(params[:listing])
      head :no_content
    else
      render json: @listing.errors, status: :unprocessable_entity
    end
  end

  # DELETE /listings/1
  # DELETE /listings/1.json
  def destroy
    @listing = Listing.find(params[:id])
    @listing.destroy

    head :no_content
  end

  private
  def restrict_access
    authenticate_or_request_with_http_token do |token, options|
      ApiKey.exists?(access_token: token)
    end
  end
end
