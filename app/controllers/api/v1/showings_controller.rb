class Api::V1::ShowingsController < ApplicationController
  #before_filter :restrict_access
  # GET /showings
  # GET /showings.json
  def index
    if params.has_key?(:buyer_id)
      @showings = Showing.where(buyer_id: "#{params[:buyer_id]}").last
    else
      @showings = Showing.all
    end
    render json: @showings
  end

  # GET /showings/1
  # GET /showings/1.json
  def show
    @showing = Showing.find(params[:id])

    render json: @showing
  end

  # POST /showings
  # POST /showings.json
  def create
    @showing = Showing.new(showing_params)

    if @showing.save
      #update num of showings for this agent
      user = User.find(@showing.agent_id)
      profile = Profile.where(user_id: user.id).last
      num_showings = profile.num_showings
      new_num_showings = num_showings + 1
      profile.update(num_showings: new_num_showings)

      #send push notification
      token = DevToken.where(user_id: @showing.buyer_id).last.dev_token
      notification = Houston::Notification.new(device: token)
      notification.alert = "Your Showing is Starting!"
      notification.badge = 57
      notification.sound = "Turn.aiff"
      notification.content_available = true
      notification.custom_data = {foo: "bar"}
      APN.push(notification)
      PPN.push(notification)

      render json: @showing, status: :created
    else
      render json: @showing.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /showings/1
  # PATCH/PUT /showings/1.json
  def update
    @showing = Showing.find(params[:id])

    if @showing.update(showing_params)
      head :no_content
      if @showing.status == "completed"
        token = DevToken.where(user_id: @showing.buyer_id).last.dev_token
        notification = Houston::Notification.new(device: token)
        notification.alert = "Your Showing is Ending!"
        notification.badge = 0
        notification.sound = "sosumi.aiff"
        notification.content_available = true
        notification.custom_data = {foo: "bar"}
        APN.push(notification)
      end
    else
      render json: @showing.errors, status: :unprocessable_entity
    end
  end

  # DELETE /showings/1
  # DELETE /showings/1.json
  def destroy
    @showing = Showing.find(params[:id])
    @showing.destroy

    head :no_content
  end

  private
  def showing_params
    params.permit(:agent_id, :lead_id, :address, :buyer_name, :agent_name, :buyer_id, :status, :mls_num)
  end

  def restrict_access
    authenticate_or_request_with_http_token do |token, options|
      ApiKey.exists?(access_token: token)
    end
  end
end
