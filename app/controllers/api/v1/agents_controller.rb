class Api::V1::AgentsController < ApplicationController
  #before_filter :authenticate_user_from_token!
  #before_filter :authenticate_user!
  #before_filter :restrict_access
  # GET /agents
  # GET /agents.json
  def index
    @agents = Agent.all
    render json: @agents
  end

  # GET /agents/1
  # GET /agents/1.json
  def show
    @agent = Agent.find(params[:id])
    render json: @agent, status: :present
  end

  # POST /agents
  # POST /agents.json
  def create
    @agent = Agent.new(agent_params)
    @user = User.find(@agent.user_id)
    if @agent.save

      profile = Profile.where(user_id: @agent.user_id).last
      @agent.update_attributes(:bio => profile.bio, :showing_rating => profile.showing_rating, :showing_feedback_count => profile.showing_feedback_count, :nice_rating => profile.nice_rating, :nice_rating_count => profile.nice_rating_count, :phone => profile.phone, :email => profile.email, :company => profile.company, brokerage_id: @user.brokerage_id, num_showings: profile.num_showings)
      render json: @agent, status: :created#, location: @agent.lat, agent_id: @agent.id
    else
      render json: @agent.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /agents/1
  # PATCH/PUT /agents/1.json
  def update
    @agent = Agent.find(params[:id])

    if @agent.update(agent_params)
      head :no_content
    else
      render json: @agent.errors, status: :unprocessable_entity
    end
  end

  # DELETE /agents/1
  # DELETE /agents/1.json
  def destroy
    @agent = Agent.find(params[:id])
    @agent.destroy

    head :no_content
  end

  private

  def agent_params
    params.permit( :name, :lat, :lon, :phone, :company, :user_id, :email, :image, :bio, :showing_rating, :showing_feedback_count, :nice_rating, :nice_rating_count, :brokerage_id, :dev_token, :num_showings, :status)
  end

  def restrict_access
    authenticate_or_request_with_http_token do |token, options|
      ApiKey.exists?(access_token: token)
    end
  end
end
