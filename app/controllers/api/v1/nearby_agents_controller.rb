class Api::V1::NearbyAgentsController < ApplicationController
	#before_filter :restrict_access
	def index
		if params.has_key?(:lat) && params.has_key?(:lon) && params.has_key?(:zip)
			zip = Zip.where(code: params[:zip]).last
			
			if zip.brokerage.nil?
		        brokerage = Brokerage.where(name: "NONE").last
		        #@mls = Mls.where(name: "NONE").last
		    else
		        brokerage = Brokerage.find(zip.brokerage_id)
		        #@mls = zip.mls
      		end
			@location = []
			@location << params[:lat].to_f
			@location << params[:lon].to_f


			agents = Agent.within(2, :origin => @location) # all agents within 2 miles
			if agents.any?
				@nearby_agents = Array.new
				agents.each do |x|
					# @nearby_agents = Array.new
					if x.brokerage_id != nil && x.brokerage_id == brokerage.id && x.status != "showing"
						@nearby_agents << x
					end
				end
			else 
				@nearby_agents = Array.new
			end
			if @nearby_agents.count >= 1
				p "two miles"
				render json: @nearby_agents
			elsif @nearby_agents.count < 1
				agents = Agent.within(5, :origin => @location)
				if agents.any?
					@nearby_agents = Array.new
					agents.each do |x|
						# @nearby_agents = Array.new
						if x.brokerage_id != nil && x.brokerage_id == brokerage.id && x.status != "showing"
							@nearby_agents << x
						end
					end
				else
					@nearby_agents = Array.new
				end
				if @nearby_agents.count < 1
					agents = Agent.within(10, :origin => @location)
					if agents.any?
						@nearby_agents = Array.new
						agents.each do |x|
							# @nearby_agents = Array.new
							if x.brokerage_id != nil && x.brokerage_id == brokerage.id && x.status != "showing"
								@nearby_agents << x
							end
						end
					else
						@nearby_agents = Array.new
					end
					p "10 miles"
					
					if brokerage.name == "NONE"
						@nearby_agents = Array.new
						agent = Agent.new
						agent.update(name: "Out of Area", lat: 39.8009948730469, lon: -89.6499938964844)
						@nearby_agents << agent
						render json: @nearby_agents
					else
						render json: @nearby_agents
					end
				else
					p "5 miles"
					render json: @nearby_agents
				end
			else
				p "two miles"
				render json: @nearby_agents
			end
		else
			render json: @nearby_agents.errors, status: :unprocessable_entity
		end

	end

  private
  def restrict_access
    authenticate_or_request_with_http_token do |token, options|
      ApiKey.exists?(access_token: token)
    end
  end

  def lead_params
    params.permit(:zip, :lat, :lon)
  end
end
