class Api::V3::AgentsController < Api::V3::ServiceController
  before_filter :authenticate


  def find
    lat = params[:lat]
    lng = params[:lng]
    radius = params[:radius]

	@showing = Showing.new({brokerage: @brokerage})
    disptacher = CurbCall::DispatchService::Dispatcher.new(@showing)
    @agents = disptacher.find_agents({lat: lat, lng: lng, radius: radius})


    if disptacher.errors.any?
      render json: disptacher.errors, status: 409
    else
      render json: @agents
    end
  end

  def index
    @agent = Agent.where(user_id:params[:user_id]).last
    render json: @agent
  end

  private
  
  def authenticate_token
  	authenticate_with_http_token do |token, options|
  		@brokerage = Brokerage.find_by(access_token: token)
  	end
  end

end
