class Api::V3::ServiceController < ActionController::Base
  # protect_from_forgery with: :null_session

  respond_to :json, :xml

  before_filter :check_rate_limit if @current_app

  private
  #test
  # curl -IH "Authorization: Token token=16d7d6089b8fe0c5e19bfe10bb156832" http://localhost:5000/v1/listings
  def authenticate
    authenticate_token || render_unauthorized

  end

  def authenticate_token
    authenticate_with_http_token do |token, options|
      @current_app = App.find_by(access_token: token)
      if @current_app.nil?
        @current_app = App.find_by(sandbox_access_token: token)
        #@sandbox = true if @current_app
        @sandbox = false
      end

      @current_app
    end
  end

  def render_unauthorized
    self.headers['WWW-Authenticate'] = 'Token realm="Application"'
    error = {errors:  ["Token is invalid."] }
    render json: error, status: 401 and return
  end

  def current_app
    @current_app
  end

  def check_rate_limit
    if @current_app.request_count > 100
      error = { :error => "Rate limit exceeded." }
      respond_with(error, :status => 403)
    else
      #move this to redis
      @current_app.increment!(:request_count)
    end
  end
end
