class Api::V3::OrganizationController < Api::V3::ServiceController
  before_filter :authenticate
  
  def showing_report
    start_date = params[:start_date]
    end_date = params[:end_date]
    
    if start_date and end_date
      report = OrganizationReport.new
      result = report.showings_report(@brokerage, start_date, end_date)
      render json: result
    else
      failure(405, "bad request", "The start_date and end_date params are required")
    end
    
  end
  
  private
  
  def authenticate_token
  	authenticate_with_http_token do |token, options|
  		@brokerage = Brokerage.find_by(access_token: token)
  	end
  end
  
  def failure(code, info, message)
    render :status => code,
           :json => { :success => false,
                      :info => info,
                      :data => {message: message} }
  end
end