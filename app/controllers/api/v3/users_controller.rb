class Api::V3::UsersController < Api::V3::ServiceController

  def update
    @user = User.find(params[:id])

    if @user.update(profile_params)
      render json: @user
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  private
  
  def profile_params
    params.permit(:player_id)
  end 

end
