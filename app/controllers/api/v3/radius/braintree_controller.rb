class Api::V3::Radius::BraintreeController <  Api::V3::ServiceController
  before_filter :authenticate
  respond_to :json
  
  #curl -v -H "Accept: application/json" -H "Content-Type: application/json" -d '{"api_v3_radius_radius_user":{"email":"eggie12@gmail.com", "password":"thereandbackagain"}}' http://localhost:5000/api/v3/radius/radius_users/sign_in.json
  
  PLAN_ID = "25yw" #extract this to env var or app global setting
  
  def client_token
    @client_token = Braintree::ClientToken.generate()
    resp = {token: @client_token}
    render json: resp
  end
  
  def subscriptions
    nonce = params[:payment_method_nonce]
    radius_user = RadiusUser.find(params[:radius_user_id])
    
    result = Braintree::Customer.create(
        :first_name => params[:first_name],
        :last_name => params[:last_name],
        :payment_method_nonce => nonce
      )
    if result.success?
      customer_id = result.customer.id
      radius_user.update_column(:bt_customer_id, customer_id)
    else
      p "Could not create customer"
    end
    
    result = Braintree::Subscription.create(
          :payment_method_token => result.customer.credit_cards[0].token,
          :plan_id => PLAN_ID
        )
        
    if result.success?
      p "Subscription Status #{result.subscription.status}"
    else
      p "Error: #{result.message}"
    end
    
    render json: {status: "OK"}, status: :created
    
  end
end