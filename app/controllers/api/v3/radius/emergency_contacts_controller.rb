class Api::V3::Radius::EmergencyContactsController < Api::V3::ServiceController

  before_filter :load_user
  
  
  def index
    @emergency_contacts = @radius_user.emergency_contacts
    render json: @emergency_contacts
  end
  
  def create
    @emergency_contact = @radius_user.emergency_contacts.build(emergency_contact_params)

    respond_to do |format|
      if @emergency_contact.save
        format.json { render json: @emergency_contact, status: :created}
      else
        format.json { render json: @emergency_contact.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @emergency_contacts = EmergencyContact.find(params[:id])
    @emergency_contacts.destroy

    head :no_content
  end

  private 
  
  def load_user
    @radius_user = RadiusUser.find(params[:user_id])
  end
    
  def emergency_contact_params
    params.require(:emergency_contact).permit(:name, :phone, :email, :id)
  end
end
