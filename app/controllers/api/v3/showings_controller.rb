class Api::V3::ShowingsController < Api::V3::ServiceController
  before_filter :authenticate, only: [:create]
  
  def index
  	agent_id = params[:agent_id]
  	@agent = Agent.find_by id: agent_id
  	if !@agent.nil?
  		@brokerage = @agent.brokerage
  		
  		@showings = @agent.user.showings.waiting
  		
  		
		if @showings.any? 
   			#claim this showing
   			return claim_showing(@showings.first, @agent, "User")
		else
			radius = params[:radius] || 10
        	lat = @agent.lat
        	lng = @agent.lon
        	puts "LAT: #{lat} - LONG: #{lng}"
        	origin = Geokit::LatLng.new(lat, lng)
			
			@showings = Showing.within(radius, origin: origin).waiting
			if @showings.any?
				return claim_showing(@showings.first, @agent, "User")
   			end
   		end
   		
   		render json: {errors: ["Could not find any showings at that GPS coord."]}, status: :unprocessable_entity
  	else
  		failure(404, "bad request", "The agent not found")
  	end
  end

  def create
  	params.merge!(brokerage_id: @brokerage.id)
    agent_id = params[:showing_owner_id]
    lat = params[:lat]
    lng = params[:lng]
    radius = params[:radius]
    if params.has_key?(:agent_email)
      agent_email = params[:agent_email]
    else
      agent_email = nil
    end

    @showing = Showing.new(showing_params)
    
    if !@showing.save
    	return failure(405, "bad request", @showing.errors)
    else 

    # get available agents in proximity
    # of will get specified agent if available
    # skip if this is an adhoc showing (radius app)
    if params[:status] == "started"
      @showing.status="started"
      @showing.showing_owner_id = params[:showing_owner_id]
      @showing.showing_owner_type = params[:showing_owner_type]
    else
      #Tenp disabled. Fix this (TO DO)
      disptacher = CurbCall::DispatchService::Dispatcher.new(@showing)
      agents = disptacher.find_agents({lat: lat, lng: lng, radius: radius, agent_id: agent_id, sandbox: false, agent_email: agent_email})

      if(agents.any?)
        disptacher.notify_agents(@showing, agents)

        ## TEMP FIX:
=begin
        agents.each do |agent|
          agent.new_request
          data = { alert: "You've Been Requested!", sound: "sosumi.aiff", message: 'request', showing_id: @showing.id}
          #data.merge!(opts)
          #user_id = Agent.find(params[:agent_id]).user_id
          user_id = Agent.find(agent.id).user_id
          push = Parse::Push.new(data)
          query = Parse::Query.new(Parse::Protocol::CLASS_INSTALLATION).eq('user_id', user_id)
          push.where = query.where
          push.save
        end
=end

      else # no agents found
        @showing.errors.add("errors", "Could not find any agents at that GPS coord.")
      end


      #if we are in sandbox, auto attach an agent
      # if @sandbox
      #   temp_agent = agents.first
      #   @showing.showing_owner_id = temp_agent.user_id
      #   @showing.agent_name = temp_agent.name
      #   @showing.attach_agent
      # end
    end


    if @showing.errors.empty? && @showing.save
      render json: @showing, status: :created
    else
      render json: @showing.errors, status: :unprocessable_entity
    end
    
    end
  end

  def show
    @showing = Showing.find(params[:id])

    render json: @showing

  end

  # Endpoint for agents to claim this showing
  def claim
    id = params[:id]
    agent_id = params[:agent_id]

    @agent = Agent.find(agent_id)
    @showing = Showing.find(id)
    claim_showing(@showing, @agent, params[:showing_owner_type])
  end

  def start
    id = params[:id]

    @showing = Showing.find(id)
    @showing.start

    if @showing.errors.empty? && @showing.save
      render json: @showing
    else
      render json: @showing.errors, status: :unprocessable_entity
    end

  end

  def self_start
    @showing = Showing.create(
    showing_owner_id:params[:showing_owner_id],
    agent_name:params[:agent_name],
    status:"started",
    showing_owner_type:"User")

    @showing.start

    if @showing.errors.empty? && @showing.save
      render json: @showing
    else
      render json: @showing.errors, status: :unprocessable_entity
    end
  end

  def end
    id = params[:id]

    @showing = Showing.find(id)
    @showing.complete

    if @showing.errors.empty? && @showing.save
      render json: @showing
    else
      render json: @showing.errors, status: :unprocessable_entity
    end

  end

  # api/v3/showings/:id/
  def update
    id = params[:id]
    @showing = Showing.find(id)
    @showing.transition!(params[:showing][:agent_state])
   

    if @showing.save && @showing.errors.empty?
      render json: @showing
    else
      render json: @showing.errors, status: :unprocessable_entity
    end
  end



  private
  
  def authenticate_token
  	authenticate_with_http_token do |token, options|
  		@brokerage = Brokerage.find_by(access_token: token)
  	end
  end
  
  def showing_params
    params.permit(:lat, :lng, :brokerage_id, :buyer_name, :buyer_phone, :buyer_email, :hood, :address, :address_two, :photo_url, :list_price, :notes, :status, :agent_state, :called, :texted )
  end
  
  def failure(code, info, message)
    render :status => code,
           :json => { :success => false,
                      :info => info,
                      :data => {message: message} }
  end
  
  def claim_showing(showing, agent, showing_owner_type="User")
  	showing.showing_owner_id = agent.user_id
    showing.showing_owner_type = showing_owner_type
    showing.agent_name  = agent.name
    showing.attach_agent

    if showing.save && showing.errors.empty?
      render json: showing
    else
      render json: showing.errors, status: :unprocessable_entity
    end
  end

end
