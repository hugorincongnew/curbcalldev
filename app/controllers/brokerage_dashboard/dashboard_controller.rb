class BrokerageDashboard::DashboardController < ApplicationController
	before_filter :authenticate_user!
  before_filter :set_brokerage
  layout 'dashboard'
  
  def index
    #get brokerage showings for today
    today=Time.zone.today
    start_date=today.beginning_of_day
    week_start_date=today.beginning_of_week
    month_start_date=today.beginning_of_month
    end_date=today.end_of_day
    
    @showings=@brokerage.showings_request.for_range(start_date, end_date).waiting#.includes(:sightings)
    @showings_this_week=@brokerage.showings.for_range(week_start_date, end_date)
    @showings_this_month=@brokerage.showings.for_range(month_start_date, end_date)
    
    @active_showings = @brokerage.showings.active.reject{|showing|showing.sightings.empty?}
  end
  
  private
  def set_brokerage
    if current_user.brokerage
      @brokerage = current_user.brokerage
    else
      redirect_to root_path, notice: 'This user does not belong to a brokerage.'
    end
  end

  # def index
  #   # if current_user.office_admin == "true"
  #     @users = User.all
  #     @agents = User.where(brokerage_id: current_user.brokerage_id)
  #     @live_agents = Agent.where(brokerage_id: current_user.brokerage_id)
  #     @brokerage = Brokerage.find(current_user.brokerage_id)
  #     @showings = Showing.all
  #
  #     #@buyers = User.where(id: )
  #
  #     # @showing_now = Array.new
  #     # Showing.where(status: nil).each do |showing|
  #     #   if User.find(showing.agent_id).exists?
  #     #     agent = User.find(showing.agent_id)
  #     #   else
  #     #     agent = 100000000000000000
  #     #   end
  #
  #     #   if agent.brokerage_id == @brokerage.id
  #     #     @showing_now << agent
  #     #   end
  #     # end
  #
  #   # else
  #   #   redirect_to login_path
  #   # end
  #     # @json = @live_agents.to_gmaps4rails
  #    #      @markers = @live_agents.to_gmaps4rails
  # end
  #
  # def show
  #   @agent = User.find(params[:id])
  #   @available = Agent.where(user_id: @agent.id).any?
  # end
end