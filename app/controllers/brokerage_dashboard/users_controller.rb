class BrokerageDashboard::UsersController < BrokerageDashboard::DashboardController
  # before_action :set_user, only: [:show, :edit, :update, :destroy]
  
  def index
    @brokerage = current_user.brokerage
    @agents = @brokerage.users.page(params[:page])
  
  end
  
  def show

    user_id = params[:id]
    @user = User.find(user_id)
    @brokerage = current_user.brokerage
    @showings = Showing.where(showing_owner_id: user_id)
  end
  
  # GET /users/new
  def new
    @user = User.new
    @form_brokerage=@brokerage#hack
  end

  # GET /users/1/edit
  def edit
    @user = User.find(params[:id])
    @form_brokerage=nil#hack
  end
  
  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params)
    @user.brokerage=@brokerage

    respond_to do |format|
      if @user.save
        @user.active_for_authentication?
        format.html { redirect_to brokerage_dashboard_user_path(@user), notice: 'User was successfully created.' }
        format.json { render :show, status: :created, location: @user }
      else
        @form_brokerage=@brokerage#hack
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update

    @user = User.find(params[:id])
    
    respond_to do |format|
      if @user.update(update_user_params)
        format.html { redirect_to brokerage_dashboard_user_path(@user), notice: 'User was successfully updated.' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user = User.find(params[:id])
    @user.destroy
    respond_to do |format|
      format.html { redirect_to brokerage_dashboard_brokerage_users_path(@brokerage), notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
  
  def showing_show
    brokerage_id = params[:id]
    user_id = params[:user_id]
    showing_id = params[:showing_id]
    
    @brokerage = Brokerage.find(brokerage_id)
    @user = User.find(user_id)
    @showing = Showing.find(showing_id)
    @sightings = @showing.sightings
    
  end
  
  
  def reset_password
    @user = User.find(params[:id])
    token = @user.send_reset_password_instructions
    
    respond_to do |format|
      format.html {redirect_to brokerage_dashboard_user_path(@user), notice: "Welcome email sent to user."}
    end
  end
  
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:name, :email, :password, emergency_contacts_attributes:[:name, :phone, :email, :id])
    end
    def update_user_params
      params.require(:user).permit(:name, :email, emergency_contacts_attributes:[:name, :phone, :email, :id, :_destroy])
    end
end
