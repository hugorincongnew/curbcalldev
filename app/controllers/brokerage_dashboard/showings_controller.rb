class BrokerageDashboard::ShowingsController < BrokerageDashboard::DashboardController
  before_action :set_showing, only: [:show, :edit, :update, :destroy]

  def real_time
    @active_showings = @brokerage.showings.active.reject{|showing|showing.sightings.empty?}
  end
  
  # GET /showings
  # GET /showings.json
  def index
    @search = params[:search]
    @showings = @brokerage.showings_request.search_param(@search, params[:status_state_search]).page(params[:page]).order("created_at DESC")
  end

  # GET /showings/1
  # GET /showings/1.json
  def show
    @user= current_user
    @sightings = @showing.sightings
    # @history = CurbCall::SafetyRadius::History.new(@showing.id, nil).all
  end

  # GET /showings/new
  def new
    @showing = Showing.new
  end

  # GET /showings/1/edit
  def edit
  end

  # POST /showings
  # POST /showings.json
  def create
    @showing = Showing.new(showing_params)

    respond_to do |format|
      if @showing.save
        format.html { redirect_to @showing, notice: 'Showing was successfully created.' }
        format.json { render :show, status: :created, location: @showing }
      else
        format.html { render :new }
        format.json { render json: @showing.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /showings/1
  # PATCH/PUT /showings/1.json
  def update
    respond_to do |format|
      if @showing.update(showing_params)
        format.html { redirect_to @showing, notice: 'Showing was successfully updated.' }
        format.json { render :show, status: :ok, location: @showing }
      else
        format.html { render :edit }
        format.json { render json: @showing.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /showings/1
  # DELETE /showings/1.json
  def destroy
    @showing.destroy
    respond_to do |format|
      format.html { redirect_to showings_url, notice: 'Showing was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_showing
      @showing = Showing.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def showing_params
      params.require(:showing).permit(:agent_id, :lead_id, :address, :buyer_name, :agent_name, :buyer_id, :status, :mls_num)
    end
end
