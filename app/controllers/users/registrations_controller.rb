class Users::RegistrationsController < Devise::RegistrationsController
  skip_before_filter :authenticate_scope!, only: [:update, :destroy]
  skip_before_action :authenticate_scope!, only: [:update, :destroy]
  before_filter :authenticate, :configure_brokerage, only: [:create], 
                               :if => Proc.new { |c| c.request.format == 'application/json' }
  before_filter :configure_sign_up_params, only: [:create]
  before_filter :configure_account_update_params, only: [:update]

  respond_to :html, :json

  # GET /resource/sign_up
  def new
    params.merge!(new: true)
    super
  end

  # POST /resource
  def create
    if params[:user].present?
  		config_email
    	config_password
    end
    check_disable_welcome_email
    super
  end

  # GET /resource/edit
  # def edit
  #   super
  # end

  # PUT /resource
  def update
    if params[:user].present? && params[:user][:id].present?
    	self.resource = User.find_by(id: params[:user][:id])
    else
    	self.resource = resource_class.to_adapter.get!(send(:"current_#{resource_name}").to_key)
    end
    
    prev_unconfirmed_email = resource.unconfirmed_email if resource.respond_to?(:unconfirmed_email)

    resource_updated = update_resource(resource, account_update_params)
    yield resource if block_given?
    if resource_updated
      if is_flashing_format?
        flash_key = update_needs_confirmation?(resource, prev_unconfirmed_email) ?
          :update_needs_confirmation : :updated
        set_flash_message :notice, flash_key
      end
      bypass_sign_in resource, scope: resource_name
      respond_with resource, location: after_update_path_for(resource)
    else
      clean_up_passwords resource
      respond_with resource
    end
  end

  # DELETE /resource
  def destroy
    self.resource = User.find_by(email: params[:email_address]) if self.resource.nil?
    self.resource.destroy if !self.resource.nil?
    respond_to do |format|
      format.html { redirect_to sign_up_path, notice: "The user was deleted." }
      format.json { head :no_content }
    end
  end

  # GET /resource/cancel
  # Forces the session data which is usually expired after sign
  # in to be expired now. This is useful if the user wants to
  # cancel oauth signing in/up in the middle of the process,
  # removing all OAuth session data.
  # def cancel
  #   super
  # end

  protected
  
  def respond_with(resource, opts = {})
    if !params[:new].present? && resource.save
    	respond_to do |format|
    		format.html { super }
    		format.json { render json: resource, serializer: DeviseResourceSerializer }
    	end
    else
    	respond_to do |format|
    		format.html { super }
    		format.json { render json: resource.errors, :status => :bad_request }
    	end
    end
  end

  # You can put the params you want to permit in the empty array.
  def configure_sign_up_params
    devise_parameter_sanitizer.for(:sign_up) << [:brokerage_id, :confirmed_at, :temporary_password]
  end

  # You can put the params you want to permit in the empty array.
  def configure_account_update_params
    devise_parameter_sanitizer.for(:account_update)
  end

  # The path used after sign up.
  # def after_sign_up_path_for(resource)
  #   super(resource)
  # end

  # The path used after sign up for inactive accounts.
  # def after_inactive_sign_up_path_for(resource)
  #   super(resource)
  # end
  
  def config_email
  	if params[:user][:email_address]
    	params[:user].merge!(email: params[:user][:email_address])
    end
  end
  
  def config_password
  	if !params[:user][:password] && !params[:user][:password_confirmation]
    	generated_password = Devise.friendly_token.first(8)
    	params[:user][:password] = generated_password
    	params[:user][:password_confirmation] = generated_password
    end
    
    params[:user].merge!(temporary_password: params[:user][:password])
  end
  
  def check_disable_welcome_email
  	if params[:disable_welcome_email]
  		params[:user].merge!(confirmed_at: Time.now)
  	end
  end
  
  def configure_brokerage
    if !@brokerage.nil?
      params[:user].merge!(brokerage_id: @brokerage.id)
    end
  end
  
  def authenticate
    authenticate_token || render_unauthorized
  end

  def render_unauthorized
    self.headers['WWW-Authenticate'] = 'Token realm="Application"'
    error = {errors:  ["Token is invalid."] }
    respond_to do |format|
      format.html { redirect_to root_path }
      format.json { render json: error, status: 401 }
    end
  end
  
  def authenticate_token
  	authenticate_with_http_token do |token, options|
  		@brokerage = Brokerage.find_by(access_token: token)
  	end
  end
end
