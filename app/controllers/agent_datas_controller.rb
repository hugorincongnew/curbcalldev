class AgentDatasController < ApplicationController

  def index
    @agent_datas = AgentData.all

    render json: @agent_datas
  end

  # GET /agent_datas/1
  # GET /agent_datas/1.json
  def show
    @agent_data = AgentData.find(params[:id])

    render json: @agent_data
  end

  # POST /agent_datas
  # POST /agent_datas.json
  def create
    @agent_data = AgentData.new(data_params)

    if @agent_data.save
      render json: @agent_data, status: :created
    else
      render json: @agent_data.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /agent_datas/1
  # PATCH/PUT /agent_datas/1.json
  def update
    @agent_data = AgentData.find(params[:id])

    if @agent_data.update(data_params)
      head :no_content
    else
      render json: @agent_data.errors, status: :unprocessable_entity
    end
  end

  # DELETE /agent_datas/1
  # DELETE /agent_datas/1.json
  def destroy
    @agent_data = AgentData.find(params[:id])
    @agent_data.destroy

    head :no_content
  end

  private

  def data_params
    params.permit(:user_id, :showings, :requests, :live)
  end 

  def restrict_access
    authenticate_or_request_with_http_token do |token, options|
      ApiKey.exists?(access_token: token)
    end
  end
end
