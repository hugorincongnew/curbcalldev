class ShowingsController < ApplicationController
  acts_as_token_authentication_handler_for User, except: [:show]
  def show
    id = ScatterSwap.reverse_hash(params[:id])
    @showing = Showing.find(id)
    @sightings = @showing.sightings
  end
end
