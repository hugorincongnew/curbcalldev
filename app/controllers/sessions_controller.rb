class SessionsController < Devise::SessionsController
  skip_before_filter :verify_authenticity_token,
                     :if => Proc.new { |c| c.request.format == 'application/json' }

  respond_to :json
  


  #login
  def create
    #try to find a user from FB oauth token or email
    if(params[:user] && params[:user][:access_token])
      fb_user = CurbCall::Oauth.get(params[:user][:access_token])
      return failure("FB oauth error") unless fb_user
      resource = User.from_oauth(fb_user)
      return failure("Could not lookup user from FB oauth") unless resource 
    elsif(params[:user])
      resource = User.find_for_database_authentication(:email => params[:user][:email])
      #TODO handle case where resource is nil
      match = resource.valid_password?(params[:user][:password])
      return failure unless match 
    else #error
      return failure("did not find post or oauth info")
    end
    
    sign_in(:user, resource)      
                                   
    render :json => resource.signin_hash
  end

  def update
    resource = User.find(params[:id])

    if resource.update(params[:user])
      head :no_content
    else
      render json: resource.errors, status: :unprocessable_entity
    end
  end

  def destroy
    resource.reset_authentication_token!
    Devise.sign_out_all_scopes ? sign_out : sign_out(resource_name)
    render :status => 200, :json => {}
  end


  def failure(message)
    render :status => 401,
           :json => { :success => false,
                      :info => "Login Failed",
                      :data => {message: message} }
  end

  private

end