class Admin::MlsController < Admin::AdminController
  def map
    
  end
  
  def listings
    bounds = params[:bounds]
    parts = bounds.split(",")
    sw = "#{parts[0]},#{parts[1]}"
    ne = "#{parts[2]},#{parts[3]}"
    
    @listings = Listing.in_bounds([sw, ne]).select(:id,:lat, :lon, :address)
    
    render json: {listings: @listings}
  end
end