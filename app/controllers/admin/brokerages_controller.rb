class Admin::BrokeragesController < ApplicationController
    layout 'admin'
    respond_to :html
    
    before_action :set_brokerage, only: [:show, :edit, :update]
    
    def index
      @brokerages = Brokerage.all
    end
    
    def show
    end
    
    def new
      @brokerage = Brokerage.new
      build_default_agent
    end
    
    def edit
      if @brokerage.users.any?
        @default_agent = @brokerage.users.first
      else
        build_default_agent
      end
    end
    
    def create
      @brokerage = Brokerage.new(brokerage_params)
      
	  if @brokerage.save
		respond_with(:admin, @brokerage)
	  else
		respond_using(:admin, @brokerage, :new)
	  end
    end
    
    def update
      if @brokerage.update(brokerage_params)
  		respond_with(:admin, @brokerage)
  	  else
  		respond_using(:admin, @brokerage, :edit)
	  end
    end
    
    private
    
    def brokerage_params
      params.require(:brokerage).permit(:name, users_attributes: [:id, :email, :name, :password, :password_confirmation])
    end
    
    def set_brokerage
      @brokerage = Brokerage.find_by id: params[:id]
    end
    
    def build_default_agent
      @default_agent = @brokerage.users.build
    end
    
end