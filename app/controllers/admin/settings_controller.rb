class Admin::SettingsController < Admin::AdminController
  def index
    
    files = [CurbCall::Messages::Types::AgentDangerMessage::SETTINGS, 
      CurbCall::Messages::Types::ShowingStartedMessage::SETTINGS,
      CurbCall::Messages::Types::ShowingCompletedMessage::SETTINGS,
      CurbCall::Messages::Types::NoAgentsAvailMessage::SETTINGS,
      CurbCall::Twillio::SmsService::SETTINGS,
      CurbCall::SafetyRadius::Base::SETTINGS,
      CurbCall::Messages::EmailMessage::SETTINGS,
      CurbCall::Messages::Types::AgentPanicMessage::SETTINGS,
      CurbCall::DispatchService::Dispatcher::SETTINGS]
      
    settings = files.reduce({}, :merge)
    @settings = {}
    
    settings.each do |k,v|
      val = CurbCall::Config[v]
      @settings[k] = val
    end
    
    @settings
    
  end
  
  def show
  end
  
  def edit
    @setting = Setting.find(params[:id])
  end
  
  def update
    @setting = Setting.find(params[:id])
    
    respond_to do |format|
      if @setting.update(params[:setting])
        format.html { redirect_to admin_settings_path, notice: 'Setting was successfully updated.' }
      else
        format.json { render json: @setting.errors, status: :unprocessable_entity }
      end
    end
  end
  
end
