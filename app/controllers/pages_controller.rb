class PagesController < ActionController::Base

	layout 'application'
	
	def check_email
	end
	
	def reset_password_success
		reset_session
	end

end