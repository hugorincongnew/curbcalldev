class RegistrationsController < Devise::RegistrationsController
  skip_before_filter :verify_authenticity_token,
                     :if => Proc.new { |c| c.request.format == 'application/json' }

  respond_to :json


  def create

    begin
    resource = User.factory_with_oauth(sign_up_params)
    rescue NoMethodError
      return render :status => 401, :json => {:errors => "Could not find an email in user hash or oauth"}
    end
    
    resource.skip_confirmation!
    if resource.save
      sign_in(resource, :store => false)
      if resource.active_for_authentication?
        legacy_ds={:success => true,
                    :auth_token => resource.authentication_token,
                    :user_id => resource.id,
                    :name => resource.name,
                     :email => resource.email,
                     :organization => resource.brokerage,
                     :bio => resource.bio,
                     :image => resource.image,
                     :phone => resource.phone}
        return render :json => legacy_ds
      else
        expire_session_data_after_sign_in!
        return render :json => {:success => true}
      end
    else
      clean_up_passwords resource
      return render :status => 401, :json => {:errors => resource.errors}
    end


  end


  private

  def account_update_params
    params.require(:user).permit( :email, :password, :password_confirmation, :current_password, :office_admin)
  end

  def sign_up_params
    params.require(:user).permit( :email, :password, :password_confirmation, :authentication_token, :name, :brokerage_id, :bio, :image, :phone, :friendly, :local_expert, :access_token, :agent)
  end
end