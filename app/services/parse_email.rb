class ParseEmail

  ##############################################
  ############ PARSE EMAILS STUFF ##############
  ##############################################

  # compare email with template, find data, send data to CRM
  def self.parse_data(email, template)
    data = {}
    total_patterns = template.patterns.count

    template.patterns.each do |pattern|
      result = ParseEmail.parse_pattern(email.body, pattern.start_word, pattern.end_word)
      data[pattern.title.downcase.to_sym] = result unless result.nil? || result.blank?
    end

    if data.count > 0 && data.count == total_patterns
      load_data_to_crm(data, email, template)
      true
    else
      UserMailer.mail_not_parsed(email.user, email).deliver
      false
    end
  end

  # return found word from pattern OR return false
  def self.parse_pattern(body, start_word, end_word)
    # decode words
    start_word = decode(start_word)
    end_word   = decode(end_word)

    start_word_i = body.index(start_word)
    if start_word_i
      end_word_i = body[start_word_i..-1].index(end_word)
      end_word_i += start_word_i if !end_word_i.nil?
    else
      end_word_i = body.index(end_word)
    end

    if start_word_i && end_word_i
      start_i = start_word_i + start_word.length
      end_i   = (end_word == "") ? -1 : end_word_i - 1
      body[start_i..end_i].split.join(' ')
    else
      false
    end
  end

  # parse one email with all templates
  def self.parse(email)
    templates = email.find_email_templates(email.from)
    subject = email.subject.downcase
    parsed = false

    templates.each do |template|
      valid_subject = template.valid_subject.try(:downcase) || ""
      if template && ( subject.include?(valid_subject) || valid_subject.empty? )
        parsed = parse_data(email, template)
      end
    end
    return parsed
  end

  # parse collection of emails
  def self.parse_emails_collection(emails)
    total_emails  = emails.count
    parsed_emails = 0
    emails.each do |email|
      parsed_emails += 1 if parse(email)
    end
    return parsed_emails, total_emails
  end

  ##################################################
  ########### DETERMIN PATTERN FROM WORD ###########
  ##################################################

  # prepare stuff to analyze pattern for goal_word
  def self.analyze_words(body, goal_word)
    goal_word.strip!
    body_length  = body.length
    word_length  = goal_word.length

    word_start_index = body.index(goal_word)
    word_end_index   = word_start_index + word_length

    return ParseEmail.analyze_pattern(word_start_index, word_end_index, body, goal_word)
  end

  # recursively search for pattern until found OR until error
  def self.analyze_pattern(word_start_index, word_end_index, body, goal_word, step=5)
    # update indexes of first and second words
    first_word_start_at = word_start_index - step
    second_word_end_at = word_end_index + step
    # calculate start_word from indexes
    if word_start_index == 0
      start_word = ""
    else
      first_word_start_at = 0 if first_word_start_at < 0
      start_word  = body[first_word_start_at..word_start_index-1]
    end
    # calculate end_word
    end_word = body[word_end_index..second_word_end_at]

    result = ParseEmail.parse_pattern(body, start_word, end_word)

    if result == goal_word
      return start_word, end_word
    elsif step <= 30
      ParseEmail.analyze_pattern(word_start_index, word_end_index, body, goal_word, step+1)
    else
      return nil, nil
    end
  end


private

  def self.decode(word)
    word.gsub("[line]", "\n").gsub("[carr]", "\r").gsub("[hor-tab]", "\t")
  end

  # SEND DATA TO CRM #
  def self.load_data_to_crm(data, email, template)
    user = User.find(template.user_id)
    source = template.source
    url = 'http://ftf.aws.af.cm/api/import_fuelleads'

    json_request = {
      "api_key" => user.api_key,
      "leads" => [
        [source, data[:name], data[:last_name], data[:email], data[:phone]]
      ]
    }.to_json
    p "FIRST NAME: #{data[:name]}, LAST NAME: #{data[:last_name]}"
    p "RAW DATA: #{data}"
    # [source, data[:name], "", data[:email], data[:phone], "street", "", "city", "state", "zip", "notes"]

    puts "EMAIL-SENT-TO-CRM >> user_id: #{user.id} >> email_id: #{email.id} >> template_id: #{template.id}"

    RestClient.post( 'http://ftf.aws.af.cm/api/import_fuelleads', json_request, :content_type => :json )

    email.update_attributes(parsed: true)

    # API STRUCTURE
    # {"api_key":"key_goes_here","leads":[["source","first_name","last_name","email","phone","street","","city","state","zip","body"],["FuelPages","Sally","Anne","sally@anne.com","1234567890","123 anwhere st","","dallas","tx","45768","possibly some notes here"]]}
  end

end
