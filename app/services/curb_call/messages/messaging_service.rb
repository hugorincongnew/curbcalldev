module CurbCall
  module Messages
    class MessagingService
      
      def initialize(message, users)
        @message = message
        @users = users
        load_plugins()
      end
    
      
      def deliver()  
        @users.each do |user|
          @plugins.each do |plugin|
            plugin = plugin.new(@message, user)
            plugin.deliver()
          end
        end
      end
      
      private
      
 
      
      def load_plugins
        @plugins = []
        #load plugins
        paths = Dir["#{Rails.root}/app/services/curb_call/messages/plugins/*_plugin.rb"]
        paths.each do |path|
          require path
          name = File.basename(path, ".*")
          klass_name = name.to_s.split('_').map(&:capitalize) * ''
          plugin = CurbCall::Messages::Plugins.const_get(klass_name)  
          @plugins << plugin
        end
        @plugins
      end

    end
  end
end