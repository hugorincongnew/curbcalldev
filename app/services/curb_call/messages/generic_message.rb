module CurbCall
  module Messages
    class GenericMessage
      attr_reader :body
      def initialize(template, args)
        @template = template
        @args = args
        @body = eval_template(@template, @args)
      end
      
      def deliver
        raise "this must be implemented in the sub-class"
      end
      
      private
      
      def eval_template(input, args)
        output = Mustache.render(input, args)
        output
      end
    end
  end
end