module CurbCall
  module Messages
    module Types
      class AgentPanicMessage < CurbCall::Messages::Types::BaseMessage
        SETTINGS = 
        {
          template_agent_panic_email_body:    "template_agent_panic_email_body",
          template_agent_panic_email_subject: "template_agent_panic_email_subject",
          template_agent_panic_sms_body:      "template_agent_panic_sms_body"
        }
        
        attr_reader :args
        
        #for sidekiq
        def self.enqueue(users, model)
          link = MagicLinkService.showing(model.id)
          args = {name: model.showing_owner.name, address: model.address, link: link}
          users.each do |user|
            ::MessageWorker.perform_async("AgentPanicMessage", user.id, args)
          end
        end
        
        def deliver_all(user_id)
          user = EmergencyContact.find(user_id)
          self.deliver_email(user)
          self.deliver_sms(user)
        end
        
        def deliver_email(user)
          if(user.email)
            ::GenericMailer.template(user.email, @from, @email_subject, @email_body ).deliver
          end
        end
        
        def deliver_sms(user)
          if(user.phone)
            ::CurbCall::Twillio::SmsService.send(user.phone, @sms_body)
          end
        end
        
        def initialize(args)
          @from = load_setting("messages_email_message_from")
          email_body_template = load_setting(SETTINGS.fetch(:template_agent_panic_email_body))
          email_body_markdown_template = eval_template(email_body_template, args)
          @email_body = eval_markdown(email_body_markdown_template)

          email_subject_template = load_setting(SETTINGS.fetch(:template_agent_panic_email_subject))
          @email_subject = eval_template(email_subject_template, args)

          sms_body_template = load_setting(SETTINGS.fetch(:template_agent_panic_sms_body))
          @sms_body = eval_template(sms_body_template, args)
        end
        
      end
    end
  end
end