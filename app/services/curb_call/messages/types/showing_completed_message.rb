module CurbCall
  module Messages
    module Types
      class ShowingCompletedMessage < CurbCall::Messages::Types::BaseMessage
        SETTINGS =
        {
          template_showing_completed_email_body: "template_showing_completed_email_body",
          template_showing_completed_email_subject: "template_showing_completed_email_subject",
          template_showing_completed_sms_body: "template_showing_completed_sms_body"
        }

        attr_reader :args

        def initialize(showing)
          @args = {name: User.find(showing.showing_owner_id).name, address: showing.address}
        end

        def body_template
          email_body_key=SETTINGS[:template_showing_completed_email_body]
          email_body_template = load_setting(email_body_key)
        end

        def subject_template
          email_subject_key=SETTINGS.fetch(:template_showing_completed_email_subject)
          email_subject_template = load_setting(email_subject_key)
        end

        def sms_template
          sms_body_key=SETTINGS[:template_showing_completed_sms_body]
          sms_body_template =  load_setting(sms_body_key)
        end
      end
    end
  end
end
