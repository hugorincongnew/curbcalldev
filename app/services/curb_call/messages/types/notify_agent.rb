module CurbCall
  module Messages
    module Types
      class NotifyAgent < CurbCall::Messages::Types::BaseMessage
      
        attr_accessor :args
      
        #for sidekiq
        def self.enqueue(showing, agents)
          args = { alert: "You've Been Requested!", sound: "sosumi.aiff", message: 'request', showing_id: showing.id }
          agents.each do |agent|
            ::MessageWorker.perform_async("NotifyAgent", agent.user.player_id, showing.id, args)
          end
        end
      
        def initialize(args)
          @args = args
        end
      
        def deliver_all(user_id, showing_id)
          #push = Parse::Push.new(@args)
          #query = Parse::Query.new(Parse::Protocol::CLASS_INSTALLATION).eq('user_id', user_id)
          #push.where = query.where
          #push.save
          if user_id
            params = {app_id: ENV["ONESIGNAL_APP_ID"], contents: { en: "You've Been Requested!"}, data: {showing_id: showing_id}, include_player_ids: [user_id]}
            OneSignal::Notification.create(params: params)
          end
        end
        
      end
    end
  end
end
