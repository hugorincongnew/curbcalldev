module CurbCall
  module Messages
    module Types
      class BaseMessage
        SETTINGS = 
        {
          base_message_setting: "I am the super class"
        }
        
        
        def load_setting(key)
          val = CurbCall::Config[key]
          raise "#{key} not found in redis" unless val
         
          val
        end
        
        def eval_template(input, args)
          output = Mustache.render(input, args)
          output
        end
        
        def eval_markdown(input)
           @markdown = Redcarpet::Markdown.new(Redcarpet::Render::HTML, autolink: true, tables: true)
          output = @markdown.render input
          output
        end
        
     
      end
    end
  end
end