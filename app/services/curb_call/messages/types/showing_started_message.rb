module CurbCall
  module Messages
    module Types
      class ShowingStartedMessage < CurbCall::Messages::Types::BaseMessage
        SETTINGS = {
          template_showing_started_email_body:    "template_showing_started_email_body",
          template_showing_started_email_subject: "template_showing_started_email_subject",
          template_showing_started_sms_body:      "template_showing_started_sms_body"
        }

        attr_reader :args

        #for sidekiq
        def self.enqueue(users, model)
          link = MagicLinkService.showing(model.id)
          showing_owner = User.find(model.showing_owner_id)
          args = {name: showing_owner.name, address: model.address, link: link}
          users.each do |user|
            ::MessageWorker.perform_async("ShowingStartedMessage", user.id, args)
          end
        end

        def initialize(args)
          @from = load_setting("messages_email_message_from")


          email_body_template = load_setting(SETTINGS.fetch(:template_showing_started_email_body))
          email_body_markdown_template = eval_template(email_body_template, args)
          @email_body = eval_markdown(email_body_markdown_template)

          email_subject_template = load_setting(SETTINGS.fetch(:template_showing_started_email_subject))
          @email_subject = eval_template(email_subject_template, args)

          sms_body_template = load_setting(SETTINGS.fetch(:template_showing_started_sms_body))
          @sms_body = eval_template(sms_body_template, args)
        end


        #entry point for sidekiq
        def deliver_all(user_id)
          user = EmergencyContact.find(user_id)
          self.deliver_email(user)
          self.deliver_sms(user)
        end


        def deliver_email(user)
          if(user.email)
            begin
            	::GenericMailer.template(user.email, @from, @email_subject, @email_body ).deliver
            rescue Net::SMTPServerBusy, ArgumentError => ex
            	puts ex.message
            end
          end
        end

        def deliver_sms(user)
          if(user.phone)
            begin
            	::CurbCall::Twillio::SmsService.send(user.phone, @sms_body)
            rescue Twilio::REST::RequestError => ex
            	puts ex.message
            end
          end
        end

      end
    end
  end
end
