module CurbCall
  module Messages
    module Types
      class NoAgentsAvailMessage < CurbCall::Messages::Types::BaseMessage
        SETTINGS = 
        {
          template_no_agents_avail_email_body: "template_no_agents_avail_email_body",
          template_no_agents_avail_email_subject: "template_no_agents_avail_email_subject"
        }
        
        attr_reader :args
        
        #for sidekiq
        def self.enqueue(users, args={})
          users.each do |user|
            ::MessageWorker.perform_async("NoAgentsAvailMessage", user.id, args)
          end
        end
        
        #sidekiq calls this
        def self.deliver(users, args={})
          message = CurbCall::Messages::Types::NoAgentsAvailMessage.new(args)
          users.each do |user|
            #or use a diff mailer w/ more formatting etc...
            message.deliver(user)
          end
        end
        
        def deliver_all(user_id)
          user = User.find(user_id)
          self.deliver_email(user)
        end
        
        def deliver_email(user)
          if(user.email)
            ::GenericMailer.template(user.email, @from, @email_subject, @email_body ).deliver
          end
        end
        
        def initialize(args={})
          @from = load_setting("messages_email_message_from")
          email_body_key=SETTINGS.fetch(:template_no_agents_avail_email_body)
          email_body_template = load_setting(email_body_key)
          email_body_markdown_template = eval_template(email_body_template, {})
          @email_body = eval_markdown(email_body_markdown_template)
          
          email_subject_key=SETTINGS.fetch(:template_no_agents_avail_email_subject)
          email_subject_template = load_setting(email_subject_key)
          @email_subject = eval_template(email_subject_template, {})
        end
        
      end
    end
  end
end