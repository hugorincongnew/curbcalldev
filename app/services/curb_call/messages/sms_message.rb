module CurbCall
  module Messages
    class SmsMessage < CurbCall::Messages::GenericMessage
      def initialize(template, args, to)
        super(template, args)

        @to = to
      end
      
      def deliver
        CurbCall::Twillio::SmsService.send(@to, @body)
      end
    end
  end
end