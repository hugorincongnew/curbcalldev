module CurbCall
  module Messages
    module Plugins 
      class SmsPlugin < CurbCall::Messages::Plugins::Base
        
        def initialize(message, user)
          @user = user
          @message = message
        end
        
        def deliver
          if(@user.phone)
          	begin
            	sms = CurbCall::Messages::SmsMessage.new(@message.sms_template, @message.args, @user.phone)
            	sms.deliver
            rescue Twilio::REST::RequestError => ex
            	puts ex.message
            end
          end
        end
      
        
      end
    end
  end
end