module CurbCall
  module Messages
    module Plugins 
      class EmailPlugin < CurbCall::Messages::Plugins::Base
        
        def initialize(message, user)
          @user = user
          @message = message
        end
        
        def deliver
          if(@user.email)
          	begin
            	mailer = CurbCall::Messages::EmailMessage.new(@message.body_template, @message.subject_template, @message.args, @user.email)
            	mailer.deliver
            rescue Net::SMTPServerBusy, ArgumentError => ex
            	puts ex.message
            end
          end
        end
        
      end
    end
  end
end