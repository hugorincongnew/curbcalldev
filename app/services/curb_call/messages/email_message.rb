module CurbCall
  module Messages
    class EmailMessage < CurbCall::Messages::GenericMessage
      
      SETTINGS = {messages_email_message_from: "messages_email_message_from"}
      
      def initialize(template, subject, args, to)
        super(template, args)

        @markdown = Redcarpet::Markdown.new(Redcarpet::Render::HTML, autolink: true, tables: true)
        @body = eval_markdown(@body)
        @subject = subject
        @to = to
        @from = CurbCall::Config[SETTINGS.fetch(:messages_email_message_from)]
       
      end
      
         
      def deliver
        ::GenericMailer.template(@to, @from, @subject, @body).deliver
      end
      
      private 
      
      def eval_markdown(input)
        
        output = @markdown.render input
        #do some routine to render html from @body
        # output = ....
        output
      end
   
    end
  end
end