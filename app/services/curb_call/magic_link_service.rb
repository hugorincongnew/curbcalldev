module CurbCall
  class MagicLinkService
    def initialize(args)
    
    end
  
    def self.showing(showing_id)
      hash = ScatterSwap.hash(showing_id)
      Rails.application.routes.url_helpers.hidden_showing_url(hash, host: ENV.fetch('ASSET_HOST'))
    end
  
  end
end