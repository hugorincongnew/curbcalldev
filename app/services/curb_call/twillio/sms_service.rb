module CurbCall
  module Twillio
    class SmsService
      SETTINGS={twillio_sms_service_from_number: "twillio_sms_service_from_number"}
      
      def self.send(to, body)
        #+18437798962
        from = CurbCall::Config[SETTINGS[:twillio_sms_service_from_number]]
        self._send(from, to, body)
      end
      
      def self._send(from, to, body)
        @client = Twilio::REST::Client.new
        @client.messages.create(from: from, to: to, body: body)
      end
    end
  end
end