module BrokerDashboardHelper
  def badge_helper(state)
    return case state
      when "warning"
        "warning"
      when "danger"
        "danger"
      else
        "success"
      end
  end
  
  def badge_helper_showing(state)
    return case state
      when "waiting"
        "primary"
      when "transit", "started" 
        "info"
      when "completed" 
        "success"
      when "expired" 
        "warning"
      else
        "default"
      end
  end
  
  def image_url(user)
    user.try(:profile).try(:profile_pic) || path_to_image("dashboard/silhouette.png")
  end
  
  def lookup_flash(name)
    case name
    when "notice" then "warning"
    when "info" then "info"
    when "alert" then "danger"  
    end
  end
  
  def last_showing(agent)
    date=nil
    if(agent.showings.empty?)
      date = "None"
    else
      date = agent.showings.last.created_at.strftime("%a %b %d %Y")
    end
    
    date
  end
end
