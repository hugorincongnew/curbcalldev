json.array!(@showings) do |showing|
  json.extract! showing, :id, :agent_id, :lead_id, :address, :buyer_name, :agent_name, :buyer_id, :status, :mls_num
  json.url showing_url(showing, format: :json)
end
