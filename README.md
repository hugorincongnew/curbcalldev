# Backend API for Curb Call


1. ### Agent App: When agent sets status to "available", Agent is created. App starts querying API for EligibleAgents that match his user_id
2. ### Buyer App: Buyer requests showing
3  ### Buyer App: Buyer app creates Showing with their ID and address of the listing.
4. ### Buyer App: Buyer app GETS all Agents and calculates distance to each. App selects closest one and finds all agents within 1 mile of that cloests agent (including the closest agent) and creates ElegibleAgent entries for each of those Agents. ElegibleAgents belong to Showings.
4. ### Agent App: Has been querying for EligibleAgent entries that match their ID. It GETS one and displays Buyer Alert and also saves the info about the buyer from the Lead. If agent claims it, app PUTs it's agent_id to the Showing and updates the Showing status field to "claimed"
5. ### Buyer App: has been querying for the status field of it's Showing to change to "claimed". Once it has changed, buyer app GETs it to gain all the data about the Agent.
