class AgentMailerPreview < ActionMailer::Preview
  def welcome
    # Mock up some data for the preview
    # user = FactoryGirl.build(:user)
    user = User.last
    # user.office_admin=true

    # Return a Mail::Message here (but don't deliver it!)
    AgentMailer.welcome(user)
  end
end