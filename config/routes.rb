CurbApi::Application.routes.draw do

  mount_griddler
  
  namespace :developers do
    devise_for :developers, module: "developers/devise", path: "developer"
    root 'dashboard#index'
    resources :apps
    get 'api' => 'api#index'
    get 'api/:action' => 'api#index', as: "api_doc"
  end

  namespace :api do
    namespace :v3 do
      get 'agents/find'
      get 'agents' => "agents#index"
      get 'service/index'
      get 'organization/showing_report' => "organization#showing_report", as: :api_v3_organization_showing_report
      resources :agents do
      	resources :showings, only: [:index]
      end
      resources :users   
      resources :showings do
        member do
          post 'claim'
          post 'start'
          post 'end'
          post 'self_start'
        end
      end
      namespace :radius do
        get 'client_token' => "braintree#client_token"
        post 'subscriptions' => "braintree#subscriptions"
        devise_for :radius_users
        resources :users do
          resources :emergency_contacts
        end
      end
    end
  end

  get 'main/home'
  get 'pages/check_email' => 'pages#check_email'
  get 'pages/reset_password_success' => 'pages#reset_password_success'

  namespace :admin do
    root "dashboard#index"
    resources :settings do
    end

    resources :brokerages

    namespace :mls do
      get 'map'
      get 'listings'
    end
  end

  mount RailsAdmin::Engine => '/rails_admin', as: 'rails_admin'

  get "brokerage_dashboard/showings/real_time" => "brokerage_dashboard/showings#real_time", as: "real_time_brokerage_dashboard_showings_path"

  shallow do
    namespace :brokerage_dashboard do
      get '/' => 'dashboard#index'
      resources :brokerage do
        resources :users do
          member do
            get 'reset_password'
          end
          resources :showings do
            resources :sightings
          end
        end
      end
    end
  end

  # resources :showings
  get 's/:id' => 'showings#show', :as => "hidden_showing"


  devise_for :users, controllers: { sessions: "users/sessions", passwords: "users/passwords", registrations: "users/registrations" }



  devise_scope :user do
    post 'registrations' => 'registrations#create', :as => 'register'
    post 'sessions' => 'sessions#create', :as => 'login'
    put 'sessions' => 'sessions#update', :as => 'edit_session'
    delete 'sessions' => 'sessions#destroy', :as => 'logout'
    put 'registrations' => 'registrations#edit', :as => 'edit'
  end

  namespace :api do
    namespace :v1 do
      resources :regions, except: [:new, :edit]
      resources :brokerages, except: [:new, :edit]
      resources :zips#, except: [:new, :edit]
      resources :profiles
      resources :eligible_agents
      resources :showings, except: [:new, :edit]
      resources :leads
      resources :agents#, except: [:new, :edit]
      resources :listings, except: [:new, :edit]
      resources :nearby_agents
      resources :mls
      resources :get_listings
      resources :dev_tokens
    end

    namespace :v2 do
      resources :regions, except: [:new, :edit]
      resources :brokerages, except: [:new, :edit]
      resources :zips#, except: [:new, :edit]
      resources :profiles
      resources :eligible_agents
      resources :showings#, except: [:new, :edit]
      resources :leads
      resources :agents#, except: [:new, :edit]
      resources :listings, except: [:new, :edit]
      resources :nearby_agents
      resources :mls
      resources :get_listings
      resources :dev_tokens
      resources :emergency_contacts
      post 'sightings/heartbeat'
    end
  end

  require 'sidekiq/web'
  mount Sidekiq::Web => '/sidekiq'


  # You can have the root of your site routed with "root"
  root 'main#home'


end
