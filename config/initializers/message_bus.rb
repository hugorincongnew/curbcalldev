redis_uri = URI.parse(ENV.fetch('REDISCLOUD_URL'))
redis_config = {:host => redis_uri.host, :port => redis_uri.port, :password => redis_uri.password}
MessageBus.redis_config = redis_config

