Parse.init :application_id => ENV.fetch("PARSE_APP_ID"),
           #:api_key        => ENV.fetch("PARSE_API_KEY"),
           :master_key     => ENV.fetch("PARSE_MASTER_KEY"),
           :quiet          => true | false,
           :host           => ENV.fetch("PARSE_SERVER_URI"),
           :path           => '/parse'