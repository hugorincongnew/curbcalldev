Braintree::Configuration.environment  = ENV.fetch('BRAINTREE_ENVIRONMENT').to_sym
Braintree::Configuration.merchant_id  = ENV.fetch('BT_MERCHANT_ID')
Braintree::Configuration.public_key   = ENV.fetch('BT_PUBLIC_KEY')
Braintree::Configuration.private_key  = ENV.fetch('BT_PRIVATE_KEY')