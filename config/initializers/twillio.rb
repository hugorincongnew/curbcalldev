Twilio.configure do |config|
  config.account_sid = ENV.fetch("TWILLIO_ACCOUNT_SID")
  config.auth_token = ENV.fetch("TWILLIO_AUTH_TOKEN")
end