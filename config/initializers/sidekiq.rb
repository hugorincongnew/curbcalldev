uri = ENV.fetch('REDISCLOUD_URL')
namespace="CC"


Sidekiq.configure_server do |config|
  config.redis = { :url => uri, :namespace => namespace}
end

Sidekiq.configure_client do |config|
  config.redis = { :url => uri, :namespace => namespace }
end

