APN = Houston::Client.development
APN.certificate = File.read("apple_dev_push_notification.pem")

PPN = Houston::Client.production
PPN.certificate = File.read("apple_push_notification.pem")