module Tools
  module Import
    module Adapters
      class Sandicor < Adapter
        ParseError = Tools::Import::ParseError
        STATES={active: "1_0", sold: "2_0"}
        PROPERTY_CLASS="RE_1"
        MLS=1
       
        include Geokit::Geocoders

        def import(options={})
          p "sandicor"
          geocodio = Geocodio::Client.new(ENV.fetch('GEOCODIO_KEY'))
          count=0

          client = RETS::Client.login(options[:signin])
          client.search(options[:query]) do | data|
            mls = data["L_ListingID"]
  
            street=data['LR_remarks1616']
            city = data['L_City']
            state = data['L_State']
            zip = data['L_Zip']
            p address = "#{street}, #{city}, #{state} #{zip}"
  
            lat=0
            lng=0
            
            begin 
             results = geocodio.geocode([address]).best
             raise "Geocode failure" if results.nil?
             lat =results.latitude # or address.lat
             lng= results.longitude 
            rescue 
               p "could not goecode #{address}"
             end
             
             #status mapper 
             sandicor_status = data["L_Status"]
             p "sandicor_status= #{sandicor_status}"
             case sandicor_status
             when "SOLD"
               status = Listing::STATES[:sold]
             when "ACTIVE"
               status = Listing::STATES[:active]
             when "PENDING"
               status = Listing::STATES[:pending]
             when "BACK ON MARKET"
               status = Listing::STATES[:pending]
             when "CONTINGENT"
               status = Listing::STATES[:pending]
             else
               p "No mapper for state: #{sandicor_status}"
               status = Listing::STATES[:sold]
             end
  
            photo_url = "https://s3.amazonaws.com/seth_static_images/no_image.jpg"
            client.get_object(:resource => :Property, :type => :Photo, :location => true, :id => "#{mls}") do |headers, content|
              photo_url = headers["location"]
              break
            end
  
            area = data['LM_Char10_3']
            name = data['LA1_Char50_3']

            attribs = {
          	     address: street,
                 mls_num: mls,
                 city: data["L_City"],
                 status: status,
                 subdivision: area,
                 list_office: data["LO1_OrganizationName"],
                 list_agent: name,
                 price: data['L_AskingPrice'],
                 baths: data['LM_Int2_6'],
                 beds: data ['LM_Int1_3'],
                 lat: lat,
                 lon: lng,
                 mls_id: MLS,
                 photo1: photo_url
            }
            
            resp = persist(attribs)
            count = count + 1;
            p "#{count} - #{resp[:action]}, #{attribs[:status]} - #{street} #{lat}, #{lng}"
            
          end
          client.logout
          p "end of data..."
        end #end import
      end
    end
  end
end



#https://rets-paragon.sandicor.com/rets/fnisrets.aspx/sandicor/search?SearchType=Property&Query=(L_AskingPrice=90000-100000)&Class=RE_1&QueryType=DMQL2&rets-version=rets/1.7.2

# http://retsgw.flexmls.com/rets2_1/Search?SearchType=Property&Class=A&QueryType=DMQL2&Query=(LIST_15=|OV61GOJ13C0)&Count=0&Format=COMPACT-DECODED&StandardNames=0&RestrictedIndicator=****&Limit=50


#example query
#(Price=100000-200000),(Area|=101,102),((ListingDate=2004-10-10+)|(Status=Active))  


# {
#                         "L_ListingID" => "130057247",
#                             "L_Class" => "RESIDENTIAL",
#                             "L_Type_" => "All Other Attached",
#                              "L_Area" => "ESCONDIDO (92025)",
#                       "L_SystemPrice" => "160000",
#                       "L_AskingPrice" => "125000",
#                     "L_AddressNumber" => "2425",
#               "L_AddressSearchNumber" => "2425",
#                  "L_AddressDirection" => "",
#                     "L_AddressStreet" => "Cranston Drive",
#                          "L_Address2" => "213",
#                              "L_City" => "Escondido",
#                             "L_State" => "CA",
#                               "L_Zip" => "92025",
#                              "L_Zip6" => "",
#                       "L_StatusCatID" => "Sold",
#                          "L_StatusID" => "0",
#                          "L_SaleRent" => "For Sale",
#                          "L_NumAcres" => "",
#                        "L_ListAgent1" => "85579",
#                       "L_ListOffice1" => "1306",
#                        "L_ListAgent2" => "",
#                       "L_ListingDate" => "2013-10-25",
#                    "L_ExpirationDate" => "",
#                     "L_OriginalPrice" => "",
#                           "L_Remarks" => "",
#                           "L_HowSold" => "",
#                      "L_ContractDate" => "2014-03-12",
#                       "L_ClosingDate" => "2014-03-17",
#                         "L_SoldPrice" => "160000",
#                     "L_SellingAgent1" => "85579",
#                    "L_SellingOffice1" => "1306",
#                      "L_HotSheetDate" => "",
#                        "L_StatusDate" => "",
#                         "L_PriceDate" => "",
#                         "L_InputDate" => "",
#                        "L_UpdateDate" => "2014-03-20T04:38:00",
#                      "L_PictureCount" => "1",
#                   "L_Last_Photo_updt" => "2014-02-16T15:10:00",
#                     "L_OffMarketDate" => "2014-03-12",
#     "L_listings_associated_doc_count" => "0",
#                  "L_asking_price_low" => "",
#                           "L_Address" => "2425 Cranston Drive",
#                           "L_ZipCode" => "92025-",
#                            "L_Status" => "SOLD",
#                               "L_DOM" => "",
#                             "L_DOMLS" => "137",
#                      "L_PricePerSQFT" => "",
#                          "LM_Char1_1" => "",
#                          "LM_Char1_2" => "No",
#                          "LM_Char1_3" => "No",
#                          "LM_Char1_5" => "Yes",
#                          "LM_Char1_6" => "Yes",
#                          "LM_Char1_7" => "",
#                          "LM_Char1_8" => "No",
#                          "LM_Char1_9" => "",
#                         "LM_Char1_10" => "",
#                         "LM_Char1_11" => "",
#                         "LM_Char1_12" => "",
#                         "LM_Char1_13" => "",
#                         "LM_Char1_14" => "",
#                         "LM_Char1_15" => "",
#                         "LM_Char1_16" => "",
#                         "LM_Char10_1" => "San Diego",
#                         "LM_Char10_2" => "North County",
#                         "LM_Char10_3" => "SOUTH ESCONDIDO",
#                         "LM_Char10_4" => "",
#                         "LM_Char10_5" => "",
#                         "LM_Char10_6" => "",
#                         "LM_Char10_7" => "",
#                         "LM_Char10_9" => "",
#                        "LM_Char10_10" => "Assessor Record",
#                        "LM_Char10_11" => "2 Story",
#                        "LM_Char10_12" => "",
#                        "LM_Char10_13" => "Full Address",
#                        "LM_Char10_14" => "",
#                        "LM_Char10_15" => "0 (Common Interest)",
#                        "LM_Char10_16" => "Assessor Record",
#                        "LM_Char10_17" => "",
#                        "LM_Char10_18" => "",
#                        "LM_Char10_19" => "",
#                        "LM_Char10_20" => "",
#                        "LM_Char10_21" => "",
#                        "LM_Char10_22" => "",
#                        "LM_Char10_23" => "",
#                        "LM_Char10_24" => "",
#                        "LM_Char10_25" => "",
#                        "LM_Char10_26" => "",
#                        "LM_Char10_27" => "",
#                        "LM_Char10_28" => "",
#                        "LM_Char10_29" => "",
#                        "LM_Char10_30" => "",
#                         "LM_Char25_1" => "238-141-43-13",
#                         "LM_Char25_2" => "",
#                         "LM_Char25_3" => "",
#                         "LM_Char25_4" => "",
#                         "LM_Char25_5" => "Adobe Hills",
#                         "LM_Char25_6" => "0",
#                         "LM_Char25_7" => "AGENT",
#                         "LM_Char25_8" => "(760) 888-7778",
#                         "LM_Char25_9" => "",
#                        "LM_Char25_10" => "",
#                        "LM_Char25_11" => "1103B7",
#                        "LM_Char25_12" => "12129",
#                        "LM_Char25_13" => "Realty One Group",
#                         "LM_Char50_1" => "Adobe Hills",
#                         "LM_Char50_2" => "Center City Pkwy",
#                         "LM_Char50_3" => "Adobe Hills",
#                         "LM_Char50_4" => "2425Cranston21392025",
#                         "LM_Char50_5" => "N/K",
#                           "LM_Int1_1" => "2",
#                           "LM_Int1_2" => "0",
#                           "LM_Int1_3" => "2",
#                           "LM_Int1_5" => "0",
#                           "LM_Int1_7" => "2",
#                           "LM_Int1_8" => "",
#                           "LM_Int1_9" => "",
#                           "LM_Int2_1" => "1989",
#                           "LM_Int2_2" => "0",
#                           "LM_Int2_3" => "2",
#                           "LM_Int2_4" => "2",
#                           "LM_Int2_5" => "",
#                           "LM_Int2_6" => "2",
#                           "LM_Int4_1" => "832",
#                           "LM_Int4_2" => "",
#                           "LM_Int4_3" => "0",
#                           "LM_Int4_4" => "0",
#                           "LM_Int4_5" => "",
#                           "LM_Int4_6" => "65705",
#                           "LM_Int4_7" => "0",
#                           "LM_Int4_8" => "2",
#                           "LM_Int4_9" => "0",
#                          "LM_Int4_10" => "0",
#                          "LM_Int4_13" => "160000",
#                          "LM_Int4_15" => "145",
#                          "LM_Int4_16" => "295",
#                          "LM_Int4_18" => "",
#                            "LM_Dec_1" => "",
#                            "LM_Dec_2" => "",
#                            "LM_Dec_3" => "295.31",
#                            "LM_Dec_4" => "3543.72",
#                            "LM_Dec_5" => "0",
#                            "LM_Dec_6" => "0",
#                            "LM_Dec_7" => "",
#                         "LM_char1_21" => "",
#                         "LM_char1_22" => "No",
#                          "LM_char5_1" => "",
#                          "LM_char5_2" => "B",
#                          "LM_char5_3" => "1103",
#                          "LM_char5_4" => "7",
#                          "LM_char5_5" => "No",
#                          "LM_char5_6" => "Attached",
#                          "LM_char5_7" => "",
#                          "LM_char5_8" => "Exclusive Right (R)",
#                          "LM_char5_9" => "",
#                         "LM_char5_10" => "",
#                         "LM_char5_11" => "",
#                         "LM_char5_12" => "1",
#                         "LM_char5_13" => "10x15",
#                         "LM_char5_14" => "10x9",
#                         "LM_char5_15" => "",
#                         "LM_char5_16" => "",
#                         "LM_char5_17" => "",
#                         "LM_char5_18" => "",
#                         "LM_char5_19" => "9x9",
#                         "LM_char5_20" => "9x14",
#                         "LM_char5_21" => "6x6",
#                         "LM_char5_22" => "0",
#                         "LM_char5_23" => "",
#                         "LM_char5_24" => "",
#                         "LM_char5_25" => "",
#                         "LM_char5_26" => "",
#                         "LM_char5_27" => "",
#                         "LM_char5_28" => "",
#                         "LM_char5_29" => "Month",
#                         "LM_char5_30" => "Monthly",
#                         "LM_char5_31" => "",
#                         "LM_char5_32" => "",
#                         "LM_char5_34" => "",
#                         "LM_char5_35" => "",
#                         "LM_char5_36" => "",
#                         "LM_char5_37" => "Vacant",
#                         "LM_char5_38" => "None Known",
#                         "LM_char5_39" => "",
#                         "LM_char5_40" => "",
#                         "LM_char5_41" => "",
#                         "LM_char5_42" => "",
#                         "LM_char5_43" => "",
#                         "LM_char5_44" => "North San Diego County Association of REALTORS (NSDCAR)",
#                        "LM_char10_31" => "",
#                        "LM_char10_32" => "",
#                        "LM_char10_33" => "",
#                        "LM_char10_34" => "",
#                        "LM_char10_35" => "",
#                        "LM_char10_36" => "06073",
#                        "LM_char10_37" => "Cash",
#                        "LM_char10_48" => "01905780",
#                        "LM_char100_1" => "",
#                        "LM_char100_2" => "",
#                        "LM_char100_3" => "",
#                        "LM_char100_5" => "",
#                        "LM_char100_6" => "",
#                        "LM_char100_7" => "2425 Cranston Dr 213",
#                          "LM_int4_21" => "",
#                           "LM_Dec_11" => "",
#                           "LM_Dec_12" => "",
#                           "LM_Dec_22" => "",
#                         "VT_VTourURL" => "",
#                     "VT_ExtVTourURL2" => "",
#                     "VT_ExtVTourURL3" => "",
#                 "LVC_Agent_Hit_Count" => "",
#                "LVC_Client_Hit_Count" => "",
#                     "LMD_MP_Latitude" => "",
#                    "LMD_MP_Longitude" => "",
#                      "LMD_MP_Quality" => "",
#                    "LMD_MP_ZoomLevel" => "",
#                  "LMD_MP_AddressLine" => "",
#                  "LMD_MP_PrimaryCity" => "",
#                "LMD_MP_SecondaryCity" => "",
#                  "LMD_MP_Subdivision" => "",
#                   "LMD_MP_PostalCode" => "",
#                    "LMD_MP_MatchCode" => "",
#                "LMD_MP_MatchedMethod" => "",
#                   "LMD_MP_UpdateDate" => "",
#                         "T_tax_db_id" => "",
#              "T_list_tax_property_id" => "",
#               "LFD_ComplexFeatures_1" => "",
#                 "LFD_Configuration_2" => "",
#                       "LFD_Cooling_3" => "N/K",
#                     "LFD_Equipment_4" => "Dishwasher",
#                      "LFD_Exterior_5" => "Stucco",
#                       "LFD_Fencing_6" => "Full",
#             "LFD_FireplaceLocation_7" => "",
#                "LFD_FloorCoverings_8" => "",
#                      "LFD_Frontage_9" => "",
#                   "LFD_GuestHouse_10" => "N/K",
#                "LFD_HeatEquipment_11" => "N/K",
#                   "LFD_HeatSource_12" => "",
#        "LFD_HomeOwnersFeeIncludes_13" => "Common Area Maintenance",
#                   "LFD_Irrigation_14" => "",
#              "LFD_LaundryLocation_15" => "N/K",
#             "LFD_LaundryUtilities_16" => "None Known",
#                 "LFD_OtherLockbox_17" => "",
#                "LFD_Miscellaneous_18" => "",
#                 "LFD_OtherFeeType_19" => "",
#                    "LFD_Ownership_20" => "Condominium",
#                 "LFD_ParkingforRV_21" => "",
#                "LFD_ParkingGarage_22" => "",
#             "LFD_ParkingNonGarage_23" => "Carport",
#                        "LFD_Patio_24" => "",
#                         "LFD_Pool_25" => "Community/Common",
#                     "LFD_PoolHeat_26" => "",
#                   "LFD_Possession_27" => "",
#        "LFD_PropRestrictionsKnown_28" => "CC&R's,Other/Remarks",
#      "LFD_ResidentialUnitLocation_29" => "",
#                         "LFD_Roof_30" => "Tile/Clay",
#            "LFD_SalesRestrictions_31" => "Need SS- No Lender Knwldg,Short Sale Approved",
#               "LFD_SchoolDistrict_32" => "",
#              "LFD_SearchableRooms_33" => "Family Room",
#                     "LFD_Security_34" => "",
#                  "LFD_SewerSeptic_35" => "Sewer Connected",
#                         "LFD_Site_36" => "",
#                     "LFD_Skirting_37" => "",
#                          "LFD_Spa_38" => "",
#                      "LFD_SpaHeat_39" => "",
#                   "LFD_Structures_40" => "",
#           "LFD_Telecommunications_41" => "",
#                        "LFD_Terms_42" => "Cash",
#                   "LFD_Topography_43" => "Level",
#                         "LFD_View_44" => "",
#                        "LFD_Water_45" => "Meter on Property",
#              "LFD_WaterHeaterType_46" => "",
#       "LFD_AdditionalPropertyUse_295" => "",
#                        "LR_remarks11" => "Poor condition, needs updating, some water related issues in bathroom and missing drywall. Cash sale only.",
#                        "LR_remarks22" => "FIRST EMAIL AGENT ONLY. I cannot return calls on this property. Property is not financable due to condition.",
#                        "LR_remarks33" => "",
#                        "LR_remarks44" => "",
#                        "LR_remarks55" => "EMAIL AGENT ONLY if property is active.",
#                        "LR_remarks66" => "",
#                      "LR_remarks1414" => "",
#                      "LR_remarks1515" => "01905780",
#                      "LR_remarks1616" => "2425 Cranston Drive 213",
#                       "LA1_LoginName" => "104514",
#                     "LA1_HiddenUsCID" => "",
#                   "LA1_UserFirstName" => "Adam",
#                    "LA1_UserLastName" => "Kelley",
#                          "LA1_UserMI" => "",
#                   "LA1_AddressNumber" => "",
#                "LA1_AddressDirection" => "",
#                   "LA1_AddressStreet" => "",
#                        "LA1_Address2" => "",
#                            "LA1_City" => "",
#                           "LA1_State" => "",
#                             "LA1_Zip" => "",
#                            "LA1_Zip6" => "",
#                "LA1_PhoneNumber1Desc" => "Agent",
#                "LA1_PhoneNumber1Area" => "760",
#                    "LA1_PhoneNumber1" => "8887778",
#                 "LA1_PhoneNumber1Ext" => "",
#                "LA1_PhoneNumber2Desc" => "",
#                "LA1_PhoneNumber2Area" => "",
#                    "LA1_PhoneNumber2" => "",
#                 "LA1_PhoneNumber2Ext" => "",
#                "LA1_PhoneNumber3Desc" => "Fax",
#                "LA1_PhoneNumber3Area" => "858",
#                    "LA1_PhoneNumber3" => "4325057",
#                 "LA1_PhoneNumber3Ext" => "",
#                "LA1_PhoneNumber4Desc" => "",
#                "LA1_PhoneNumber4Area" => "",
#                    "LA1_PhoneNumber4" => "",
#                 "LA1_PhoneNumber4Ext" => "",
#                "LA1_PhoneNumber5Desc" => "Home",
#                "LA1_PhoneNumber5Area" => "760",
#                    "LA1_PhoneNumber5" => "8887778",
#                 "LA1_PhoneNumber5Ext" => "",
#                           "LA1_Email" => "",
#                         "LA1_WebPage" => "",
#                          "LA1_Status" => "",
#                         "LA1_AgentID" => "104514",
#                        "LA1_Char50_1" => "",
#                        "LA1_Char50_3" => "Adam Kelley",
#                       "LA1_Char255_1" => "",
#                     "LA2_HiddenUsCID" => "",
#                   "LA2_UserFirstName" => "",
#                    "LA2_UserLastName" => "",
#                          "LA2_UserMI" => "",
#                   "LA2_AddressNumber" => "",
#                "LA2_AddressDirection" => "",
#                   "LA2_AddressStreet" => "",
#                        "LA2_Address2" => "",
#                            "LA2_City" => "",
#                           "LA2_State" => "",
#                             "LA2_Zip" => "",
#                            "LA2_Zip6" => "",
#                "LA2_PhoneNumber1Desc" => "",
#                "LA2_PhoneNumber1Area" => "",
#                    "LA2_PhoneNumber1" => "",
#                 "LA2_PhoneNumber1Ext" => "",
#                "LA2_PhoneNumber2Desc" => "",
#                "LA2_PhoneNumber2Area" => "",
#                    "LA2_PhoneNumber2" => "",
#                 "LA2_PhoneNumber2Ext" => "",
#                "LA2_PhoneNumber3Desc" => "",
#                "LA2_PhoneNumber3Area" => "",
#                    "LA2_PhoneNumber3" => "",
#                 "LA2_PhoneNumber3Ext" => "",
#                "LA2_PhoneNumber4Desc" => "",
#                "LA2_PhoneNumber4Area" => "",
#                    "LA2_PhoneNumber4" => "",
#                 "LA2_PhoneNumber4Ext" => "",
#                "LA2_PhoneNumber5Desc" => "",
#                "LA2_PhoneNumber5Area" => "",
#                    "LA2_PhoneNumber5" => "",
#                 "LA2_PhoneNumber5Ext" => "",
#                           "LA2_Email" => "",
#                         "LA2_WebPage" => "",
#                          "LA2_Status" => "",
#                        "LA2_Char50_1" => "",
#                        "LA2_Char50_3" => "",
#                       "LA2_Char255_1" => "",
#                       "SA1_LoginName" => "104514",
#                     "SA1_HiddenUsCID" => "",
#                   "SA1_UserFirstName" => "Adam",
#                    "SA1_UserLastName" => "Kelley",
#                          "SA1_UserMI" => "",
#                   "SA1_AddressNumber" => "",
#                "SA1_AddressDirection" => "",
#                   "SA1_AddressStreet" => "",
#                        "SA1_Address2" => "",
#                            "SA1_City" => "",
#                           "SA1_State" => "",
#                             "SA1_Zip" => "",
#                            "SA1_Zip6" => "",
#                "SA1_PhoneNumber1Desc" => "Agent",
#                "SA1_PhoneNumber1Area" => "760",
#                    "SA1_PhoneNumber1" => "8887778",
#                 "SA1_PhoneNumber1Ext" => "",
#                "SA1_PhoneNumber2Desc" => "",
#                "SA1_PhoneNumber2Area" => "",
#                    "SA1_PhoneNumber2" => "",
#                 "SA1_PhoneNumber2Ext" => "",
#                "SA1_PhoneNumber3Desc" => "Fax",
#                "SA1_PhoneNumber3Area" => "858",
#                    "SA1_PhoneNumber3" => "4325057",
#                 "SA1_PhoneNumber3Ext" => "",
#                "SA1_PhoneNumber4Desc" => "",
#                "SA1_PhoneNumber4Area" => "",
#                    "SA1_PhoneNumber4" => "",
#                 "SA1_PhoneNumber4Ext" => "",
#                "SA1_PhoneNumber5Desc" => "Home",
#                "SA1_PhoneNumber5Area" => "760",
#                    "SA1_PhoneNumber5" => "8887778",
#                 "SA1_PhoneNumber5Ext" => "",
#                           "SA1_Email" => "",
#                         "SA1_WebPage" => "",
#                          "SA1_Status" => "",
#                        "SA1_Char50_1" => "",
#                        "SA1_Char50_3" => "Adam Kelley",
#                       "SA1_Char255_1" => "",
#                     "LO1_HiddenOrgID" => "1306",
#                   "LO1_BranchOfOrgID" => "27168",
#                     "LO1_HiddenOtyID" => "",
#                       "LO1_ShortName" => "",
#                "LO1_OrganizationName" => "Realty One Group",
#                "LO1_OrgAddressNumber" => "",
#             "LO1_OrgAddressDirection" => "",
#                "LO1_OrgAddressStreet" => "",
#                     "LO1_OrgAddress2" => "",
#                         "LO1_OrgCity" => "",
#                        "LO1_OrgState" => "",
#                          "LO1_OrgZip" => "",
#                         "LO1_OrgZip6" => "",
#                "LO1_PhoneNumber1Desc" => "Office",
#                "LO1_PhoneNumber1Area" => "760",
#                    "LO1_PhoneNumber1" => "4451702",
#                 "LO1_PhoneNumber1Ext" => "",
#                           "LO1_EMail" => "",
#                         "LO1_WebPage" => "",
#                        "LO1_board_id" => "",
#                        "LO1_Char10_1" => "Yes",
#                        "LO1_Char10_2" => "",
#                        "LO1_Char10_3" => "",
#                        "LO1_Char10_4" => "",
#                        "LO1_Char50_1" => "",
#                        "LO1_Char50_2" => "",
#                        "LO1_Char50_3" => "",
#                        "LO1_Char50_4" => "",
#                        "LO1_Char50_5" => "",
#                     "SO1_HiddenOrgID" => "1306",
#                   "SO1_BranchOfOrgID" => "27168",
#                     "SO1_HiddenOtyID" => "",
#                       "SO1_ShortName" => "",
#                "SO1_OrganizationName" => "Realty One Group",
#                "SO1_OrgAddressNumber" => "",
#             "SO1_OrgAddressDirection" => "",
#                "SO1_OrgAddressStreet" => "",
#                     "SO1_OrgAddress2" => "",
#                         "SO1_OrgCity" => "",
#                        "SO1_OrgState" => "",
#                          "SO1_OrgZip" => "",
#                         "SO1_OrgZip6" => "",
#                "SO1_PhoneNumber1Desc" => "Office",
#                "SO1_PhoneNumber1Area" => "760",
#                    "SO1_PhoneNumber1" => "4451702",
#                 "SO1_PhoneNumber1Ext" => "",
#                           "SO1_EMail" => "",
#                         "SO1_WebPage" => "",
#                        "SO1_board_id" => "",
#                        "SO1_Char10_1" => "",
#                        "SO1_Char10_2" => "",
#                        "SO1_Char10_3" => "",
#                        "SO1_Char10_4" => "",
#                        "SO1_Char50_1" => "",
#                        "SO1_Char50_2" => "",
#                        "SO1_Char50_3" => "",
#                        "SO1_Char50_4" => "",
#                        "SO1_Char50_5" => "",
#                      "LV_vow_include" => "Yes",
#                      "LV_vow_address" => "",
#                      "LV_vow_comment" => "Yes",
#                          "LV_vow_avm" => "Yes",
#                        "L_IdxInclude" => "Yes"
# }