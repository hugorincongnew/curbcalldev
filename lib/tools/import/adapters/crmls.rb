module Tools
  module Import
    module Adapters
      class Crmls < Adapter
        ParseError = Tools::Import::ParseError
       
        include Geokit::Geocoders

        def import(options={})
          p "import"
          geocodio = Geocodio::Client.new(ENV.fetch('GEOCODIO_KEY'))
          count=0

          client = RETS::Client.login(options[:signin])         
          client.search(options[:query]) do |data|
            
            public_listing = data["InternetSendAddressYN"] == "1"
            next unless public_listing #skip if not authorized
            
            street = "#{data["StreetNumber"]} #{data["StreetName"]}  #{data["StreetSuffix"]}"
            city = data["City"]
            zip = data["PostalCode"]
            address = "#{street}, #{city}, #{data["State"]} #{zip}"
            lat=0.0
            lng=0.0
         
            begin 
             results = geocodio.geocode([address]).best
             raise "Geocode failure" if results.nil?
             lat =results.latitude # or address.lat
             lng= results.longitude 
            rescue 
               p "could not goecode #{address}"
             end
            
            #status mapper 
            crmls_status = data["Status"]
            case crmls_status
            when "Closed Sale"
              status = Listing::STATES[:sold]
            when "Active"
              status = Listing::STATES[:active]
            when "Pending Sale"
              status = Listing::STATES[:pending]
            when "Backup Offer"
              status = Listing::STATES[:pending]
            else
              p "No mapper for state: #{crmls_status}"
              status = Listing::STATES[:sold]
            end
            

            photo_url = "https://s3.amazonaws.com/seth_static_images/no_image.jpg"            
            listing_key = data["ListingKey"] #lookup picture with this
            photo_opts = {:search_type => :Media, :class => :Media, :status => :Active,:query => "(ClassKey=#{listing_key})"}
            client.search(photo_opts) do |image_data|
              p photo_url = image_data["MediaURL"]
              break # we only want first image
            end
            
            
            area = data['Area'].split(" - ")[1]
            name = "#{data['LA_FirstName']} #{data['LA_LastName']}"

            attribs = {
          	     address: street,
                 mls_num: data["MLnumber"],
                 city: data["City"],
                 status: status,
                 subdivision: area,
                 list_office: data["LO_Name"],
                 list_agent: name,
                 price: data['ListPrice'],
                 baths: data['BathsTotal'],
                 beds: data ['Bedrooms'],
                 lat: lat,
                 lon: lng,
                 mls_id: 6,
                 photo1: photo_url
            }
             
            resp = persist(attribs)

            count = count +1;

            p "#{count} - #{resp[:action]}, #{attribs[:status]} - #{street} #{lat}, #{lng}"
             
          end
          
          client.logout
        end
      end
    end
  end
end
