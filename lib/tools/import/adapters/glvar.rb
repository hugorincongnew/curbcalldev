module Tools
  module Import
    module Adapters
      class Glvar < Adapter
        ParseError = Tools::Import::ParseError
        PROPERTY_CLASS="1"
        MLS=7
       
        include Geokit::Geocoders

        def import(options={})
          p "glvar"
          geocodio = Geocodio::Client.new(ENV.fetch('GEOCODIO_KEY'))
          count=0
      
          client = RETS::Client.login(options[:signin])
          client.search(options[:query]) do | data|
  
            lat=0
            lng=0
            
           p address = "#{data['244']} #{data['243']}, #{data['2909']}, #{data['2963']} #{data['10']}" 

            begin 
              results = geocodio.geocode([address]).best
            raise "Geocode failure" if results.nil?
              lat =results.latitude # or address.lat
              lng= results.longitude 
            rescue 
              p "could not goecode #{address}"
            end

            glvar_status = data['242']
            case glvar_status
            when "Closed"
              status = Listing::STATES[:sold]
            when "Active-Exclusive Right"
              status = Listing::STATES[:active]
            when "Pending Offer"
              status = Listing::STATES[:pending]
            else
              p "No mapper for state: #{glvar_status}"
              status = Listing::STATES[:sold]
            end

            mls_num = data['163']
            sys_id=data['sysid']

            # https://www.flexmls.com/developers/rets/tutorials/how-to-retrieve-high-resolution-photos/
            photo_url = "https://s3.amazonaws.com/seth_static_images/no_image.jpg"
           
            begin
              client.get_object(:resource => :Property, :type => "Photo", :location => false, :id => "#{sys_id}") do |headers, content|
                fn = headers["content-id"]

                sio = StringIO.new(content)
                def sio.original_filename; "glvar_#{Time.now.to_i}.png"; end
          			uploader = GlvarPicUploader.new
          			uploader.store!(sio)
                photo_url = uploader.url
                p photo_url
                break
              end
            rescue RETS::APIError
              p "Could not find a picture for mls: #{mls_num}, skipping this property"
              next
            end 


            attribs = {
              address: address,
              mls_num: mls_num, 
              city: data['2909'], 
              price: data['144'], 
              beds: data['68'], 
              baths: data['61'], 
              status: status,
              subdivision: data["247"],
              list_office: data["171"],
              list_agent: data["26"],
              lat: lat,
              lon: lng,
              mls_id: MLS,
              photo1: photo_url
            }
            ap attribs

            resp = persist(attribs)
            count = count + 1;
            p "#{count} - #{resp[:action]}, #{attribs[:status]} - #{mls_num}, #{address} #{lat}, #{lng}"


          end #end block
          client.logout 
          p "end of data..."
        end #end import
      end
    end
  end
end

h={
        "1" => "Single Family Res",
       "10" => "89011",
      "100" => "",
      "101" => "",
      "103" => "2500",
      "104" => "2011-05-31T11:12:44",
      "109" => "No",
      "112" => "None",
      "113" => "0",
      "120" => "No",
      "122" => "1",
      "125" => "Yes",
      "127" => "Monthly",
      "129" => "6",
      "130" => "Yes",
      "132" => "Res-Townhouse",
      "134" => "M",
      "135" => "2014-08-27T12:31:08",
      "137" => "KWMP",
      "138" => "2011-05-22T00:00:00",
      "143" => "002498",
      "144" => "90000",
      "154" => "1307",
      "155" => "None",
      "158" => "B5",
      "159" => "67",
      "163" => "1152057",
      "164" => "",
      "171" => "Keller Williams Market Place",
      "172" => "702-939-0000",
      "173" => "49000",
     "1734" => "",
     "1736" => "",
     "1738" => "84.90566",
      "175" => "No",
      "176" => "161-34-613-073",
      "177" => "E LAS VEGAS ARTSN B",
       "18" => "2014-06-11T10:55:26",
     "1809" => "Y",
      "182" => "User will upload by clicking on TOOLS in MLXchange/Fusion",
      "184" => "0",
      "186" => "",
      "187" => "",
      "188" => "Power On",
      "199" => "96000",
        "2" => "0.03",
      "202" => "",
      "203" => "No",
      "204" => "62",
      "205" => "",
      "206" => "Yes",
      "207" => "No",
      "208" => "800",
      "210" => "",
      "211" => "",
      "213" => "Treem Harriet*",
     "2139" => "http://instatour.propertypanorama.com/instaview/las/1152057",
      "214" => "Basic Tech Trade",
      "215" => "White Thurman",
      "216" => "No",
      "217" => "34",
      "218" => "",
      "219" => "Public",
      "221" => "",
     "2237" => "",
     "2238" => "2011-05-31T11:24:56",
      "228" => "",
      "231" => "0",
      "232" => "",
      "233" => "",
     "2341" => "85",
     "2343" => "B5",
     "2345" => "67",
      "235" => "90000",
     "2353" => "FIRST LIGHT AT BOULDER RANCH U",
     "2359" => "",
      "236" => "No",
     "2361" => "1060",
     "2367" => "",
     "2369" => "Yes",
      "237" => "1060",
     "2377" => "Thorpe Jim*",
     "2379" => "2",
     "2381" => "",
     "2383" => "702-456-1099",
     "2385" => "listings@brenkus.com",
     "2386" => "101",
     "2388" => "\"CCRs\",\"Gated\",\"Guest Parking\",\"Jogging Trail\",\"Play Ground/Park\"",
     "2390" => "\"Ground Maintenance\",\"Insurance\",\"Management\",\"Recreation Facilities\"",
     "2392" => "Yes",
     "2394" => "Yes",
     "2414" => "\"Two Story\"",
     "2416" => "595",
     "2418" => "",
      "242" => "Active-Exclusive Right",
     "2420" => "No",
     "2422" => "",
     "2424" => "No",
     "2426" => "No Furniture",
     "2428" => "No",
      "243" => "TRICKLING DESCENT ST",
     "2430" => "",
     "2432" => "",
     "2434" => "",
     "2438" => "\"No On Street Parking\"",
      "244" => "5943",
     "2440" => "No",
     "2442" => "No",
     "2448" => "",
     "2450" => "",
     "2452" => "Townhouse",
     "2454" => "Unknown",
     "2463" => "No",
     "2465" => "",
     "2467" => "",
      "247" => "FIRST LIGHT AT BOULDER RANCH U",
      "249" => "65",
       "25" => "",
      "250" => "Monthly",
     "2505" => "Townhouse",
     "2507" => "Attached",
     "2509" => "",
     "2511" => "",
     "2513" => "",
      "253" => "21",
     "2535" => "",
     "2537" => "",
     "2539" => "",
     "2543" => "1 Level/Second Floor",
     "2547" => "",
     "2549" => "",
     "2576" => "",
       "26" => "Rick Brenkus",
      "260" => "\"Closet\"",
      "261" => "Public",
     "2630" => "",
      "264" => "2001",
      "266" => "\"Multi-Family\"",
     "2660" => "No",
     "2661" => "",
      "268" => "",
      "269" => "\"Attached \",\"Auto Door Opener(s)\",\"Entry to House\",\"Storage Area/Shelves\"",
       "27" => "702-456-5959",
      "270" => "\"Pitched\",\"Composition Shingle\"",
      "271" => "\"Under 1/4 Acre\",\"Paved Road\"",
      "272" => "",
      "273" => "",
      "274" => "FROM I-95 TO RUSSELL RD * EAST ON RUSSELL TO BOULDER FALLS * S THRU GATE * 1T LEFT THEN RIGHT ON TRICKLING DESCENT * PROPERTY ON RIGHT",
      "276" => "\"Kitchen/Dining Room Combo\"",
      "277" => "\"None\"",
      "278" => "\"Breakfast Bar/Counter\",\"Solid Surface Countertops\",\"Tile Flooring\"",
      "279" => "\"Vaulted Ceiling\",\"Front\"",
       "28" => "633",
      "280" => "\"Double Sink\",\"Tub\"",
      "281" => "\"Mbr Separate From Other\",\"Mbr Walk-In Closet\",\"Master Bedroom Upstairs\"",
      "282" => "",
      "283" => "",
      "284" => "",
      "285" => "Close of Escrow",
     "2852" => "",
     "2858" => "Yes",
     "2859" => "Yes",
     "2860" => "Yes",
     "2861" => "5943  TRICKLING DESCENT ST 101",
     "2864" => "",
      "287" => "\"Cash\",\"Conventional\",\"FHA\"",
     "2878" => "",
     "2879" => "",
      "288" => "",
     "2883" => "",
      "289" => "\"Stove (G)\"",
     "2890" => "",
     "2892" => "",
     "2893" => "",
      "290" => "\"None\"",
     "2903" => "",
     "2904" => "",
     "2908" => "2014-08-27T00:00:00",
     "2909" => "Henderson",
      "291" => "\"Frame & Stucco\"",
      "292" => "\"Drywall\"",
     "2925" => "",
     "2927" => "",
     "2928" => "No",
     "2929" => "",
      "293" => "\"Carpet\",\"Tile\"",
     "2930" => "",
     "2931" => "",
     "2932" => "",
      "294" => "",
     "2940" => "1263",
     "2941" => "",
     "2946" => "Yes",
      "295" => "",
     "2953" => "1060",
      "296" => "\"None\"",
     "2963" => "Nevada",
      "297" => "East",
      "298" => "\"None\"",
      "299" => "\"Courtyard\"",
        "3" => "21",
       "30" => "Yes",
      "300" => "\"Drip Irrigation/Bubblers\",\"Desert Landscaping\"",
      "301" => "\"Central\"",
      "302" => "\"Gas\"",
      "303" => "\"Central\"",
      "304" => "\"Cable TV Wired\"",
      "305" => "\"None\"",
       "31" => "Yes",
       "32" => "",
       "33" => "No",
       "34" => "Gas",
       "35" => "First Light",
       "36" => "702-942-2500",
       "37" => "601 - Henderson",
       "38" => "",
       "39" => "39",
        "4" => "62",
        "5" => "34",
       "54" => "",
       "56" => "",
       "58" => "Yes",
       "60" => "0",
       "61" => "2",
       "62" => "0",
       "63" => "2",
       "64" => "1/2 Bath Downstairs",
       "68" => "2",
       "70" => "",
       "71" => "",
       "72" => "Resale",
       "73" => "",
       "74" => "0",
       "84" => "",
       "85" => "",
       "86" => "Electric",
       "87" => "Clark County",
       "89" => "11X11",
       "90" => "",
       "91" => "",
       "92" => "8X8",
       "93" => "",
       "94" => "",
       "95" => "15X12",
       "96" => "14X12",
       "97" => "",
    "sysid" => "61066834"
}