# require_relative './adapter_error'
# require_relative './parse_error'
module Tools
  module Import
    module Import
      extend self


      # Get the current adapter class.
      def adapter
        return @adapter if defined?(@adapter) && @adapter

        self.use nil # load default adapter

        @adapter
      end


      # Set the MLS/RETS parser utilizing a symbol, string, or class.
      # Supported by default are:
      #
      # * <tt>:crmls</tt>
      # * <tt>:armls</tt>
      def use(new_adapter)
        @adapter = load_adapter(new_adapter)
      end
      alias adapter= use

      def load_adapter(new_adapter)
        case new_adapter
        when String, Symbol
          load_adapter_from_string_name new_adapter.to_s
        when NilClass, FalseClass
          load_adapter default_adapter
        when Class, Module
          new_adapter
        else
          raise ::LoadError, new_adapter
        end
      rescue ::LoadError => exception
        raise AdapterError.build(exception)
      end


      #add a listing to the database
      def import(options)
        adapter = current_adapter(options)
        begin
          adapter.instance.import(options)
        rescue adapter::ParseError => exception
          raise ParseError.build(exception, options)
        end
      end

      def current_adapter(options={})
        if new_adapter = options[:adapter]
          load_adapter(new_adapter)
        else
          adapter
        end
      end




      private

      def load_adapter_from_string_name(name)

        require_relative "./adapters/#{name.downcase}"
        klass_name = name.to_s.split('_').map(&:capitalize) * ''
        Tools::Import::Adapters.const_get(klass_name)
      end
    end
  end
end

