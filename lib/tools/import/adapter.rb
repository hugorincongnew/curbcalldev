require 'singleton'


module Tools
  module Import
    class Adapter
      
      ACTIONS={update: :update, create: :create}
      include Singleton
      def persist(valid_attributes)
        raise "Invalid State: #{valid_attributes[:status]}" unless valid?(valid_attributes)
      
        listing = Listing.find_by_mls_num(valid_attributes[:mls_num])
        
        type=ACTIONS[:create]
        
        if(listing) #update
          type = ACTIONS[:update]
          listing = listing.update(valid_attributes)
        else #create
          type = ACTIONS[:create]
          listing = Listing.create!(valid_attributes)
        end
        
        {listing: listing, action:type}
      end

     
      
      private
    
        def valid?(input)
          valid_status = Listing::STATES.values.include? input[:status]
          
          
        end


    end
  end
end

