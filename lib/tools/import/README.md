#Curb Call MLS/RETS Import System

This is a set of scripts that keep the Curb Call DB in sync w/ the various MLS/RETS databases for which we have agreements in place. To add support for a new data source, you must implement a new adapter (see New Adapter section).

##Directory Structure

```
.
├── import
│   ├── README.md
│   ├── adapter.rb
│   ├── adapter_error.rb
│   ├── adapters
│   │   └── crmls.rb
│   ├── import.rb
│   └── parse_error.rb`
```

##New Adapter
When you want to add support for a new datasource you must implement the adapter.rb protocal. The adapter protocal has 3 rules:

1. Your file must be in the adapters subdirectory
2. You must implement a ParseError exception class
3. You must implement the import(options={}) method
4. You must call `persist(attributes)` to save

Here is a bare-bones example:

`sample_adapter.rb`
```ruby
module Tools
  module Import
    module Adapters
      class Crmls < Adapter
        ParseError = Tools::Import::ParseError
        def import(options={})
          properties = query_datasource
          properties.each do |property|
            listing_attributes = { address: property.street, mls_num: property.mls_num, .... }
            persist(listing_attributes)
          end 
        end
      end
    end
  end
end
```

You would then run this adapter like this:

```
Tools::Import::Import.use :sample_adapter
Tools::Import::Import.import({})
```

