##Onboarding

The onboarding system follows the adapter pattern. When you want to onboard a new office, you take the file they send you and create an adapter that can parse the file, put it in a stardard format we know, and then pass that to the onboarding system.

New adapters go into `./adapters/`
Data for the adapters goes into `./data`

You adapter must include `include Tools::Onboard::Base` which has the onboarding logic. You must parse the file into a hash with the keys:

* brokerage_id 
* name
* email

You must also set the FILE_NAME constant so the program knows where to find the data file to parse

Then pass `onboard_list` an array of those hash values and it will onboard them.


###Example (Seattle)

To run the seattle onboard script, in the rails console:

Tools::Onboard::Adapters::SeattleAdapter.run