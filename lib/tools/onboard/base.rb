require "rubygems"
require "bundler/setup"

module Tools
  module Onboard
    module Base
      
      DATA_DIR = File.join(Dir.getwd ,'/lib/tools/onboard/data/')
      
      def onboard_list(users)
        ActiveRecord::Base.transaction do
          errs = []
          users.each do |user|
            begin
              onboard_user user[:brokerage_id], user[:name], user[:email], user[:emergency_contacts], user[:office_admin]
            rescue ActiveRecord::RecordInvalid => err
              p "ERR Message: #{err}"
              errs << err
            end
          end
          
          p ""
          p "finished: inserted: #{users.length-errs.length} records and experienced: #{errs.length} errors"
          errs.each{|err|p err}
          #raise "ROLLBACK!  "
        end
      end
    
      def onboard_user(brokerage_id, name, email, emergency_contacts, office_admin=false)
        brokerage = Brokerage.find(brokerage_id)
      
        user = User.new
        user.brokerage = brokerage
        user.name = name
        user.email = email
        password=name.downcase.strip.gsub(' ', '')
        user.password=password
        user.password_confirmation=password
        user.agent=true
        user.office_admin=office_admin
        # user doens't have mls_id column ?
        
        emergency_contacts.each do |contact|
          user.emergency_contacts.build(contact)
        end
    
        user.save!
        p "created agent: #{user.id} - #{user.email} - #{user.emergency_contacts.inspect}"
        

      end
    
      def rand_pass(size = 8)
         chars = (('a'..'z').to_a + ('0'..'9').to_a) - %w(i o 0 1 l 0)
         (1..size).collect{|a| chars[rand(chars.size)] }.join
      end
    end
  end
end

if __FILE__ == $0
   # ENV['RAILS_ENV'] = "development" # Set to your desired Rails environment name
#    ENV['ASSET_HOST']="http://localhost:3000"
#    require File.join(Dir.getwd ,'config/environment.rb')
   # CurbCall::Onboard.run
end