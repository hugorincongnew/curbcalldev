require "rubygems"
require "bundler/setup"
require 'csv'
require 'pp'


include Tools::Onboard::Base

class Tools::Onboard::Adapters::RogLvAdapter  
  BROKERAGE_ID=7
  
  def self.run(path, is_admins)
    users = self.parse(path, is_admins)
    
     onboard_list users
  end
  
  private
    
  
  def self.parse(path, is_admins)
    users=[]
    CSV.new(open(path)).each do |row|
      name = row[0]
      email = row[1]
      phone = row[2] #do I need phone?
      user={name: name, email: email,  brokerage_id: BROKERAGE_ID, emergency_contacts:[]}
      
      unless((em1name = row[3]).blank?)
        em1phone = row[4].delete("^0-9") if row[4]
        contact = {name: em1name, phone: em1phone}
        user[:emergency_contacts] << contact
      end
      
      unless((em2name = row[5]).blank?)
        em2phone = row[6].delete("^0-9") if row[6]
        contact = {name: em2name, phone: em2phone}
        user[:emergency_contacts] << contact
      end
      
      user[:office_admin] = is_admins

      users.push user
    end
    users
  end
  
end


if __FILE__ == $0
end