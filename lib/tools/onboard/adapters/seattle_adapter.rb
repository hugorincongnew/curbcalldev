require "rubygems"
require "bundler/setup"
require 'csv'
require 'pp'


include Tools::Onboard::Base

class Tools::Onboard::Adapters::SeattleAdapter  
  BROKERAGE_ID=5
  FILE_NAME="seattle.csv"
  
  def self.run
    path = File.join(DATA_DIR, FILE_NAME)
    users = self.parse(path)
    
     onboard_list users
  end
  
  private
    
  
  def self.parse(path)
    users=[]
    CSV.foreach(path) do |row|
      name = row[0]
      email = row[4]
      user={name: name, email: email,  brokerage_id: BROKERAGE_ID}

      users.push user
    end
    users
  end
  
end


if __FILE__ == $0
  path="/Users/eggie5/Development/curbcall/curbstaging/lib/tools/onboard/data/seattle.csv"
  
  SeattleAdapter.run path
end