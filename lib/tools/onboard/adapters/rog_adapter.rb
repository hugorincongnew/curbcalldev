require "rubygems"
require "bundler/setup"
require 'csv'
require 'pp'


include Tools::Onboard::Base

class Tools::Onboard::Adapters::RogAdapter  
  BROKERAGE_ID=1
  FILE_NAME="rog1.csv"
  
  def self.run
    path = File.join(DATA_DIR, FILE_NAME)
    users = self.parse(path)
    
     onboard_list users
  end
  
  private
    
  
  def self.parse(path)
    users=[]
    CSV.foreach(path) do |row|
      name = "#{row[2]} #{row[3]}"
      email = row[4]
      user={name: name, email: email,  brokerage_id: BROKERAGE_ID}

      users.push user
    end
    users
  end
  
end


if __FILE__ == $0
  path="/Users/eggie5/Development/curbcall/curbstaging/lib/tools/onboard/data/rog.csv"
  
  RogAdapter.run path
end