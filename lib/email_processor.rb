require './app/models/brokerage.rb'

class EmailProcessor

  def initialize(email)
    @email = email
  end

  def process
    email_address = (@email.to[0][:token] + '@curbcallcollect.com.')

    brokerage = Brokerage.find_by_lead_email(email_address) || Brokerage.find_by_lead_email(@email.headers['X-Forwarded-To'])

    body = @email.raw_html ? @email.raw_html : @email.raw_text
    body = body.encode('UTF-8', 'binary', invalid: :replace, undef: :replace, replace: '', universal_newline: true)
    if brokerage
      brokerage_email = Email.new
      brokerage_email.from     = @email.from[:host]
      brokerage_email.subject  = @email.subject
      brokerage_email.body     = Nokogiri::HTML(body).text
      brokerage_email.parsed   = false
      brokerage_email.user     = brokerage
      if brokerage_email.save
        brokerage.users.each do |user|
          GenericMailer.template(user.email, brokerage_email.from, brokerage_email.subject, brokerage_email.body).deliver
        end
        puts "EMAIL_PROCESSOR: Email for user with id #{user.id} is saved. Email id: #{user_email.id}"
      else
        puts "EMAIL_PROCESSOR: Email for user with id #{user.id} is not saved"
      end
    else
      puts "EMAIL_PROCESSOR: User with address #{email_address} is not found"
    end
  end

end
