namespace :tasks_expired_showing_request do
  desc "Change request status after 2 minutes"
  task first_task: :environment do
    @showings = Showing.to_expired
    @showings.each do |showing|
      puts "tasks_expired_showing_request"
      puts "#{showing.id}"
      showing.expired
    end
  end

end
