task get_mris: :environment do
	mls = Mls.where(name: "Metropolitan Regional Information Systems, Inc.").last
	client = RETS::Client.login(:url => "http://ptest.mris.com:6103/ptest/login", :username => "MRISTEST", :password => "PMRISTEST", :useragent => {:name => "RETS Test/1.0"})

	client.search(:search_type => :Property, :class => :RES, :query => "(ListPrice=950000-1000000)") do |data|
		if data['ListingStatus'] == "Active"
			listing = Listing.create!(address: data['FullStreetAddress'], 
								mls_num: data['ListingKey'], 
								city: data['CityName'],
								price: data['ListPrice'], 
								baths: data['Baths'], 
								beds: data['Beds'], 
								status: data['ListingStatus'], 
								remarks: data['remarks'],
								subdivision: data['Subdivision'], 
								list_office: data['ListOfficeName'], 
								list_agent: "#{data['ListAgentFirstName']} #{data['ListAgentLastName']} #{data['ListAgentNameSuffix']}",
								lat: data['Latitude'].to_f,
								lon: data['Longitude'].to_f,
								mls_id: mls.id)

			#get an image
			image_data = Array.new
			client.get_object(:resource => :Property, :type => :Photo, :location => true, :id => "#{data['ListingKey']}:1:2:3:4:5:6:7:8:9:10") do |object|
				image_data << object if object
				if image_data.any?
					listing.update(photo1: image_data.first['location'])
				end
			end
		end
	end
end