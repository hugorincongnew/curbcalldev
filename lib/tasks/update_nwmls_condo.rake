task update_nwmls_condo: :environment do
	@updates = 0
	@deletes = 0
	@new_listings = 0

	#time_then = Mls.find(5).update_time.strftime('%FT%R')
	time_then = Time.now - 1.hours
	@time = Time.now.strftime('%FT%R')
	listings = Nwmls::Listing.find(:property_type => "COND", :county => "King", :begin_date => time_then, :end_date => @time)
	if listings.count >= 1
		Mls.find(5).update(update_time: Time.now)
		listings.each do |x|
			listing = Listing.where(mls_num: x.listing_number)
			if listing.any?
				listing = listing.last
				if x.status == "Active" || x.status == "Contingent"
					# agent = Nwmls::Member.find(x.listing_agent_number)
					agent = ListAgent.where(mls_num:x.listing_agent_number).last
					if x.show_address_to_public == true
						listing.update(address: "#{x.house_number} #{x.street} #{x.street_suffix} #{x.unit}", 
									mls_num: "#{x.listing_number}", 
									city: "#{x.city}", 
									status: "#{x.status}",
									#remarks: "#{x.marketing_remarks}",
									subdivision: "#{x.community_name}",
									list_office: "#{agent.office_name}",
									list_agent: "#{agent.first_name} #{agent.last_name}",
									price: x.listing_price,
									beds: x.bedrooms,
									baths: x.bathrooms,
									mls_id: 5,
									lat: x.latitude,
									lon: x.longitude)
						@updates = @updates + 1
					end #if show_address_to_public == true
				else # if status isn't active or contingent we'll delete it from our db
					listing.destroy!
					@deletes = @deletes + 1
				end #if status == Active or contingent
			else # if listing doesn't exist yet
				if x.status == "Active" || x.status == "Contingent"

					# Get the pix
					result = Nwmls::Image.find(:listing_number => x.listing_number)
					count = result.count
					if count >= 1
						results = ImageQuery::retrieve_images("#{result.first.picture_file_name}")
						parser = Nori.new
						parsed = parser.parse(results) 
						parsed2 = parser.parse(parsed['soap:Envelope']['soap:Body']['RetrieveImagesResponse']['RetrieveImagesResult'])
						blob = parsed2['Results']['Images']['Image']['BLOB']
						#decode the blob
						sio = StringIO.new(Base64.decode64(blob))
						image = MiniMagick::Image.read(sio) #convert it to an image
						uploader = NwmlsPicUploader.new
						uploader.store!(image) #push to S3
						@image_url = uploader.url #return the location of the image
						#end getting the listing images

						#x.update(photo1: image_url)			
					else 
						p "No pix of this one"	
					end
					# end getting pix
					
					agent = ListAgent.where(mls_num:x.listing_agent_number).last
					Listing.create!(address: "#{x.house_number} #{x.street} #{x.street_suffix} #{x.unit}", 
							mls_num: "#{x.listing_number}", 
							city: "#{x.city}", 
							status: "#{x.status}",
							remarks: "#{x.marketing_remarks}",
							subdivision: "#{x.community_name}",
							list_office: "#{agent.office_name}",
							list_agent: "#{agent.first_name} #{agent.last_name}",
							price: x.listing_price,
							beds: x.bedrooms,
							baths: x.bathrooms,
							mls_id: 5,
							lat: x.latitude,
							lon: x.longitude,
							photo1: @image_url)
					@new_listings = @new_listings + 1
				end # if status is active or contingent 
			end # if listing.any?
		end # listings.each 
	end #if listings.count > 1
	p "#{@updates} Updated Condo Listings"
	p "#{@deletes} Deleted Condo Listings"
	p "#{@new_listings} New Condo Listings"
end