task update_crmls: :environment do
  
  price_query =  "90000-100000"
  import_query = "(ListPrice=#{price_query}),(Status=A)"
  ts=1.day.ago.strftime("%Y-%m-%d")
  update_query = "(TimestampModified=#{ts}+)"

  signin={:url => "https://rets.crmls.org/contact/rets/login", :username => "REALTYONE", :password => "glenIa6#E"}
  query={:search_type => :Property, :class => :Residential, :status => :Active, :query => update_query}

  options={signin: signin, query: query}
  
  UpdateCrmlsWorker.perform_async(options)
end