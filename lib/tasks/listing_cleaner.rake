task listing_cleaner: :environment do

  Listing.find_each do |listing|
    if(listing.status == Listing::STATES[:closed])
      #delete it from the DB
      listing.destroy
      #log it
    end
  end
	
end