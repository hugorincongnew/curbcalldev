task get_nwmls_agents: :environment do
 	agent = Nwmls::Member.find()
 	agent.each do |x|
 		ListAgent.create!(mls_num:x.member_mlsid, first_name:x.first_name, last_name:x.last_name, office_code:x.office_mlsid, office_name:x.office_name)
 	end
end