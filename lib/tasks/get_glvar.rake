task get_listings: :environment do
	include Geokit::Geocoders
	
	# client = RETS::Client.login(:url => "http://glvar.apps.retsiq.com/rets/login", :username => "curbcall", :password => "glvar4515")
	# client.search(:search_type => :Property, :class => 1, :query => "(144=990000-1000000)") do |data|
	# 	if data['242'] == "Active-Exclusive Right"
	# 		listing = Listing.create(address: "#{@data['244']} #{@data['243']}", mls_num: @data['sysid'], city: "@data['2909']", 
	# 				price: @data['144'], beds: @data['68'], baths: @data['61'], status: @data['242'], photo1: @data[''])
	# 	end
	# end





	require 'rets'
	login_url = "http://glvar.apps.retsiq.com/rets/login"

	# Pass the :login_url, :username, :password and :version of RETS
	client = Rets::Client.new({
	  login_url: login_url,
	  username: 'curbcall',
	  password: 'glvar4515'
	})

	begin
	  client.login
	rescue => e
	  puts 'Error: ' + e.message
	  exit!
	end

	puts 'We connected!'

	p "Looking for 0 = 99k"
	property = client.find(:all,:search_type => 'Property',:class => '1', :status => 'A', :query => '(144=1-98999),(242=ER)')
	p "#{property.count} records"
	property.each do |x|
		unless Listing.exists?(mls_num: x['sysid'])
			photos = client.objects '*', {
				resource: 'Property',
				object_type: 'Photo',
				resource_id: x['sysid']
			}
			tmp = Tempfile.new(photo.filename)
			uploader = GlvarPicUploader.new
			uploader.store!(tmp)
      url = uploader.url

			Listing.create!(address: "#{x['244']} #{x['243']}", mls_num: x['sysid'], city: x['2909'], 
	 				price: x['144'], beds: x['68'], baths: x['61'], status: x['242'], photo1: url, mls_id: 4)
		end
	end

	p "Looking for 99k = 250k"
	property1 = client.find(:all,:search_type => 'Property',:class => '1', :status => 'A', :query => '(144=99000-250000),(242=ER)')
	p "#{property1.count} records"
	property1.each do |x|
		unless Listing.exists?(mls_num: x['sysid'])
			photos = client.objects '*', {
				resource: 'Property',
				object_type: 'Photo',
				resource_id: x['sysid']
			}
			Listing.create!(address: "#{x['244']} #{x['243']}", mls_num: x['sysid'], city: x['2909'], 
	 				price: x['144'], beds: x['68'], baths: x['61'], status: x['242'], photo1: x[''], mls_id: 4)
		end
	end

	p "Looking for 500k = 1m"
	property2 = client.find(:all,:search_type => 'Property',:class => '1', :status => 'A', :query => '(144=5000001-1000000),(242=ER)')
	p "#{property2.count} records"
	property2.each do |x|
		unless Listing.exists?(mls_num: x['sysid'])
			photos = client.objects '*', {
				resource: 'Property',
				object_type: 'Photo',
				resource_id: x['sysid']
			}
			Listing.create!(address: "#{x['244']} #{x['243']}", mls_num: x['sysid'], city: x['2909'], 
	 				price: x['144'], beds: x['68'], baths: x['61'], status: x['242'], photo1: x[''], mls_id: 4)
		end
	end

	p "Looking for 1m = 2m"
	property3 = client.find(:all,:search_type => 'Property',:class => '1', :status => 'A', :query => '(144=1000001-2000000),(242=ER)')
	p "#{property3.count} records"
	property3.each do |x|
		unless Listing.exists?(mls_num: x['sysid'])
			photos = client.objects '*', {
				resource: 'Property',
				object_type: 'Photo',
				resource_id: x['sysid']
			}
			Listing.create!(address: "#{x['244']} #{x['243']}", mls_num: x['sysid'], city: x['2909'], 
	 				price: x['144'], beds: x['68'], baths: x['61'], status: x['242'], photo1: x[''], mls_id: 4)
		end
	end

	p "Looking for 2m+ "
	property3 = client.find(:all,:search_type => 'Property',:class => '1', :status => 'A', :query => '(144=2000001-100000000),(242=ER)')
	p "#{property3.count} records"
	property3.each do |x|
		unless Listing.exists?(mls_num: x['sysid'])
			photos = client.objects '*', {
				resource: 'Property',
				object_type: 'Photo',
				resource_id: x['sysid']
			}
			Listing.create!(address: "#{x['244']} #{x['243']}", mls_num: x['sysid'], city: x['2909'], 
	 				price: x['144'], beds: x['68'], baths: x['61'], status: x['242'], photo1: x[''], mls_id: 4)
		end
	end

	location = GoogleGeocoder.geocode('3568 Front St, San Diego, CA  92103')
end	