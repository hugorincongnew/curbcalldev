task clear_leads: :environment do
	Lead.all.each do |x|
		minutes_old = ((Time.now - x.created_at) / 1.minute).to_i
		if minutes_old > 10 #if the lead is more than 10 minutes old
			x.destroy!
		end
	end
end