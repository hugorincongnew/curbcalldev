task get_nwmls_images: :environment do
	Listing.where(mls_id: 5).each do |listing|
		#get the image of the house:

		result = Nwmls::Image.find(:listing_number => listing.mls_num)
		count = result.count
		if count >= 1
			results = ImageQuery::retrieve_images("#{result.first.picture_file_name}")
			parser = Nori.new
			parsed = parser.parse(results) 
			parsed2 = parser.parse(parsed['soap:Envelope']['soap:Body']['RetrieveImagesResponse']['RetrieveImagesResult'])
			blob = parsed2['Results']['Images']['Image']['BLOB']
			#decode the blob
			sio = StringIO.new(Base64.decode64(blob))
			image = MiniMagick::Image.read(sio) #convert it to an image
			uploader = NwmlsPicUploader.new
			uploader.store!(image) #push to S3
			image_url = uploader.url #return the location of the image
			#end getting the listing images

			listing.update(photo1: image_url)			
		else 
			p "No pix of this one"	
		end
		#name = parsed2['Results']['Images']['Image']['ImageId']

	end
end