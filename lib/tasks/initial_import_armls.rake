task initial_import_armls: :environment do
  
  #break it up into 6 threads or jobs w/ sidekiq
  #the 2x dyno can support up to 8 threads it says...
  jobs = %w(90000-100000 100001-250000 250001-500000 500001-750000 750001-875000 875001-1000000 1000001-100000000)
  
  jobs.each do |price_query|  
    
    import_query = "(LIST_22=#{price_query}),(LIST_15=OV61GOJ13C0)"

    proxy_url = "http://quotaguard1413:985a5b3fa8ac@us-east-1-static-brooks.quotaguard.com:9293"
    signin = {:url => "http://retsgw.flexmls.com:6103/rets2_1/Login", :username => "az.rets.reog08g", :password => "spart-tropy91",
      http_proxy: proxy_url}
    query = {:search_type => :Property, :class => "A", :query => import_query}

    options={signin: signin, query: query}
  
    p "starting sidekiq thread... #{import_query}"
    ArmlsUpsertWorker.perform_async(options)
  end
end