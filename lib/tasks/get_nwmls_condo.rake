task get_nwmls_condo: :environment do
	@new_listings = 0
	@time = Time.now
	listings = Nwmls::Listing.find(:property_type => "COND", :status => "A", :county => "King")
	if listings.count >= 1
		Mls.find(5).update(update_time: Time.now)

		listings.each do |x|
			#office = Nwmls::Office.find(x.listing_office_number)
			# agent = Nwmls::Member.find(x.listing_agent_number)
			agent = ListAgent.where(mls_num:x.listing_agent_number).last
			unless agent.nil?
				if x.show_address_to_public == true
					Listing.create!(address: "#{x.house_number} #{x.street} #{x.street_suffix} #{x.unit}", 
								mls_num: "#{x.listing_number}", 
								city: "#{x.city}", 
								status: "#{x.status}",
								#remarks: "#{x.marketing_remarks}",
								subdivision: "#{x.community_name}",
								list_office: "#{agent.office_name}",
								list_agent: "#{agent.first_name} #{agent.last_name}",
								#list_agent: x.listing_agent_number,
								price: x.listing_price,
								beds: x.bedrooms,
								baths: x.bathrooms,
								mls_id: 5,
								lat: x.latitude,
								lon: x.longitude)
					@new_listings = @new_listings + 1
				end # show_address_to_public
			end #agent.nil?
		end
	end #if listings.count > 1
	p "#{@new_listings} New Condo Listings"
end