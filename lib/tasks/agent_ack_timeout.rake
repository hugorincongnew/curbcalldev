task agent_ack_timeout: :environment do

  showings = Showing.where(agent_state: Showing::STATES[:warning])
  showings.each do |showing|
    SafetyTimeoutWorker.perform_async(showing.id)
  end
  
  
end