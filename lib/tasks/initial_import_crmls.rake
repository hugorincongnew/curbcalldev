task initial_import_crmls: :environment do
  
  #break it up into 6 threads or jobs w/ sidekiq
  #the 2x dyno can support up to 8 threads it says...
  jobs = %w(90000-100000 100001-250000 250001-500000 500001-750000 750001-875000 875001-1000000 1000001-100000000)
  
  jobs.each do |price_query|  
    import_query = "(ListPrice=#{price_query}),(Status=A)"

    signin = {:url => "https://rets.crmls.org/contact/rets/login", :username => "REALTYONE", :password => "glenIa6#E"}
    query =  {:search_type => :Property, :class => :Residential, :status => :Active, :query => import_query}

    options={signin: signin, query: query}
  
    p "starting sidekiq thread... #{import_query}"
    UpdateCrmlsWorker.perform_async(options)
  end
end