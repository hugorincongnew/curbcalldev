task initial_import_sandicor: :environment do
  
  #break it up into 6 threads or jobs w/ sidekiq
  #the 2x dyno can support up to 8 threads it says...
  jobs = %w(90000-100000 100001-250000 250001-500000 500001-750000 750001-875000 875001-1000000 1000001-100000000)
  
  jobs.each do |price_query|  
    #can't get sandicor to obey status filter, so just importing al
    import_query = "(L_AskingPrice=#{price_query}),(L_UpdateDate=1950-01-01T00:00:00+)"

    signin = {:url => "https://rets-paragon.sandicor.com/rets/fnisrets.aspx/sandicor/login?rets-version=rets/1.7.2", 
        :username => "500616", :password => "@bhuxvs2"}
    query =  {:search_type => :Property, :class => "RE_1", :query => import_query}

    options={signin: signin, query: query}
  
    p "starting sidekiq thread... #{import_query}"
    SandicorUpsertWorker.perform_async(options)
  end
end