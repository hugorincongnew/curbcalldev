task initial_import_glvar: :environment do
  
  #break it up into 6 threads or jobs w/ sidekiq
  #the 2x dyno can support up to 8 threads it says...
  jobs = %w(90000-100000 100001-250000 250001-500000 500001-750000 750001-875000 875001-1000000 1000001-100000000)
  
  jobs.each do |price_query|  
    import_query = "(144=#{price_query}),(242=ER)"

    signin = {:url => "http://glvar.apps.retsiq.com/rets/login", :username => "curbcall", :password => "glvar4515"}
    query =  {:search_type => :Property, :class => "1", :status => :Active, :query => import_query}

    options={signin: signin, query: query}
  
    p "starting sidekiq thread... #{import_query}"
    GlvarUpsertWorker.perform_async(options)
  end
end