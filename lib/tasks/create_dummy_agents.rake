task create_agents: :environment do

	Agent.create!(:name => "Derek Jeter", :lat => "33.207059", :lon => "-117.37793", :phone => "567-999-2222", :company => "Century 21",
					:user_id => 2, :email => "derek@yankees.com", :bio => "Jason Thomas has completed the Seniors Real Estate Specialist(SRES®) Designation Course. 
					A Seniors Real Estate Specialist® understands the demands of the mature market. These Specialist have a wealth of knowledge on reverse mortgages, 
					real estate planning, 1031 exchanges, and housing options and have been educated on all aspects of 50+ buying and selling.")

	Agent.create!(:name => "Mariano Rivera", :lat => "32.743171", :lon => "-117.132111", :phone => "567-999-2222", :company => "Century 21",
					:user_id => 2, :email => "moe@yankees.com", :bio => "Jason Thomas has completed the Seniors Real Estate Specialist(SRES®) Designation Course. 
					A Seniors Real Estate Specialist® understands the demands of the mature market. These Specialist have a wealth of knowledge on reverse mortgages, 
					real estate planning, 1031 exchanges, and housing options and have been educated on all aspects of 50+ buying and selling.")

	Agent.create!(:name => "Andy Pettite", :lat => "32.762517", :lon => "-117.089195", :phone => "567-999-2222", :company => "Keller Williams",
					:user_id => 2, :email => "moe@yankees.com", :bio => "Jason Thomas has completed the Seniors Real Estate Specialist(SRES®) Designation Course. 
					A Seniors Real Estate Specialist® understands the demands of the mature market. These Specialist have a wealth of knowledge on reverse mortgages, 
					real estate planning, 1031 exchanges, and housing options and have been educated on all aspects of 50+ buying and selling.")

	Agent.create!(:name => "Robby Cano", :lat => "32.729309", :lon => "-117.16301", :phone => "567-999-2222", :company => "Prudential California",
					:user_id => 2, :email => "moe@yankees.com", :bio => "Jason Thomas has completed the Seniors Real Estate Specialist(SRES®) Designation Course. 
					A Seniors Real Estate Specialist® understands the demands of the mature market. These Specialist have a wealth of knowledge on reverse mortgages, 
					real estate planning, 1031 exchanges, and housing options and have been educated on all aspects of 50+ buying and selling.")

	
end