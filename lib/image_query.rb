module ImageQuery
  extend self
 
  mattr_accessor :user do
    'hoverboard'
  end
 
  mattr_accessor :pass do
    'cP6cHias'
  end
 
  mattr_accessor :endpoint do
    'http://images.idx.nwmls.com/imageservice/imagequery.asmx?WSDL'
  end
 
  def retrieve_images(listing_number)
    xml = Builder::XmlMarkup.new
    xml.instruct!
    request = xml.soap :Envelope, 'xmlns:xsi' => "http://www.w3.org/2001/XMLSchema-instance", 'xmlns:xsd' => "http://www.w3.org/2001/XMLSchema", 'xmlns:soap' => "http://schemas.xmlsoap.org/soap/envelope/" do
      xml.soap :Body do
        xml.RetrieveImages(xmlns: "NWMLS:EverNet") do
          xml.query image_query(listing_number)
        end
      end
    end
 
    send_request(request)
  end
 
  def image_query(listing_number)
    xml = Builder::XmlMarkup.new
    xml.ImageQuery xmlns: "NWMLS:EverNet:ImageQuery:1.0" do
      xml.Auth do
        xml.UserId user
        xml.Password pass
      end
 
      xml.Query do
        #xml.ByListingNumber listing_number
        xml.ByName listing_number
      end
 
      xml.Results do
        xml.Schema "NWMLS:EverNet:ImageData:1.0"
      end
    end
  end
 
  def send_request(xml)
    RestClient.post endpoint, xml, 'content-type' => 'text/xml; charset=utf-8'
  end
end