#spec/helpers/controller_spec_helpers.rb
module AuthHelpers
  def authWithApp token
    # curl -IH "Authorization: Token token=785f4ddb1f372bf57d87f2fe1128d5f40f31451a" http://localhost:5000/api/v3/agents/find
    headers={}
    headers['HTTP_AUTHORIZATION'] = ActionController::HttpAuthentication::Token.encode_credentials(token)
    headers.merge!('HTTP_ACCEPT'=> "application/json")
    headers.merge!('CONTENT_TYPE' => "application/json")
    headers
  end
end


# curl -vH "Authorization: Token token=785f4ddb1f372bf57d87f2fe1128d5f40f31451a" -H "Content-Type: application/json" -d '{"lat":"32.7449015","lng":"-117.1323579","radius":"5"}' http://localhost:5000/api/v3/showings
#
# curl -vH "Authorization: Token token=785f4ddb1f372bf57d87f2fe1128d5f40f31451a" http://localhost:5000/api/v3/showings/8364