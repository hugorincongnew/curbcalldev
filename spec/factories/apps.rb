FactoryGirl.define do
  
  sequence :access_token do |n|
    "asdf#{n}"
  end
  
  
  factory :app do
    name "MyString"
    description "MyString"
    website "MyString"
    organization "MyString"
    client_id "MyString"
    client_secret "MyString"
    
    factory :app_with_access_token do 
      access_token
    end
  end

end
