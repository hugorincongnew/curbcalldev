FactoryGirl.define do
  # Defines a new sequence
  sequence :email do |n|
    "developer#{n}@domain.com"
  end
  
  sequence :api_key do |n|
    "asdf#{n}"
  end
  
  
  factory :developer do
    email
    
    factory :developer_with_api_key do 
      api_key
    end
  end

end
