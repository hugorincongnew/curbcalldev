# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :showing, :class => 'Showing' do
    buyer_id "buyer_id"
    lat 32.7155887
    lng -117.1609563
    lead
    hood "string"
    address "string?"
    buyer_name "string name"
    agent_name "agent name"
    mls_num "msl string"
    showing_owner { |a| a.association(:user) }

    factory :active_showing do
    end

    factory :showing_w_wait_state do
      status "waiting"
    end

    factory :showing_with_sightings do
      ignore do
        sightings_count 5
      end

      after(:create) do |showing, evaluator|
        create_list(:sighting, evaluator.sightings_count, showing: showing)
      end
    end

  end
end
