# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :lead do
    association :buyer, :factory => :user
    agent
    listing
  end
end
