# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :eligible_agent do
    lead
    agent_user_id class: "User" #is this true?
  end
end