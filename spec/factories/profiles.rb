# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :profile, :class => 'Profile' do
    bio "bio text"
    user
    profile_pic "https://graph.facebook.com/uid123/picture?type=large&width=640"
  end
end