# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  sequence :authentication_token do |n|
    "asdf#{n}"
  end
  
  

  
  factory :user do
    sequence(:email) { |n| "buyer#{n}@curbcallapp.com" }      
    name "User Name"
    bio "bio"
    phone "818259344"
    friendly 2.0
    local_expert 3.0
    agent true
    admin true
    office_admin false
    password "password"
    
    association :brokerage
    
    factory :user_with_authentication_token do 
      authentication_token
    end
    
    factory :user_with_profile do
      after(:create) do |user, evaluator|
        create_list(:profile, 1, user: user)
      end
    end
    
    factory :user_with_showings do
      ignore do
        showings_count 5
      end

      after(:create) do |user, evaluator|
        create_list(:showing_with_sightings, evaluator.showings_count, showing_owner: user)
      end
    end
    
    factory :user_from_oauth do
      after(:create) do |user, evaluator|
        create_list(:identity, 1, user: user)
        create_list(:profile, 1, user: user)
      end
    end
    
  end
end