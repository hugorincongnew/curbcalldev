FactoryGirl.define do
  factory :identity, :class => 'Identity' do
    user
    provider "facebook"
    uid "uid123"
  end

end
