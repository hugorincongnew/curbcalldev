# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :mls, :class => 'Mls' do
    name "mls name"
    username "u"
    password "p"
    login_link "asfd"
    retsly false
    
    factory :mls_w_zips do
        after(:create) do |mls|
          zip1 = create(:zip)
          zip2 = create(:zip)
          mls.zips = [zip1, zip2]
          
          brokerage1 = create(:brokerage)
          brokerage2 = create(:brokerage)
          mls.brokerages = [brokerage1, brokerage2]
        end
      end
      
  end
end