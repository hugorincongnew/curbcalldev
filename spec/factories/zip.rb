# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :zip, :class => 'Zip' do
    code 92104
  end
end