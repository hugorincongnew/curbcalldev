# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :agent do
    name "Agent Name"
    lat 32.7155887
    lon -117.1609563
    phone "8182539344"
    company "Qualcomm"
    user
    sequence(:email) { |n| "agent#{n}@curbcallapp.com" }
    showing_rating 2.0
    showing_feedback_count 2
    status "status"
    mls
    
    association :brokerage
    
    factory :agent2 do
      name "Alternate Agent"
    end
  end
end