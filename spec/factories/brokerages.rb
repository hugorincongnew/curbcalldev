# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :brokerage do
    name "brokerage name"

    factory :brokerage_w_mlss do
      after(:create) do |brokerage|
        mls = create(:mls)
        mls2 = create(:mls)
        brokerage.mls = [mls, mls2]
      end
    end
      
    factory :brokerage_w_users do
      ignore do
        users_count 5
      end

      after(:create) do |brokerage, evaluator|
        create_list(:user_with_showings, evaluator.users_count, brokerage: brokerage)
      end
    end
  end
end
