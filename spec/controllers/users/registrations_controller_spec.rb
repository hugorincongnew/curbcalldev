require 'rails_helper'

RSpec.describe Users::RegistrationsController, :type => :controller do

	before(:each) do
      @request.env["devise.mapping"] = Devise.mappings[:user]
      @user = {password: "123456789", password_confirmation: "123456789" }
    end

	it "New User with email param" do
		@user[:email] = "user@mail.com"
		post :create, {user: @user, format: :json}
		expect(response).to have_http_status(:success)
	end
	
	it "New User with email_address param" do
		@user[:email_address] = "user@mail.com"
		post :create, {user: @user, format: :json}
		expect(response).to have_http_status(:success)
	end
end