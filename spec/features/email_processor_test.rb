require 'rails_helper'

RSpec.feature 'Viewer sends comment to Griddler Demo', :type => :request do
  scenario 'it shows up on the home page' do
    message = 'Joel was here'
    brokerage = create(:brokerage)

    post '/email_processor', fake_sendgrid_params(message, brokerage)

    assert_equal 200, status
    response.code.should eq "200"
  end

  def fake_sendgrid_params(message, brokerage)
    {
      to: 'test <'+brokerage.lead_email+'>',
      from: 'Joel Oliveira <rafael@softwarecriollo.com>',
      text: message
    }
  end
end
