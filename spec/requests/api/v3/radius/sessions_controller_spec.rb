require 'rails_helper'

describe "radius sessions_api", :type => :request  do

  context "traditional user/pass sign-in" do 
    it "should login and return session info" do
      user = create(:radius_user)

      attribs={api_v3_radius_radius_user:{email: user.email, password: "password" }}
      headers = {'CONTENT_TYPE' => "application/json", 'ACCEPT' => 'application/json'}
      post(api_v3_radius_radius_user_session_path, attribs.to_json, headers)
      expect(response.status).to eq(201)
      jresp = JSON.load(response.body)
      p api_v3_radius_radius_user_session_url(:json)
      p jresp
      expect(jresp["email"]).to eq(user.email)
    end

  end
end