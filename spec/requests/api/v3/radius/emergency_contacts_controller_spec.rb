require 'rails_helper'

describe "emergency contacts controller apis", :type => :request  do

    before do
      @headers = authWithApp create(:app_with_access_token).access_token
      @radius_user = create(:radius_user)
      @radius_user.emergency_contacts.build(name: "Cassee Egg", email: "egg@egg.com")
      @radius_user.save
    end
  
    it "can get emergency contacts for radius user" do
      get api_v3_radius_user_emergency_contacts_path(@radius_user), {}, @headers
      expect(response.status).to eq(200)
      jresp = JSON.load(response.body)
      p jresp
      expect(jresp[0]["email"]).to eq(@radius_user.emergency_contacts[0].email)
    end
    
    it "can add a contact" do
      params={emergency_contact: {name: "Tom Cruise", email: "tom@cc.com"}}
      post api_v3_radius_user_emergency_contacts_path(@radius_user), params.to_json, @headers
      expect(response.status).to eq(201)
      jresp = JSON.load(response.body)
      p jresp
      expect(jresp["name"]).to eq("Tom Cruise")
    end
    
    it "can delete contact" do
      before_count = @radius_user.emergency_contacts.count
      params = {}
      delete api_v3_radius_user_emergency_contact_path(@radius_user, @radius_user.emergency_contacts.last), params.to_json, @headers
      expect(response.status).to eq(204)
      expect(@radius_user.reload.emergency_contacts.count).to eq(before_count-1)
    end

end