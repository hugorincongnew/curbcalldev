require 'rails_helper'

describe "Braintree Controller APIs", :type => :request  do

    before do
      @headers = authWithApp create(:app_with_access_token).access_token
    end
    
    describe "/client_token" do
      it "generate token" do
        get api_v3_radius_client_token_path, {}, @headers
        expect(response.status).to be(200)
      end
    end
    
    describe "/subscriptions" do
      before do
        @radius_user = create(:radius_user)
      end
      it "should create a subscription" do

        params = {radius_user_id: @radius_user.id, payment_method_nonce: Braintree::Test::Nonce::Transactable}
        post api_v3_radius_subscriptions_path, params.to_json, @headers
        expect(response.status).to be(201)
      end
    end


end