require 'rails_helper'

describe "radius registrations_api", :type => :request  do

  context "traditional user/pass signup" do 
    
    #TAKE TIPS FROM HERE
    # http://blog.andrewray.me/how-to-set-up-devise-ajax-authentication-with-rails-4-0/
    #TO FINISH UP THIS STORY
    it "should create user object from user/pass" do
      before_count = RadiusUser.count
      attribs={api_v3_radius_radius_user:{email:"eggie7@gmail.com", password:"thereandbackagain", password_confirmation:"thereandbackagain"}}
      headers = {'CONTENT_TYPE' => "application/json", 'ACCEPT' => 'application/json'}
      post api_v3_radius_radius_user_registration_path(:json), attribs.to_json, headers
      expect(response.status).to eq(201)
      
      expect(RadiusUser.count).to eq(before_count+1)
    end
  end
  
  

end