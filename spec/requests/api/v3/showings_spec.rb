require 'rails_helper'

RSpec.describe "Showings API", :type => :request do
   
  describe "AdHoc Showing", type: :request do
    before do
      @headers = authWithApp create(:app_with_access_token).access_token
    end
    
    it "should create a ad-hoc showing w/ a radius user" do
      before_count = Showing.count
      user = create(:radius_user)
      
      params = { showing_owner_type: "RadiusUser", showing_owner_id: user.id, status: "started" }
      post api_v3_showings_path(:json), params.to_json, @headers
      expect(response.status).to eq(201)
      expect(Showing.count).to eq(before_count+1)
      p jresp = JSON.load(response.body)
      expect(jresp["showing_owner_id"]).to eq(user.id)
      id = jresp['id']
      showing = Showing.find(id)
      # expect(showing.agent).to eq(user.id)
      expect(showing.status).to eq("started")
      
      #now add an emergency contact using the API
      params={emergency_contact: {name: "Tom Cruise", email: "tom@cc.com"}}
      post api_v3_radius_user_emergency_contacts_path(user), params.to_json, @headers
      expect(response.status).to eq(201)
      
      #now trigger a panic and confirm the emergency contact gets the message
      params={ showing:{agent_state: Showing::STATES[:danger]} }
      put api_v3_showing_path(showing), params.to_json, @headers
      expect(showing.reload.agent_state).to eq(Showing::STATES[:danger])
      
    end
    
    it "should support ad-hoc showings" do
      before_count = Showing.count
      user = create(:user_with_profile)
      
      #create an ADHOC showing by hard-coding the state to "started"
      #this ios radius client only posts agent id and agent name
      params = { agent_id: user.id, agent_name: "AdHoc Name", status: "started" }
      post api_v3_showings_path(:json), params.to_json, @headers
      expect(response.status).to eq(201)
      expect(Showing.count).to eq(before_count+1)
      jresp = JSON.load(response.body)
      id = jresp['id']
      showing = Showing.find(id)
      expect(showing.status).to eq("started")
    end
  end
  
  describe "POST /v3/showings" do
    before do
      @brokerage = create(:brokerage)
      @headers = authWithApp @brokerage.access_token
      @agent = create(:agent, brokerage: @brokerage) # he is in DT SD
      @params = {lat: 32.74490212, lng: -117.13775593, radius: 5}
    end
    
    it "should start showing w/ auto agent" do 
      post api_v3_showings_path, @params.to_json, @headers
      expect(response.status).to be(201)
    end
    
    it "should queue a 2 min. timeout when showing created" do
      post api_v3_showings_path, @params.to_json, @headers
      
      showing = Showing.last
      jid = showing.dispatcher.lookup_notify_job
      expect(jid.length).to eq(24)
    end
    
    it "should start showing w/ given agent" do 
      @agent = create(:agent2) # he is in DT SD
      params = {lat: 32.74490212, lng: -117.13775593, radius: 5, agent_id: @agent.id}
      post api_v3_showings_path, params.to_json, @headers
      expect(response.status).to be(201)
      showing = JSON.load(response.body)
      #now post to /claim for an agent to claim the lead
      claim_params = {agent_id: @agent.id}
      post claim_api_v3_showing_path(showing["id"]), claim_params.to_json, @headers
      showing = JSON.load(response.body)
      expect(showing["agent_name"]).to eq("Alternate Agent")
    end

  end
  
  # this test what the dev. client will poll
  describe "GET /v3/showings/:id" do
    before do
      @brokerage = create(:brokerage)
      @headers = authWithApp @brokerage.access_token

      #make some agents go online
      @agent = create(:agent, brokerage: @brokerage) # he is in DT SD
    end
    it "should find available agents" do
      ref = create(:showing_w_wait_state)
      get api_v3_showing_path(ref), {}, @headers
      expect(response.status).to be(200)
      showing = JSON.load(response.body)
      expect(showing["id"]).to eq(ref.id)
      expect(showing["status"]).to eq(ref.status)
    end
    
    it "should return the timeout status when no agents accept" do
      ref = create(:showing_w_wait_state)
      get api_v3_showing_path(ref), {}, @headers
      expect(response.status).to be(200)
      showing = JSON.load(response.body)
      expect(showing["id"]).to eq(ref.id)
      expect(showing["status"]).to eq(ref.status)
      
      #now timeout the showing, poll and assert the timeoue
      showing = Showing.find(showing["id"])
      showing.timeout_search
      
      get api_v3_showing_path(ref), {}, @headers
      expect(response.status).to be(200)
      showing = JSON.load(response.body)
      expect(showing["id"]).to eq(ref.id)
      expect(showing["status"]).to eq(Showing::SHOWING_STATES.fetch(:timeout))
      
    end
    
    it "should show a 'transit' status" do
      ref = create(:showing_w_wait_state)
      get api_v3_showing_path(ref), {}, @headers
      expect(response.status).to be(200)
      showing = JSON.load(response.body)
      showing = Showing.find(showing["id"])


      #now post to /claim for an agent to claim the lead
      #find an agent to claim the lead
      @claim_agent = create(:agent)
      claim_params = {agent_id: @claim_agent.id}
      post claim_api_v3_showing_path(ref), claim_params.to_json, @headers
      
      
      #poll and check status
      get api_v3_showing_path(ref), {}, @headers
      expect(response.status).to be(200)
      showing = JSON.load(response.body)
      expect(showing["id"]).to eq(ref.id)
      expect(showing["status"]).to eq(Showing::SHOWING_STATES.fetch(:transit))
      
      
    end

  end
  
  context "a showing that is waiting for an agent to attach" do
    before do
      @headers = authWithApp create(:brokerage).access_token
      #make some agents go online
      @agent = create(:agent) # he is in DT SD
      
      ref = create(:showing_w_wait_state)
      get api_v3_showing_path(ref), {}, @headers
      expect(response.status).to be(200)
      showing = JSON.load(response.body)
      @showing = Showing.find(showing["id"])
      expect(@showing.status).to eq("waiting")
    end
    it "it should attach agent and start showing" do 
      params = {agent_id: @agent.id}
      post claim_api_v3_showing_path, params.to_json, @headers
      expect(Showing.last.status).to eq(Showing::SHOWING_STATES.fetch(:transit))
      
      post start_api_v3_showing_path, params.to_json, @headers
      expect(Showing.last.status).to eq(Showing::SHOWING_STATES.fetch(:started))
      
      #now end it
      post end_api_v3_showing_path, params.to_json, @headers
      expect(Showing.last.status).to eq(Showing::SHOWING_STATES.fetch(:completed))
    end
  end
  
  describe "PUT showing", type: :request do
    before do
      @headers = authWithApp create(:brokerage).access_token
    end
    it "should update an exisiting showing" do
       showing = create(:showing)
       params = { showing:{ agent_state: Showing::STATES[:warning] }}
       put api_v3_showing_path(showing), params.to_json, @headers
       expect(response.status).to eq(200)
       showing_after = Showing.find(showing.id)
       expect(showing_after.agent_state).to eq(Showing::STATES[:warning])

       #now esclate it to danger
       params={ showing:{agent_state: Showing::STATES[:danger]} }
       put api_v3_showing_path(showing), params.to_json, @headers
       showing_after = Showing.find(showing.id)
       expect(showing_after.agent_state).to eq(Showing::STATES[:danger])
    end
  end
  
  context "sandbox request" do
    before do
      @headers = authWithApp create(:app_with_access_token).sandbox_access_token
    end
    
    it "should return showing w/ an attached agent" do
      params = {lat: 32.74490212, lng: -117.13775593, radius: 5}
      post api_v3_showings_path, params.to_json, @headers
      expect(response.status).to be(201)
      showing = JSON.load(response.body)
      expect(showing["agent_name"]).to eq("Sandbox Agent")
    end
  end
  
  context "find agent showings" do
  
  	before do
  	  @brokerage = create(:brokerage)
      @headers = authWithApp @brokerage.access_token
      @agent = create(:agent, brokerage: @brokerage)
    end
    
    it "returns the eligible showing with showing_owner field not empty" do
    	showings = create_list(:showing_w_wait_state, 5, showing_owner: @agent.user)
    	puts api_v3_agent_showings_path(@agent)
    	
    	get api_v3_agent_showings_path(@agent), {}, @headers
    	expect(response).to have_http_status(:success)
    	showing_id = JSON.parse(response.body)['id']
    	showing = Showing.find_by id: showing_id
    	expect(showings).to include(showing)
    end
    
    it "returns the eligible showing with showing_owner field empty" do
    	showings = create_list(:showing_w_wait_state, 5)
    	puts api_v3_agent_showings_path(@agent)
    	
    	get api_v3_agent_showings_path(@agent), {}, @headers
    	expect(response).to have_http_status(:success)
    	showing_id = JSON.parse(response.body)['id']
    	showing = Showing.find_by id: showing_id
    	expect(showings).to include(showing)
    end
    
    it "returns not find any showings" do
    	puts api_v3_agent_showings_path(@agent)
    	
    	get api_v3_agent_showings_path(@agent), {}, @headers
    	expect(response).to have_http_status(:unprocessable_entity)
    end
    
    it "not found agent" do
    	path = api_v3_agent_showings_path(@agent)
    	puts path
    	@agent.destroy
    	get path, {}, @headers
    	expect(response).to have_http_status(404)
    	expect(JSON.parse(response.body)['data']['message']).to eq("The agent not found")
    end
  
  end


end
