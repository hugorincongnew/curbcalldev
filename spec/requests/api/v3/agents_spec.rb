require 'rails_helper'

RSpec.describe "Agents API", :type => :request do
   
  let(:valid_params) { {lat: 32.74490212, lng: -117.13775593} }
  
  context "one available agent" do
    before do
      @headers = authWithApp create(:app_with_access_token).access_token
      
      #make some agents go online
      @agent = create(:agent) # he is in DT SD
    end
    it "should find available agents" do
      params = {lat: 32.74490212, lng: -117.13775593, radius:100}
      get api_v3_agents_find_path, params, @headers
      agents = JSON.load(response.body)
      expect(response.status).to be(200)
      expect(agents.class).to eq(Array)
      expect(agents.length).to eq(1)
      expect(agents.last["id"]).to eq(@agent.id)
    end
  end
  
  context "no agents available" do
    before do
      @headers = authWithApp create(:app_with_access_token).access_token
    end
    it "should handle error" do
      expect(Agent.count).to eq(0) # no agents available
      params = {lat: 32.74490212, lng: -117.13775593, radius:4}
      get api_v3_agents_find_path, params, @headers
      expect(response.status).to eq(409)
      error = JSON.load(response.body)
      puts error.to_json
      expect(error.class).to eq(Hash)
      expect(error['errors'].first).to eq("Could not find any agents")
      
    end
  end
  
  context "sandbox request" do
    before do
      @headers = authWithApp create(:app_with_access_token).sandbox_access_token
    end
    
    it "should return fake agent" do
      params = {lat: 32.74490212, lng: -117.13775593, radius:100}
      get api_v3_agents_find_path, params, @headers
      pending "not sure what to do w/ this yet"
      expect(response.status).to be(200)
      agents = JSON.load(response.body)
      expect(agents.length).to eq(1)
      expect(agents.first["name"]).to eq("Sandbox Agent")
    end
  end
  
  
  describe "Authentication" do
    before do
      @app = create(:app_with_access_token)
      @headers=authWithApp @app.access_token
      @agent = create(:agent)
    end
    
    it "has authentication" do
      get api_v3_agents_find_path, valid_params, @headers
      expect(response.status).to be(200)
    end
    
    it "should block requests w/o token" do
      @headers['HTTP_AUTHORIZATION']="invalid token"
      get api_v3_agents_find_path, valid_params, @headers
      expect(response.status).to be(401)
    end
  end



end
