require 'rails_helper'

RSpec.describe "Showings API", :type => :request do
  
  
   # {
   #   showing:
   #   {
   #     agent_id: "#{App::Persistence['user_id']}",
   #     lead_id: "#{lead_id}",
   #     agent_name: "#{App::Persistence['user_name']}",
   #     buyer_id: App::Persistence['lead_id'],
   #     buyer_name: App::Persistence['lead_name'],
   #     address: App::Persistence['address'],
   #     user_email: App::Persistence['user_email'],
   #     user_token: App::Persistence['authToken']
   #   }
   # }
  
   let(:valid_attributes) do
     user = create(:user_with_profile)
     lead = create(:lead)

     {
       showing_owner_id: user.id,
       showing_owner_type: "User",
       lead_id: lead.id,
       agent_name: "Fake Name",
       buyer_name: "Buyer Name",
       address: "address"
     }

   end
   


  let(:valid_session) do
    user=create(:user)
    {'X-User-Email' => "#{user.email}", 'X-User-Token' =>  "#{user.authentication_token}"}
  end
  
  describe "AdHoc Showing", type: :request do
    it "should support ad-hoc showings in v2 api" do
      before_count = Showing.count
      user = create(:user_with_profile)
      
      #create an ADHOC showing by hard-coding the state to "started"
      params = { showing_owner_type: 'RadiusUser', showing_owner_id: user.id, agent_name: "AdHoc Name", status: "started" }
      post api_v2_showings_path(:json), params, valid_session
      expect(response.status).to eq(201)
      expect(Showing.count).to eq(before_count+1)
      jresp = JSON.load(response.body)
      id = jresp['id']
      showing = Showing.find(id)
      expect(showing.status).to eq("started")
    end
  end

  describe "PUT showing", type: :request do
    it "should update an exisiting model" do
       profile = create(:showing)
       put api_v2_showing_path(profile), { agent_state: Showing::STATES[:warning] }, valid_session
       expect(response.status).to eq(200)

       showing_after = Showing.find(profile.id)
       expect(showing_after.agent_state).to eq(Showing::STATES[:warning])

       #now esclate it to danger
       put api_v2_showing_path(profile), { agent_state: Showing::STATES[:danger] }, valid_session
       showing_after = Showing.find(profile.id)
       expect(showing_after.agent_state).to eq(Showing::STATES[:danger])
    end

    it "should transition from warning to danger" do
      profile = create(:showing)
      expect(profile.agent_state).to eq(Showing::STATES[:safe])
      profile.syn_agent
      expect(profile.agent_state).to eq(Showing::STATES[:warning])
      profile.save!

      #now transition it from warning -> danger via API call
      put api_v2_showing_path(profile), { agent_state: Showing::STATES[:danger] }, valid_session
      showing_after = Showing.find(profile.id)
      expect(showing_after.agent_state).to eq(Showing::STATES[:danger])

      get api_v2_showing_path(profile), {}, valid_session
      jresp = JSON.load(response.body)
      expect(jresp['agent_state']).to eq(Showing::STATES[:danger])
    end
  end
  
  describe "showing states" do
    it "should transtion a showing to start when client posts" do
      before_count = Showing.count
      post api_v2_showings_path(:json), valid_attributes, valid_session
      expect(response.status).to eq(201)
      expect(Showing.count).to eq(before_count+1)
      jresp = JSON.load(response.body)
      id = jresp['id']
      showing = Showing.find(id)
      expect(showing.status).to eq("waiting")
      
    end
  end
  
  context "a normal showing" do
    before do
      @showing = create(:showing)
    end
    
    context "a user hits the panic button" do 
      it "should set the showing to panic state" do
        put api_v2_showing_path(@showing), { agent_state: Showing::STATES[:danger] }, valid_session
        expect(@showing.reload.agent_state).to eq(Showing::STATES[:danger])
      end
    end
    
    context "a user ends the showing " do 
      it "should transtion to completd and fire complete message" do
        @showing.attach_agent
        @showing.start
        put api_v2_showing_path(@showing), { status: Showing::SHOWING_STATES.fetch(:completed) }, valid_session
        expect(@showing.reload.status).to eq(Showing::SHOWING_STATES.fetch(:completed))
        #now make sure the event handler ran!
        expect(@showing.history.all.last[:event]).to eq(CurbCall::SafetyRadius::History::EVENTS[:completed])
      end
    end
    
    context "a user goes into panic mode" do
      before do
        @showing.attach_agent
        @showing.start
        expect(@showing.reload.agent_state).to eq(Showing::STATES.fetch(:safe))
        
        # do some heartbeats that say it's safe
        10.times do
          #normally I'd do this post api_v2_sightings_heartbeat_path, { showing_id: @showing.id, lat: 0, lon: 0 } , valid_session
          #but i dont have sidekiq in te test env, so just inlining the sidekiq routine here:
          sighting_params = { showing_id: @showing.id, lat: 0, lon: 0 }
          sighting = Sighting.create!(sighting_params)
          sighting.showing.perform_safety_analysis #this overwrites the panic to safe
        end
        
        
        put api_v2_showing_path(@showing), { agent_state: Showing::STATES[:danger] }, valid_session
        expect(@showing.reload.agent_state).to eq(Showing::STATES[:danger])
        
        CurbCall::Config[CurbCall::SafetyRadius::Base::SETTINGS.fetch(:safety_radius)] = 0.0008
        CurbCall::Config[CurbCall::SafetyRadius::Base::SETTINGS.fetch(:safety_threshold)] = 5
            
      end
      
      it "heartbeats should not transition it back to safe" do
        expect(@showing.reload.agent_state).to eq(Showing::STATES[:danger])
        expect(@showing.reload.status).to eq(Showing::SHOWING_STATES.fetch(:pending))
        
        #now it should still be danger
        expect(@showing.reload.agent_state).to eq(Showing::STATES[:danger])
         
      end
    end
  end
  

end
