require 'rails_helper'

describe "nearby_agents_api"  do

 
  let(:valid_attributes) do

    {
      lat: "32.747575",
      lon: "-117.1323579",
      zip: "92104"
    }

  end

  let(:valid_session) do
    user=create(:user)
    {'X-User-Email' => "#{user.email}", 'X-User-Token' =>  "#{user.authentication_token}"}
  end
  
  describe "GET index" , :type => :request do
    it "gets all nearby agents" do
      #login user
      zip = create(:zip)
      get api_v2_nearby_agents_path, valid_attributes, valid_session
      expect(response.status).to eq(200)
      json = JSON.load(response.body)
      p json
    end
  end
  
end