require 'rails_helper'

describe "registrations_api", :type => :request  do

  context "traditional user/pass signup" do 
    
    it "should create user object from user/pass" do
      before_count = User.count
      attribs={user:{email:"eggie7@gmail.com", password:"thereandbackagain", password_confirmation:"thereandbackagain", name: "Dick Cheney"}}
      headers = {'CONTENT_TYPE' => "application/json", 'ACCEPT' => 'application/json'}
      post(register_path, attribs.to_json, headers) 
      expect(response.status).to eq(200)
      
      expect(User.count).to eq(before_count+1)
    end
  end

  context "user signs up via FB oauth" do
    before do
      #stub
      h={"id"=>"uid123", "email"=>"eggie10@gmail.com", "first_name"=>"Alex", "gender"=>"male", "last_name"=>"Egg", "link"=>"",
         "locale"=>"en_US", "name"=>"Alex Egg", "timezone"=>-8, "updated_time"=>"2014-08-07T21:43:17+0000", 
         "verified"=>true}
      json="{\"id\":\"uid123\",\"email\":\"eggie10\\u0040gmail.com\",\"first_name\":\"Alex\",\"gender\":\"male\",\"last_name\":\"Egg\",\"link\":\"https:\\/\\/www.facebook.com\\/app_scoped_user_id\\/10152910932811474\\/\",\"locale\":\"en_US\",\"name\":\"Alex Egg\",\"timezone\":-8,\"updated_time\":\"2014-08-07T21:43:17+0000\",\"verified\":true}"

      CurbCall::Oauth.stub(:fb_api_get){json}
      # CurbCall::Oauth.stub(:get){h}
    end
    
    it "should create a new user" do
      before_count = User.count
      attribs= {user: { access_token:"accexxtokenx"}}
      headers = {'CONTENT_TYPE' => "application/json", 'ACCEPT' => 'application/json' }
      post(register_path, attribs.to_json, headers)
      expect(response.status).to eq(200)
      jresp = JSON.load(response.body)
      expect(jresp["auth_token"].length).to eq(20)
      
      expect(User.count).to eq(before_count+1)
      
      #ensure identity entry is created
      user=User.last
      expect(user.identities.last.uid).to eq("uid123")
      expect(user.email).to eq("eggie10@gmail.com")
    end
    
    it "should auto-fill profile form FB info" do
      attribs= {user: { access_token:"accexxtokenx"}}
      headers = {'CONTENT_TYPE' => "application/json", 'ACCEPT' => 'application/json' }
      post(register_path, attribs.to_json, headers)
      expect(response.status).to eq(200)
      
      user=User.last
      expect(user.name).to eq("Alex Egg")
      profile = user.profile
      expect(profile.name).to eq("Alex Egg")
      expect(profile.profile_pic).to eq("https://graph.facebook.com/uid123/picture?type=large&width=640")
    end
  end
  
  context "user signups up w/ oauth but the FB response is corrupt" do
    before do
      #stub
      json="{asdfasfd I am not json\"en_US\",\"name:-8,\"updated_time\":\"2014-08-07T21:43:17+0000\",\"verified\":true}"

      CurbCall::Oauth.stub(:fb_api_get){json}
      # CurbCall::Oauth.stub(:get){h}
    end
    
    it "should handle the error gracefully" do
      before_count = User.count
      attribs= {user: { access_token:"accexxtokenx"}}
      headers = {'CONTENT_TYPE' => "application/json", 'ACCEPT' => 'application/json' }
      post(register_path, attribs.to_json, headers)
      expect(response.status).to eq(401)
      
      expect(User.count).to eq(before_count)
    end
  end
  

end