require 'rails_helper'

RSpec.describe "Sightings API", :type => :request do
  
  

  let(:valid_attributes) do
    showing = create (:showing)
    { showing_id: showing.id, lat: 0, lon: 0 }
  end
  
  let(:valid_session) do
    user=create(:user)
    {'X-User-Email' => "#{user.email}", 'X-User-Token' =>  "#{user.authentication_token}"} 
  end

  describe "POST heartbeat" do
    it "returns http success" do
      post api_v2_sightings_heartbeat_path, valid_attributes , valid_session
      expect(response).to have_http_status(:success)
      
      expect(Sighting.last.showing.id).to eq(valid_attributes[:showing_id])
    end
  end

end
