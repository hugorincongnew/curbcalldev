require 'rails_helper'

describe "sessions_api", :type => :request  do

  context "traditional user/pass sign-in" do 
    it "should login and return session info" do
      #create user to login as
      user = create(:user)

      attribs={user:{email: user.email, password: "password" }}
      headers = {'CONTENT_TYPE' => "application/json", 'ACCEPT' => 'application/json'}
      post(login_path, attribs.to_json, headers)
      expect(response.status).to eq(200)
      jresp = JSON.load(response.body)
      expect(jresp["data"]["auth_token"].length).to eq(20)
    end

  end
  
  context "user signed up via FB oauth" do
    before do
      #create an oauthed user
      @user = create(:user_from_oauth)
      
      #stub
      # h={"id"=>"uid123", "first_name"=>"Alex", "gender"=>"male", "last_name"=>"Egg", "link"=>"",
      #    "locale"=>"en_US", "name"=>"Alex Egg", "timezone"=>-8, "updated_time"=>"2014-08-07T21:43:17+0000",
      #    "verified"=>true}
      # CurbCall::Oauth.stub(:get){h}
      
      json="{\"id\":\"uid123\",\"email\":\"eggie10\\u0040gmail.com\",\"first_name\":\"Alex\",\"gender\":\"male\",\"last_name\":\"Egg\",\"link\":\"https:\\/\\/www.facebook.com\\/app_scoped_user_id\\/10152910932811474\\/\",\"locale\":\"en_US\",\"name\":\"Alex Egg\",\"timezone\":-8,\"updated_time\":\"2014-08-07T21:43:17+0000\",\"verified\":true}"

      CurbCall::Oauth.stub(:fb_api_get){json}
    end
    
    it "should login w/ oauth token" do
      before_count = User.count
      attribs={user:{ access_token: "accesstokenxxx" }}
      headers = {'CONTENT_TYPE' => "application/json", 'ACCEPT' => 'application/json'}
      post(login_path, attribs.to_json, headers) #sessions controller
      expect(response.status).to eq(200)
      # expect(session["warden.user.user.key"]).to eq("asdf")
      
      jresp = JSON.load(response.body)
      expect(jresp["data"]["user_id"]).to eq(@user.id)
      expect(jresp["data"]["image"]).to eq("https://graph.facebook.com/uid123/picture?type=large&width=640")
      expect(User.count).to eq(before_count)
      
    end
    
    it "should not login w/ bad token" do 
      before_count = User.count
      CurbCall::Oauth.stub(:get){nil}
      attribs={user:{ access_token: "accesstokenxxx"  }}
      headers = {'CONTENT_TYPE' => "application/json", 'ACCEPT' => 'application/json'}
      post(login_path, attribs.to_json, headers)
      expect(response.status).to eq(401) #unauthorized!
      expect(User.count).to eq(before_count)
    end
  end

  

end