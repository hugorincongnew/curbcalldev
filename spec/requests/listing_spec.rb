require 'rails_helper'

describe "listings_api"  do

  # This should return the minimal set of attributes required to create a valid
  # Listing. As you add validations to Listing, be sure to
  # adjust the attributes here as well.
  listing_params= {

    }

  let(:valid_attributes) {listing_params}




  # This should return the minimal set of values that should be in the session
  # in order to pass any filters (e.g. authentication) defined in
  # CardsController. Be sure to keep this updated too.
  let(:valid_session) do
    user=create(:user)
    {'X-User-Email' => "#{user.email}", 'X-User-Token' =>  "#{user.authentication_token}"} 
  end

  describe "GET index" , :type => :request do
    it "gets all listings" do
      #login user
      get api_v2_listings_path, {}, valid_session
      expect(response.status).to eq(200)
    end
  end
  
  describe "GET show", type: :request do
    it "gets a listing" do
      listing = create(:listing) 
      get api_v2_listing_path(listing), {}, valid_session
      expect(response.status).to eq(200)
    end
    
    it "should redirect a user w/ bad creds" do
      listing = create(:listing) 
      get api_v2_listing_path(listing), {}, {}
      
      #actually it should 401...
      expect(response.status).to eq(302)
    end
  end

  
  # describe "PUT show", type: :request do
  #   it "should be updateable" do
  #     listing = create(:listing)
  #     put api_v2_listing_path(listing), { listing: { price: 1000 }}, valid_session
  #     expect(response.status).to eq(200)
  #
  #     listing_after = Lising.find(listing.id)
  #     expect(listing.price).to eq(1000)
  #   end
  # end
end