require 'rails_helper'

describe "profiles_api"  do


  # This should return the minimal set of values that should be in the session
  # in order to pass any filters (e.g. authentication) defined in
  # CardsController. Be sure to keep this updated too.
  let(:valid_session) do
    user=create(:user)
    {'X-User-Email' => "#{user.email}", 'X-User-Token' =>  "#{user.authentication_token}"} 
  end

  # describe "GET index" , :type => :request do
  #   it "gets all listings" do
  #     #login user
  #     get api_v2_listings_path, {}, valid_session
  #     expect(response.status).to eq(200)
  #   end
  # end
  #
  # describe "GET show", type: :request do
  #   it "gets a listing" do
  #     listing = create(:listing)
  #     get api_v2_listing_path(listing), {}, valid_session
  #     expect(response.status).to eq(200)
  #   end
  #
  #   it "should redirect a user w/ bad creds" do
  #     listing = create(:listing)
  #     get api_v2_listing_path(listing), {}, {}
  #
  #     #actually it should 401...
  #     expect(response.status).to eq(302)
  #   end
  #end

  
  describe "POST profile", type: :request do
    it "should should update existing profile on create POST" do
      user = create(:user_with_profile)
      expect(user.profile.bio).to eq("bio text") #just confirm the factory works
      before_count = Profile.count
      before_id = user.profile.id
  
      post api_v2_profiles_path, {  bio: "new bio", user_id: user.id }, valid_session
      expect(response.status).to eq(200)

      user = User.find(user.id) #refresh w/ new db data
      expect(user.profile.bio).to eq("new bio")
      expect(user.profile.id).to eq(before_id)
      
      after_count = Profile.count
      expect(after_count).to eq(before_count)
    end
    
    it "should do a normal create" do
       user = create(:user) # w/ no profile
       expect(user.profile).to eq(nil)
       post api_v2_profiles_path, {bio: "new bio", user_id: user.id }, valid_session
       expect(response.status).to eq(201)
       
       user = User.find(user.id) #refresh w/ new db data
       expect(user.profile.bio).to eq("new bio")
       
    end
  end
  describe "PUT profile", type: :request do
    it "should update an exisiting model" do
       profile = create(:profile)
       put api_v2_profile_path(profile), { bio: "updated bio" }, valid_session
       expect(response.status).to eq(200)

       profile_after = Profile.find(profile.id)
       expect(profile_after.bio).to eq("updated bio")
    end
  end
end