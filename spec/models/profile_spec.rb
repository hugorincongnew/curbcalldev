require 'rails_helper'

RSpec.describe Profile, :type => :model do
  it "should have a user association" do
    profile = create(:profile)
    
    #just make sure the user assocation is working
    expect(profile.user.name).to eq("User Name") 
  end
  
  it "should not overwrite a profile" do
    # user_w_profile = create(:user_w_profile)
    user = create(:user_with_profile)
    expect(user.profile.bio).to eq("bio text") #just confirm the factory works
    
  end
end
