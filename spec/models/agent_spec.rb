require 'rails_helper'

RSpec.describe Agent, :type => :model do
 
  it "should have a new history when save the agent" do
  	expect{create(:agent)}.to change{HistoryAgent.all.count}
  end
  
  it "should update the history" do
    agent = create(:agent)
    history = HistoryAgent.not_finish(agent.user).first
    expect{agent.destroy}.to change{history.reload.end_time}
  end
  
end
