require 'rails_helper'


RSpec.describe "Safety Radius", :type => :model do
  
  describe "LatLng collection factory" do
    context "showing WITH sightings" do 
      it "should convert collection of sighitngs to collection of LatLng" do
        showing = create(:showing_with_sightings)
        points = CurbCall::SafetyRadius::LatLng.collection_factory(showing.sightings)
        expect(points.length).to eq(5)
        expect(points.last.class).to eq(CurbCall::SafetyRadius::LatLng)
      end
    end
  end
  
  describe "safety-radius feature" do
    context "a showing where the agent STAYED in the radius" do
      it "should return a safe status" do

        radius = 10
        threshold = 5
        showing = create(:showing_with_sightings)
        points = CurbCall::SafetyRadius::LatLng.collection_factory(showing.sightings)
    
        safety_service = CurbCall::SafetyRadius::Base.factory(radius, points, threshold)
        result = safety_service.run_analysis()
    
        expect(result.state).to eq(CurbCall::SafetyRadius::Base::STATES[:safe])
      end
    end
    context "a showing where the agent EXITED radius" do
      before do
        @radius = 10
        @threshold = 5
        showing = create(:showing_with_sightings)
        @points = CurbCall::SafetyRadius::LatLng.collection_factory(showing.sightings)
      end
      
      it "should return warning if warning over threshold" do
        #create 5 out-of-bounds points
        (0..6).each{|i| @points << CurbCall::SafetyRadius::LatLng.new(100,100) }
    
        safety_service = CurbCall::SafetyRadius::Base.factory(@radius, @points, @threshold)
        result = safety_service.run_analysis()
    
        expect(result.state).to eq(CurbCall::SafetyRadius::Base::STATES[:warning])
      end
      
      it "should not return warning unless threshold meet" do
        #create 3 out-of-bounds points (less than the threshold)
        (0..3).each{|i| @points << CurbCall::SafetyRadius::LatLng.new(100,100) }
    
        safety_service = CurbCall::SafetyRadius::Base.factory(@radius, @points, @threshold)
        result = safety_service.run_analysis()
    
        expect(result.state).to eq(CurbCall::SafetyRadius::Base::STATES[:safe])
      end
    end
  end
  
  it "should do hit detection" do
    pt = CurbCall::SafetyRadius::LatLng.new(3,3)
    safety_service = CurbCall::SafetyRadius::Base.factory(10, [pt], 5)
    origin = CurbCall::SafetyRadius::LatLng.new(0,0)
    radius = 10
    
    sighting = CurbCall::SafetyRadius::LatLng.new(3,3)
    expect(safety_service.is_inside_radius?(origin, radius, sighting)).to eq(true)
    
    sighting = CurbCall::SafetyRadius::LatLng.new(11,3)
    expect(safety_service.is_inside_radius?(origin, radius, sighting)).to eq(false)
    
    sighting = CurbCall::SafetyRadius::LatLng.new(1,11)
    expect(safety_service.is_inside_radius?(origin, radius, sighting)).to eq(false)
    
    sighting = CurbCall::SafetyRadius::LatLng.new(11,11)
    expect(safety_service.is_inside_radius?(origin, radius, sighting)).to eq(false)
    
    sighting = CurbCall::SafetyRadius::LatLng.new(9,9)
    expect(safety_service.is_inside_radius?(origin, radius, sighting)).to eq(true)
    
    sighting = CurbCall::SafetyRadius::LatLng.new(-1,-9)
    expect(safety_service.is_inside_radius?(origin, radius, sighting)).to eq(true)
    
    sighting = CurbCall::SafetyRadius::LatLng.new(-1,-11)
    expect(safety_service.is_inside_radius?(origin, radius, sighting)).to eq(false)
    
    #update the origin
    origin = CurbCall::SafetyRadius::LatLng.new(32,-114)
    
    sighting = CurbCall::SafetyRadius::LatLng.new(-1,-11)
    expect(safety_service.is_inside_radius?(origin, radius, sighting)).to eq(false)
    
    sighting = CurbCall::SafetyRadius::LatLng.new(23,-116)
    expect(safety_service.is_inside_radius?(origin, radius, sighting)).to eq(true)
    
  end
end
