require 'rails_helper'

RSpec.describe Lead, :type => :model do
  it "should send an email on 'claimed' state" do
    # expect(AgentMailer).to respond_to(:lead_info)
    lead = create(:lead)
    expect(lead.status).to eq(Lead::STATES[:unanswered])
    
    lead.claim()
    
    expect(lead.status).to eq(Lead::STATES[:claimed])
  end
end
