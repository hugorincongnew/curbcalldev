require 'rails_helper'

RSpec.describe App, :type => :model do
  describe "sandbox key" do
    it "should gen live and sandbox keys" do
      app = create(:app)
      expect(app.access_token.length).to eq(40)
      expect(app.sandbox_access_token.length).to eq(40)
    end    
  end
end
