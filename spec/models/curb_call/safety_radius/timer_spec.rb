require 'rails_helper'

include CurbCall::SafetyRadius

describe CurbCall::SafetyRadius::Timer do
  before do
    @showing = create(:showing)
  end
  
  it "should work" do
    
    @showing.syn_agent
    
    
    expired = CurbCall::SafetyRadius::Timer.timeout?(@showing.history.all.last[:ts], 6.minutes.from_now)
    
    expect(expired).to eq (true)
    
    
  end
  
  context "showing was in warning, but now is safe" do
    it "should not transition to danger" do
      #set to warning
      @showing.syn_agent
      #now the timer is going... 5 min...
      #but the user acks
      @showing.syn_ack
      
      #timer elapses
      @showing.check_ack_timeout(6.minutes.from_now)
      expect(@showing.agent_state).to eq(Showing::STATES.fetch(:safe))
    end
  end
end