require 'rails_helper'

include CurbCall::SafetyRadius

describe CurbCall::SafetyRadius::History do
  it "should work" do
    showing = create(:showing)
    history = History.new(showing.id, nil)
    
    history.add(History::EVENTS[:start])
    
    expect(history.all).to eq([{:ts=>Time.zone.now.to_s, :event=>"start"}])
    
  end
end