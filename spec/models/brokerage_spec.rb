require 'rails_helper'

RSpec.describe Brokerage, :type => :model do
  it "should have multiple zips" do
    b=create(:brokerage_w_mlss)

    expect(b.mls.length).to eq(2)
  end

  it "should have a access token after create" do
  	brokerage = build(:brokerage)
  	expect(brokerage.access_token).to eq(nil)
  	brokerage.save!
  	expect(brokerage.reload.access_token).not_to eq(nil)
  end

  describe "should generate a lead email after create" do
    before(:each) do
      @brokerage = create(:brokerage)
    end
    it "should have a lead email after create" do
      expect(@brokerage.lead_email).not_to eq(nil)
    end

    it "should generate with the @curbcallcollect.com domain" do
      expect(@brokerage.lead_email[22..-1]).to eq("@curbcallcollect.com")
    end
  end

end
