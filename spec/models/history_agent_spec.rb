require 'rails_helper'

RSpec.describe HistoryAgent, :type => :model do

  before(:each) do
    @start_time = Time.now
    @brokerage = create(:brokerage)
    @user = create(:user, brokerage: @brokerage)
    @agents = create_list(:agent, 5, user: @user)
    @agents.each do |agent|
      agent.destroy
    end
  end
  
  it "find length time valid" do
    result = HistoryAgent.length_of_time(@brokerage, @start_time, Time.now)
    expect(result.any?).to eq(true)
  end
  
  it "find num_showing zero" do
    result = HistoryAgent.agent_notified(@brokerage, @start_time, Time.now)
    expect(result.any?).to eq(true)
    expect(result.first.total_request).to eq(0)
  end
  
  it "find num_showing valid" do
    @agents = create_list(:agent, 5, user: @user)
    @agents.each_with_index do |agent, index|
      agent.new_request
      agent.destroy
    end
    result = HistoryAgent.agent_notified(@brokerage, @start_time, Time.now)
    expect(result.any?).to eq(true)
    expect(result.first.total_request).to eq(5)
  end
  
end
