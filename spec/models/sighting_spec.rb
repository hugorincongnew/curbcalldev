require 'rails_helper'

RSpec.describe Sighting, :type => :model do
  it "should find sightings for a showing" do
    showing = create(:showing)
    
    sighting1 = Sighting.create(showing: showing, lat: 0, lon: 0)
    sightings2 = Sighting.create(showing: showing, lat: 10, lon: -10)

    showing.sightings << sighting1
    showing.sightings << sightings2
    
    expect(showing.sightings.length).to eq(2)
  end
  
  it "should geolocate" do
    #this was a test for the POST GIS 
    #gelocator, but the app isn't using it for some reason
    # showing = create(:showing)
    #
    # far = Sighting.create!(
    #     showing:      showing,
    #     lat:   40.000000,
    #     lon: -77.000000
    #   )
    #
    #   close = Sighting.create!(
    #     showing:      showing,
    #     lat:   39.010000,
    #     lon: -75.990000
    #   )
    #
    #   close_sightings = Sighting.close_to(39.000000, -76.000000).load
    #
    #   expect(Sighting.count).to eq(2)
    #   assert_equal 1, close_sightings.size
    #   assert_equal close, close_sightings.first
  end
end
