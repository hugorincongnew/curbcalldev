require 'rails_helper'

RSpec.describe Showing, :type => :model do
  
  describe "Showings have state" do
    it "should end nomially" do
      showing = create(:showing)
      expect(showing.status).to eq(Showing::SHOWING_STATES[:waiting])
      
      #showing ends normally
      expect(showing.status).to eq(Showing::SHOWING_STATES[:waiting])
    end
    
    it "should end anomiously if agent has issus" do
      showing = create(:showing)
      expect(showing.status).to eq(Showing::SHOWING_STATES[:waiting])
      
      #agent is in trouble
      expect(showing.status).to eq(Showing::SHOWING_STATES[:waiting])
    end
  end
  
  describe "real-time map feature" do 
    context "no current showings" do
      it "it should return no showings" do 
        brokerage = create(:brokerage)
        showings = brokerage.showings.active
        expect(showings.count).to eq(0)
      end
    end
    
    context "5 active showing" do 
      before do
         @brokerage = create(:brokerage_w_users, users_count: 1)        
      end
      
      it "should return 5 active showing" do
         expect(@brokerage.users.count).to eq(1)
         showings = @brokerage.showings.active
         expect(showings.count).to eq(5)
      end
      
      it "should return the last 5 sightings" do
        showings = @brokerage.showings.active
        pts = Showing.collect_last_points(showings)
        p pts
      end
    end
  end
  
  describe "safety radius feature" do
    context "agent is on a showing" do 
      before do
        CurbCall::Config[CurbCall::SafetyRadius::Base::SETTINGS[:safety_sync_message]] = "Are you ok?"
      end
      it "should have a status " do
        showing = create(:showing)
      
        #showing starts off in the safe state
        expect(showing.agent_state).to eq( Showing::STATES[:safe])        
        
        #now the agent forgets to turn off the tracking and goes
        #outside the safety radius, we trigger a syn message
        showing.syn_agent
        expect(showing.agent_state).to eq( Showing::STATES[:warning])
        
        #the user acknowldges the message we go back to safe states
        showing.syn_ack
        expect(showing.agent_state).to eq( Showing::STATES[:safe])
        
        #the agent is abducted
        showing.syn_agent
        expect(showing.agent_state).to eq( Showing::STATES[:warning])
        
        #doesn't ack in time threshold
        showing.ack_timeout
        expect(showing.agent_state).to eq( Showing::STATES[:danger])
        
        #agent is recovered
        showing.syn_ack
        expect(showing.agent_state).to eq( Showing::STATES[:safe])
      end
    end
  end
  
  context "a normal showing" do
    before do
      @showing = create(:showing)
    end
    
    context "a user hits the panic button" do 
      it "should set the showing to panic state" do
        @showing.panic
        expect(@showing.agent_state).to eq(Showing::STATES[:danger])
      end
    end
  end
  
  context "elegible showing w/ an brokerage" do
  	before do
      @brokerage = create(:brokerage)
    end
  	
  	it "should success" do
  		@showings = Showing.eligible(@brokerage)
  		expect(@showings).to eq([])
  	end
  	
  end
  
  context "find brokerge in range" do
  
    before(:each) do
      @start_time = Time.now
      @brokerage = create(:brokerage)
      @showings = create_list(:showing, 5, brokerage: @brokerage)
      @end_time = Time.now
    end
  
  	it "return a list valid" do
      expect(Showing.for_brokerage(@brokerage).for_range(@start_time, @end_time).count).to eq(@showings.size)
  	end
  
  	it "in status claimed should return a list invalid" do
  	  expect(Showing.for_brokerage(@brokerage).for_range(@start_time, @end_time).claimed.count).to eq(0)
  	end
  	
  	it "in status claimed should return a list valid" do
  	  @showings = create_list(:showing, 5, brokerage: @brokerage, status: "completed")
      @end_time = Time.now
      expect(Showing.for_brokerage(@brokerage).for_range(@start_time, @end_time).claimed.count).to eq(@showings.size)
  	end
  
  end

end
