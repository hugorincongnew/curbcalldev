require 'rails_helper'

RSpec.describe EligibleAgent, :type => :model do
  it "should send push request on creation" do
    lead=create(:lead)
    agent_user = create(:agent)
    
    ea = EligibleAgent.create(lead: lead, agent_user_id: agent_user.id)
    
    expect(ea.sidekiq_id.length).to eq(24) #ensure the job was queued
  end
end
