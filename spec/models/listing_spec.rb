require 'rails_helper'

RSpec.describe Listing, :type => :model do
  it "should have a zip code" do
    l=create(:listing)
    mls = l.mls
    
    #this mls should compromise 2 zip codes
    expect(mls.name).to eq("mls name")
    expect(mls.zips.length).to eq(2)
    expect(mls.brokerages.length).to eq(2)

  end
end
